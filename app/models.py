# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)

# pylint: disable=invalid-name,no-self-argument
"Models mapping to the database"
from __future__ import annotations

from datetime import datetime
from enum import Enum

import pytz
from pydantic import BaseModel, ValidationError, field_validator
from pydantic.functional_serializers import PlainSerializer
from sqlmodel import JSON, Boolean, Column, Field, SQLModel
from typing_extensions import Annotated

TZ = pytz.UTC

## to serialize dates for datetime objects
JsonDate = Annotated[
    datetime,
    PlainSerializer(lambda x: datetime.strftime(x, '%Y-%m-%dT%H:%M:%SZ'),
                    return_type=str,
                    when_used='json-unless-none')]


class CanBe(str, Enum):
    """CanBe type"""
    DRAFTED = 'DRAFTED'
    DISCARDED = 'DISCARDED'
    PUBLISHED = 'PUBLISHED'
    CANCELLED = 'CANCELLED'
    AMENDED = 'AMENDED'
    CORRECTED = 'CORRECTED'


class Status(str, Enum):
    """Status type"""
    NEW = 'NEW'
    PUBLISHED = 'PUBLISHED'
    CORRECTED = 'CORRECTED'
    AMENDED = 'AMENDED'
    CANCELLED = 'CANCELLED'
    EXPIRED = 'EXPIRED'
    DRAFT = 'DRAFT'
    DRAFT_AMENDED = 'DRAFT_AMENDED'
    DRAFT_CORRECTED = 'DRAFT_CORRECTED'


class CloudType(str, Enum):
    """CloudType type"""
    CB = 'CB'
    TCU = 'TCU'


class CloudCoverage(str, Enum):
    """CloudCoverage type"""
    FEW = 'FEW'
    SCT = 'SCT'
    BKN = 'BKN'
    OVC = 'OVC'


class Nsc(str, Enum):
    """Nsc type"""
    NSC = 'NSC'


class Nsw(str, Enum):
    """Nsw type"""
    NSW = 'NSW'


class Knots(str, Enum):
    """Knots type"""
    KT = 'KT'


class Cloud(BaseModel):
    """Cloud type"""
    type: CloudType | None = None
    height: int
    coverage: CloudCoverage


class VerticalVisibility(BaseModel):
    """VerticalVisibility type"""
    verticalVisibility: int


class Clouds(BaseModel):
    """Clouds type"""
    cloud1: Cloud | VerticalVisibility | Nsc
    cloud2: Cloud | None = None
    cloud3: Cloud | None = None
    cloud4: Cloud | None = None


class Valid(BaseModel):
    """Valid type"""
    end: datetime | None = None
    start: datetime


class Visibility(BaseModel):
    """Visibility type"""
    range: int
    unit: str


class WeatherPhenomena(str, Enum):
    """WeatherPhenomena type"""
    PLUSDS = '+DS'
    PLUSDZ = '+DZ'
    PLUSDZPL = '+DZPL'
    PLUSDZPLRA = '+DZPLRA'
    PLUSDZRA = '+DZRA'
    PLUSDZRAPL = '+DZRAPL'
    PLUSDZRASG = '+DZRASG'
    PLUSDZRASN = '+DZRASN'
    PLUSDZSG = '+DZSG'
    PLUSDZSGRA = '+DZSGRA'
    PLUSDZSN = '+DZSN'
    PLUSDZSNRA = '+DZSNRA'
    PLUSFC = '+FC'
    PLUSFZDZ = '+FZDZ'
    PLUSFZDZRA = '+FZDZRA'
    PLUSFZRA = '+FZRA'
    PLUSFZRADZ = '+FZRADZ'
    PLUSFZUP = '+FZUP'
    PLUSPL = '+PL'
    PLUSPLDZ = '+PLDZ'
    PLUSPLDZRA = '+PLDZRA'
    PLUSPLRA = '+PLRA'
    PLUSPLRADZ = '+PLRADZ'
    PLUSPLRASN = '+PLRASN'
    PLUSPLSG = '+PLSG'
    PLUSPLSGSN = '+PLSGSN'
    PLUSPLSN = '+PLSN'
    PLUSPLSNRA = '+PLSNRA'
    PLUSPLSNSG = '+PLSNSG'
    PLUSRA = '+RA'
    PLUSRADZ = '+RADZ'
    PLUSRADZPL = '+RADZPL'
    PLUSRADZSG = '+RADZSG'
    PLUSRADZSN = '+RADZSN'
    PLUSRAPL = '+RAPL'
    PLUSRAPLDZ = '+RAPLDZ'
    PLUSRAPLSN = '+RAPLSN'
    PLUSRASG = '+RASG'
    PLUSRASGDZ = '+RASGDZ'
    PLUSRASGSN = '+RASGSN'
    PLUSRASN = '+RASN'
    PLUSRASNDZ = '+RASNDZ'
    PLUSRASNPL = '+RASNPL'
    PLUSRASNSG = '+RASNSG'
    PLUSSG = '+SG'
    PLUSSGDZ = '+SGDZ'
    PLUSSGDZRA = '+SGDZRA'
    PLUSSGPL = '+SGPL'
    PLUSSGPLSN = '+SGPLSN'
    PLUSSGRA = '+SGRA'
    PLUSSGRADZ = '+SGRADZ'
    PLUSSGRASN = '+SGRASN'
    PLUSSGSN = '+SGSN'
    PLUSSGSNPL = '+SGSNPL'
    PLUSSGSNRA = '+SGSNRA'
    PLUSSHGR = '+SHGR'
    PLUSSHGRRA = '+SHGRRA'
    PLUSSHGRRASN = '+SHGRRASN'
    PLUSSHGRSN = '+SHGRSN'
    PLUSSHGRSNRA = '+SHGRSNRA'
    PLUSSHGS = '+SHGS'
    PLUSSHGSRA = '+SHGSRA'
    PLUSSHGSRASN = '+SHGSRASN'
    PLUSSHGSSN = '+SHGSSN'
    PLUSSHGSSNRA = '+SHGSSNRA'
    PLUSSHRA = '+SHRA'
    PLUSSHRAGR = '+SHRAGR'
    PLUSSHRAGRSN = '+SHRAGRSN'
    PLUSSHRAGS = '+SHRAGS'
    PLUSSHRAGSSN = '+SHRAGSSN'
    PLUSSHRASN = '+SHRASN'
    PLUSSHRASNGR = '+SHRASNGR'
    PLUSSHRASNGS = '+SHRASNGS'
    PLUSSHSN = '+SHSN'
    PLUSSHSNGR = '+SHSNGR'
    PLUSSHSNGRRA = '+SHSNGRRA'
    PLUSSHSNGS = '+SHSNGS'
    PLUSSHSNGSRA = '+SHSNGSRA'
    PLUSSHSNRA = '+SHSNRA'
    PLUSSHSNRAGR = '+SHSNRAGR'
    PLUSSHSNRAGS = '+SHSNRAGS'
    PLUSSHUP = '+SHUP'
    PLUSSN = '+SN'
    PLUSSNDZ = '+SNDZ'
    PLUSSNDZRA = '+SNDZRA'
    PLUSSNPL = '+SNPL'
    PLUSSNPLRA = '+SNPLRA'
    PLUSSNPLSG = '+SNPLSG'
    PLUSSNRA = '+SNRA'
    PLUSSNRADZ = '+SNRADZ'
    PLUSSNRAPL = '+SNRAPL'
    PLUSSNRASG = '+SNRASG'
    PLUSSNSG = '+SNSG'
    PLUSSNSGPL = '+SNSGPL'
    PLUSSNSGRA = '+SNSGRA'
    PLUSSS = '+SS'
    PLUSTSGR = '+TSGR'
    PLUSTSGRRA = '+TSGRRA'
    PLUSTSGRRASN = '+TSGRRASN'
    PLUSTSGRSN = '+TSGRSN'
    PLUSTSGRSNRA = '+TSGRSNRA'
    PLUSTSGS = '+TSGS'
    PLUSTSGSRA = '+TSGSRA'
    PLUSTSGSRASN = '+TSGSRASN'
    PLUSTSGSSN = '+TSGSSN'
    PLUSTSGSSNRA = '+TSGSSNRA'
    PLUSTSRA = '+TSRA'
    PLUSTSRAGR = '+TSRAGR'
    PLUSTSRAGRSN = '+TSRAGRSN'
    PLUSTSRAGS = '+TSRAGS'
    PLUSTSRAGSSN = '+TSRAGSSN'
    PLUSTSRASN = '+TSRASN'
    PLUSTSRASNGR = '+TSRASNGR'
    PLUSTSRASNGS = '+TSRASNGS'
    PLUSTSSN = '+TSSN'
    PLUSTSSNGR = '+TSSNGR'
    PLUSTSSNGRRA = '+TSSNGRRA'
    PLUSTSSNGS = '+TSSNGS'
    PLUSTSSNGSRA = '+TSSNGSRA'
    PLUSTSSNRA = '+TSSNRA'
    PLUSTSSNRAGR = '+TSSNRAGR'
    PLUSTSSNRAGS = '+TSSNRAGS'
    PLUSTSUP = '+TSUP'
    PLUSUP = '+UP'
    MINUSDS = '-DS'
    MINUSDZ = '-DZ'
    MINUSDZPL = '-DZPL'
    MINUSDZPLRA = '-DZPLRA'
    MINUSDZRA = '-DZRA'
    MINUSDZRAPL = '-DZRAPL'
    MINUSDZRASG = '-DZRASG'
    MINUSDZRASN = '-DZRASN'
    MINUSDZSG = '-DZSG'
    MINUSDZSGRA = '-DZSGRA'
    MINUSDZSN = '-DZSN'
    MINUSDZSNRA = '-DZSNRA'
    MINUSFZDZ = '-FZDZ'
    MINUSFZDZRA = '-FZDZRA'
    MINUSFZRA = '-FZRA'
    MINUSFZRADZ = '-FZRADZ'
    MINUSFZUP = '-FZUP'
    MINUSPL = '-PL'
    MINUSPLDZ = '-PLDZ'
    MINUSPLDZRA = '-PLDZRA'
    MINUSPLRA = '-PLRA'
    MINUSPLRADZ = '-PLRADZ'
    MINUSPLRASN = '-PLRASN'
    MINUSPLSG = '-PLSG'
    MINUSPLSGSN = '-PLSGSN'
    MINUSPLSN = '-PLSN'
    MINUSPLSNRA = '-PLSNRA'
    MINUSPLSNSG = '-PLSNSG'
    MINUSRA = '-RA'
    MINUSRADZ = '-RADZ'
    MINUSRADZPL = '-RADZPL'
    MINUSRADZSG = '-RADZSG'
    MINUSRADZSN = '-RADZSN'
    MINUSRAPL = '-RAPL'
    MINUSRAPLDZ = '-RAPLDZ'
    MINUSRAPLSN = '-RAPLSN'
    MINUSRASG = '-RASG'
    MINUSRASGDZ = '-RASGDZ'
    MINUSRASGSN = '-RASGSN'
    MINUSRASN = '-RASN'
    MINUSRASNDZ = '-RASNDZ'
    MINUSRASNPL = '-RASNPL'
    MINUSRASNSG = '-RASNSG'
    MINUSSG = '-SG'
    MINUSSGDZ = '-SGDZ'
    MINUSSGDZRA = '-SGDZRA'
    MINUSSGPL = '-SGPL'
    MINUSSGPLSN = '-SGPLSN'
    MINUSSGRA = '-SGRA'
    MINUSSGRADZ = '-SGRADZ'
    MINUSSGRASN = '-SGRASN'
    MINUSSGSN = '-SGSN'
    MINUSSGSNPL = '-SGSNPL'
    MINUSSGSNRA = '-SGSNRA'
    MINUSSHGR = '-SHGR'
    MINUSSHGRRA = '-SHGRRA'
    MINUSSHGRRASN = '-SHGRRASN'
    MINUSSHGRSN = '-SHGRSN'
    MINUSSHGRSNRA = '-SHGRSNRA'
    MINUSSHGS = '-SHGS'
    MINUSSHGSRA = '-SHGSRA'
    MINUSSHGSRASN = '-SHGSRASN'
    MINUSSHGSSN = '-SHGSSN'
    MINUSSHGSSNRA = '-SHGSSNRA'
    MINUSSHRA = '-SHRA'
    MINUSSHRAGR = '-SHRAGR'
    MINUSSHRAGRSN = '-SHRAGRSN'
    MINUSSHRAGS = '-SHRAGS'
    MINUSSHRAGSSN = '-SHRAGSSN'
    MINUSSHRASN = '-SHRASN'
    MINUSSHRASNGR = '-SHRASNGR'
    MINUSSHRASNGS = '-SHRASNGS'
    MINUSSHSN = '-SHSN'
    MINUSSHSNGR = '-SHSNGR'
    MINUSSHSNGRRA = '-SHSNGRRA'
    MINUSSHSNGS = '-SHSNGS'
    MINUSSHSNGSRA = '-SHSNGSRA'
    MINUSSHSNRA = '-SHSNRA'
    MINUSSHSNRAGR = '-SHSNRAGR'
    MINUSSHSNRAGS = '-SHSNRAGS'
    MINUSSHUP = '-SHUP'
    MINUSSN = '-SN'
    MINUSSNDZ = '-SNDZ'
    MINUSSNDZRA = '-SNDZRA'
    MINUSSNPL = '-SNPL'
    MINUSSNPLRA = '-SNPLRA'
    MINUSSNPLSG = '-SNPLSG'
    MINUSSNRA = '-SNRA'
    MINUSSNRADZ = '-SNRADZ'
    MINUSSNRAPL = '-SNRAPL'
    MINUSSNRASG = '-SNRASG'
    MINUSSNSG = '-SNSG'
    MINUSSNSGPL = '-SNSGPL'
    MINUSSNSGRA = '-SNSGRA'
    MINUSSS = '-SS'
    MINUSTSGR = '-TSGR'
    MINUSTSGRRA = '-TSGRRA'
    MINUSTSGRRASN = '-TSGRRASN'
    MINUSTSGRSN = '-TSGRSN'
    MINUSTSGRSNRA = '-TSGRSNRA'
    MINUSTSGS = '-TSGS'
    MINUSTSGSRA = '-TSGSRA'
    MINUSTSGSRASN = '-TSGSRASN'
    MINUSTSGSSN = '-TSGSSN'
    MINUSTSGSSNRA = '-TSGSSNRA'
    MINUSTSRA = '-TSRA'
    MINUSTSRAGR = '-TSRAGR'
    MINUSTSRAGRSN = '-TSRAGRSN'
    MINUSTSRAGS = '-TSRAGS'
    MINUSTSRAGSSN = '-TSRAGSSN'
    MINUSTSRASN = '-TSRASN'
    MINUSTSRASNGR = '-TSRASNGR'
    MINUSTSRASNGS = '-TSRASNGS'
    MINUSTSSN = '-TSSN'
    MINUSTSSNGR = '-TSSNGR'
    MINUSTSSNGRRA = '-TSSNGRRA'
    MINUSTSSNGS = '-TSSNGS'
    MINUSTSSNGSRA = '-TSSNGSRA'
    MINUSTSSNRA = '-TSSNRA'
    MINUSTSSNRAGR = '-TSSNRAGR'
    MINUSTSSNRAGS = '-TSSNRAGS'
    MINUSTSUP = '-TSUP'
    MINUSUP = '-UP'
    BCFG = 'BCFG'
    BLDU = 'BLDU'
    BLSA = 'BLSA'
    BLSN = 'BLSN'
    BR = 'BR'
    DRDU = 'DRDU'
    DRSA = 'DRSA'
    DRSN = 'DRSN'
    DS = 'DS'
    DU = 'DU'
    DZ = 'DZ'
    DZPL = 'DZPL'
    DZPLRA = 'DZPLRA'
    DZRA = 'DZRA'
    DZRAPL = 'DZRAPL'
    DZRASG = 'DZRASG'
    DZRASN = 'DZRASN'
    DZSG = 'DZSG'
    DZSGRA = 'DZSGRA'
    DZSN = 'DZSN'
    DZSNRA = 'DZSNRA'
    FC = 'FC'
    FG = 'FG'
    FU = 'FU'
    FZDZ = 'FZDZ'
    FZDZRA = 'FZDZRA'
    FZFG = 'FZFG'
    FZRA = 'FZRA'
    FZRADZ = 'FZRADZ'
    FZUP = 'FZUP'
    HZ = 'HZ'
    MIFG = 'MIFG'
    PL = 'PL'
    PLDZ = 'PLDZ'
    PLDZRA = 'PLDZRA'
    PLRA = 'PLRA'
    PLRADZ = 'PLRADZ'
    PLRASN = 'PLRASN'
    PLSG = 'PLSG'
    PLSGSN = 'PLSGSN'
    PLSN = 'PLSN'
    PLSNRA = 'PLSNRA'
    PLSNSG = 'PLSNSG'
    PO = 'PO'
    PRFG = 'PRFG'
    RA = 'RA'
    RADZ = 'RADZ'
    RADZPL = 'RADZPL'
    RADZSG = 'RADZSG'
    RADZSN = 'RADZSN'
    RAPL = 'RAPL'
    RAPLDZ = 'RAPLDZ'
    RAPLSN = 'RAPLSN'
    RASG = 'RASG'
    RASGDZ = 'RASGDZ'
    RASGSN = 'RASGSN'
    RASN = 'RASN'
    RASNDZ = 'RASNDZ'
    RASNPL = 'RASNPL'
    RASNSG = 'RASNSG'
    SA = 'SA'
    SG = 'SG'
    SGDZ = 'SGDZ'
    SGDZRA = 'SGDZRA'
    SGPL = 'SGPL'
    SGPLSN = 'SGPLSN'
    SGRA = 'SGRA'
    SGRADZ = 'SGRADZ'
    SGRASN = 'SGRASN'
    SGSN = 'SGSN'
    SGSNPL = 'SGSNPL'
    SGSNRA = 'SGSNRA'
    SHGR = 'SHGR'
    SHGRRA = 'SHGRRA'
    SHGRRASN = 'SHGRRASN'
    SHGRSN = 'SHGRSN'
    SHGRSNRA = 'SHGRSNRA'
    SHGS = 'SHGS'
    SHGSRA = 'SHGSRA'
    SHGSRASN = 'SHGSRASN'
    SHGSSN = 'SHGSSN'
    SHGSSNRA = 'SHGSSNRA'
    SHRA = 'SHRA'
    SHRAGR = 'SHRAGR'
    SHRAGRSN = 'SHRAGRSN'
    SHRAGS = 'SHRAGS'
    SHRAGSSN = 'SHRAGSSN'
    SHRASN = 'SHRASN'
    SHRASNGR = 'SHRASNGR'
    SHRASNGS = 'SHRASNGS'
    SHSN = 'SHSN'
    SHSNGR = 'SHSNGR'
    SHSNGRRA = 'SHSNGRRA'
    SHSNGS = 'SHSNGS'
    SHSNGSRA = 'SHSNGSRA'
    SHSNRA = 'SHSNRA'
    SHSNRAGR = 'SHSNRAGR'
    SHSNRAGS = 'SHSNRAGS'
    SHUP = 'SHUP'
    SN = 'SN'
    SNDZ = 'SNDZ'
    SNDZRA = 'SNDZRA'
    SNPL = 'SNPL'
    SNPLRA = 'SNPLRA'
    SNPLSG = 'SNPLSG'
    SNRA = 'SNRA'
    SNRADZ = 'SNRADZ'
    SNRAPL = 'SNRAPL'
    SNRASG = 'SNRASG'
    SNSG = 'SNSG'
    SNSGPL = 'SNSGPL'
    SNSGRA = 'SNSGRA'
    SQ = 'SQ'
    SS = 'SS'
    TS = 'TS'
    TSGR = 'TSGR'
    TSGRRA = 'TSGRRA'
    TSGRRASN = 'TSGRRASN'
    TSGRSN = 'TSGRSN'
    TSGRSNRA = 'TSGRSNRA'
    TSGS = 'TSGS'
    TSGSRA = 'TSGSRA'
    TSGSRASN = 'TSGSRASN'
    TSGSSN = 'TSGSSN'
    TSGSSNRA = 'TSGSSNRA'
    TSRA = 'TSRA'
    TSRAGR = 'TSRAGR'
    TSRAGRSN = 'TSRAGRSN'
    TSRAGS = 'TSRAGS'
    TSRAGSSN = 'TSRAGSSN'
    TSRASN = 'TSRASN'
    TSRASNGR = 'TSRASNGR'
    TSRASNGS = 'TSRASNGS'
    TSSN = 'TSSN'
    TSSNGR = 'TSSNGR'
    TSSNGRRA = 'TSSNGRRA'
    TSSNGS = 'TSSNGS'
    TSSNGSRA = 'TSSNGSRA'
    TSSNRA = 'TSSNRA'
    TSSNRAGR = 'TSSNRAGR'
    TSSNRAGS = 'TSSNRAGS'
    TSUP = 'TSUP'
    UP = 'UP'
    VA = 'VA'
    VCBLDU = 'VCBLDU'
    VCBLSA = 'VCBLSA'
    VCBLSN = 'VCBLSN'
    VCDS = 'VCDS'
    VCFC = 'VCFC'
    VCFG = 'VCFG'
    VCPO = 'VCPO'
    VCSH = 'VCSH'
    VCSS = 'VCSS'
    VCTS = 'VCTS'
    VCVA = 'VCVA'
    FZRASN = 'FZRASN'
    FZRAPL = 'FZRAPL'
    FZRASG = 'FZRASG'
    FZDZSG = 'FZDZSG'
    FZDZSN = 'FZDZSN'
    FZDZPL = 'FZDZPL'
    MINUSFZRASN = '-FZRASN'
    MINUSFZRAPL = '-FZRAPL'
    MINUSFZRASG = '-FZRASG'
    MINUSFZDZSG = '-FZDZSG'
    MINUSFZDZSN = '-FZDZSN'
    MINUSFZDZPL = '-FZDZPL'
    PLUSFZRASN = '+FZRASN'
    PLUSFZRAPL = '+FZRAPL'
    PLUSFZRASG = '+FZRASG'
    PLUSFZDZSG = '+FZDZSG'
    PLUSFZDZSN = '+FZDZSN'
    PLUSFZDZPL = '+FZDZPL'


class Weather(BaseModel):
    """Weather type"""
    weather1: WeatherPhenomena | Nsw
    weather2: WeatherPhenomena | None = None
    weather3: WeatherPhenomena | None = None


class Wind(BaseModel):
    """Wind type"""
    direction: int | str
    gust: int | str | None = None
    speed: int | str
    unit: Knots


class BaseForecast(BaseModel):
    """BaseForecast type"""
    cavOK: bool | None = None
    cloud: Clouds | None = None
    valid: Valid
    visibility: Visibility | None = None
    weather: Weather | None = None
    wind: Wind

    @field_validator('visibility')
    def mutually_exclusive(cls, v, info):
        '''Make sure fields cavOK and visibility are mutually exclusive'''
        if info.data.get('cavOK') is not None and v:
            raise ValueError("'cavOK' and 'visibility' are mutually exclusive.")
        return v


class DraftBaseForecast(BaseModel):
    """Draft BaseForecast type - make all fields optional"""
    cavOK: bool | None = None
    cloud: Clouds | None = None
    valid: Valid | None = None
    visibility: Visibility | None = None
    weather: Weather | None = None
    wind: Wind | None = None


class Change(str, Enum):
    """Change type"""
    FM = 'FM'
    BECMG = 'BECMG'
    TEMPO = 'TEMPO'


class Probability(str, Enum):
    """Probability type"""
    PROB30 = 'PROB30'
    PROB40 = 'PROB40'


class DraftChangeGroup(BaseModel):
    """Draft ChangeGroup type - make all fields optional"""
    cavOK: bool | None = None
    cloud: Clouds | None = None
    visibility: Visibility | None = None
    weather: Weather | None = None
    wind: Wind | None = None
    change: Change | None = None
    probability: Probability | None = None
    valid: Valid | None = None


class ChangeGroup(BaseModel):
    """ChangeGroup type - valid required, extra validation"""
    cavOK: bool | None = None
    cloud: Clouds | None = None
    visibility: Visibility | None = None
    weather: Weather | None = None
    wind: Wind | None = None
    change: Change | None = None
    probability: Probability | None = None
    valid: Valid

    @field_validator('probability')
    @classmethod
    def mutually_exclusive(cls, v, info):
        '''Check compatibility of change and probability'''
        # if probability and change can only BOTH
        # be NOT None if change is equal to TEMPO
        if v and info.data.get(
                'change') and not info.data.get('change') == 'TEMPO':
            raise ValueError(
                "A probability is only allowed in combination with TEMPO or an empty Change input"
            )
        # also check that either probability or change is NOT NONE,
        # they cannot BOTH be None
        if not v and not info.data.get('change'):
            raise ValueError(
                "Provided TAF cannot be published due to missing data (change or prob are missing)."
            )
        return v


class MessageType(str, Enum):
    """MessageType type"""
    ORG = 'ORG'
    AMD = 'AMD'
    COR = 'COR'
    CNL = 'CNL'


class TafBase(BaseModel):
    """Base model for a Taf body (not strict)"""
    issueDate: JsonDate | None = None
    baseTime: JsonDate
    location: str
    messageType: MessageType
    uuid: str
    validDateEnd: JsonDate
    validDateStart: JsonDate
    status: Status
    previousId: str | None = None
    previousValidDateStart: JsonDate | None = None
    previousValidDateEnd: JsonDate | None = None


class TafDraft(TafBase):
    """Non-strict version of TafBase for draft Tafs"""
    changeGroups: list[DraftChangeGroup] | None = None
    baseForecast: DraftBaseForecast | None = None


class TafPublish(TafBase):
    """Strict version of TafBase for publishing"""
    changeGroups: list[ChangeGroup] | None = None
    baseForecast: BaseForecast

    @field_validator("changeGroups")
    def validate_contents(cls, v):
        """Check that contents of changeGroups are valid"""
        # pylint: disable=no-self-argument
        if v is not None:
            for group in v:
                if not group:
                    raise ValueError(
                        "Provided TAF cannot be published because" \
                            "a changegroup cannot be None or empty"
                    )
        return v


class TafInterface(BaseModel):
    """Model for interacting with the database"""
    taf: TafDraft | TafPublish
    creationDate: JsonDate | None = None
    editor: str | None = None


class DataToFE(BaseModel):
    """Model for returning data to the FE"""
    # if you would use "TafDraft | Tafpublish"
    # instead of "dict" for the taf return type,
    # you'd run into datetime string formatting
    # issues, therefore use "dict" for now
    # --> this might be solved by switching to
    #     pydantic V2
    taf: TafDraft | TafPublish
    creationDate: JsonDate
    editor: str | None = None
    canbe: list[CanBe]

    class ConfigDict:
        """JSON encoding of datetime"""
        # pylint: disable=too-few-public-methods
        json_encoders = {
            datetime: lambda v: datetime.strftime(v, '%Y-%m-%dT%H:%M:%SZ')
        }


class PayloadFromFE(BaseModel):
    """Model for interacting with the FE, that sends different fields"""
    taf: TafPublish | TafDraft
    changeStatusTo: Status | None = None
    editor: str | None = None

    @field_validator('changeStatusTo')
    def publish_or_draft(cls, v, info):
        "Validate the contents of the taf field depending on status"
        if v in {
                Status.PUBLISHED, Status.CORRECTED, Status.AMENDED,
                Status.CANCELLED
        }:
            try:
                TafPublish(**info.data.get("taf").model_dump())
            except (ValidationError, AttributeError) as err:
                raise ValueError(str(err)) from err
        else:
            try:
                TafDraft(**info.data.get("taf").model_dump())
            except (ValidationError, AttributeError) as err:
                raise ValueError(str(err)) from err
        return v


class TafDbModel(SQLModel, table=True):
    "Class representing TAFs stored in database table tafs"

    __tablename__ = 'taftable'

    taf_id: str = Field(primary_key=True)
    creationDate: JsonDate
    baseTime: JsonDate
    editor: str | None
    taf: dict = Field(default={}, sa_column=Column(JSON))
    superseded: bool = Field(sa_column=Column(Boolean), default=False)

    def __init__(self, data: TafInterface) -> None:
        super().__init__()
        if data.creationDate is not None:
            self.creationDate = data.creationDate
        self.taf = data.taf.model_dump()
        self.taf_id = self.taf['uuid']
        self.baseTime = self.taf["baseTime"]
        self.editor = data.editor

    def update(self, data: TafInterface, taf_id: str) -> None:
        """Updates the contents of the TAF with the provided data"""
        self.taf = dict(data.taf, uuid=taf_id)  #Set id in taf json

    def update_editor(self, editor: str | None) -> None:
        """Updates the editor of the TAF"""
        self.editor = editor
