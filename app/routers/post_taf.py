# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""Implementation of the TAF lifecycle related to post TAF"""
import logging
import uuid
from datetime import datetime, timedelta, timezone

from fastapi import HTTPException
from pydantic import ValidationError

from app.crud.errors import CreateError, DeleteError, ReadError, UpdateError
from app.crud.taf import CRUD
from app.models import CanBe, PayloadFromFE, Status, TafBase, TafDbModel, \
    TafInterface, TafPublish
from app.routers.warnings import EditorWarning
from app.utils.taf_lifecycle import get_allowed_actions, set_taf_config
from app.utils.taf_publisher import TAFPublisher
from app.utils.taf_utils import get_lineno


def handle_post_error(message: str,
                      code: int,
                      line: str,
                      previous: BaseException | None = None):
    "Handle error by logging it and raising an exception"
    logging.error(message, extra={"line": line})
    raise HTTPException(status_code=code, detail=message) from previous


taf_config, airports = set_taf_config()


def is_expired(taf: TafBase, reference_timestamp: datetime) -> bool:
    "Check if a TAF is expired"
    expiry_timestamp = taf.baseTime.replace(tzinfo=timezone.utc) + timedelta(
        hours=int(airports[taf.location]['period']))
    return expiry_timestamp < reference_timestamp


def requires_publish(new_status: Status) -> bool:
    "Check if a TAF requires publication"
    return new_status not in (Status.DRAFT, Status.DRAFT_AMENDED,
                              Status.DRAFT_CORRECTED)


def is_allowed_status_change(status: Status, new_status: Status) -> bool:
    "Returns True if the changes from status old_status to new_status is allowed, False otherwise"
    # pylint: disable=too-many-return-statements
    allowed_actions: list[CanBe] = get_allowed_actions(status)
    if new_status == Status.DRAFT:
        return CanBe.DRAFTED in allowed_actions
    if new_status == Status.PUBLISHED:
        return CanBe.PUBLISHED in allowed_actions
    if new_status == Status.AMENDED:
        return CanBe.AMENDED in allowed_actions
    if new_status == Status.DRAFT_AMENDED:
        return CanBe.AMENDED in allowed_actions
    if new_status == Status.CORRECTED:
        return CanBe.CORRECTED in allowed_actions
    if new_status == Status.DRAFT_CORRECTED:
        return CanBe.CORRECTED in allowed_actions
    if new_status == Status.CANCELLED:
        return CanBe.CANCELLED in allowed_actions
    return False


def requires_superseding(old_status: Status, new_status: Status) -> bool:
    "Check if TAF must be superseded"
    return (old_status != Status.NEW and old_status != Status.DRAFT and
            old_status != Status.DRAFT_AMENDED and
            old_status != Status.DRAFT_CORRECTED and
            new_status != Status.DRAFT_AMENDED and
            new_status != Status.DRAFT_CORRECTED)


def mark_as_superseded(old_data: TafDbModel, crud_session: CRUD) -> bool:
    "Mark as superseded to hide from FE"
    try:
        old_data.superseded = True
        # update TAF
        # TAF should be in TafInterface format for updating
        inner_taf = TafPublish.model_validate(old_data.taf)

        crud_session.update(
            old_data,
            TafInterface(taf=inner_taf,
                         creationDate=old_data.creationDate,
                         editor=old_data.editor))
        return True

    except (UpdateError, ValidationError) as err:
        handle_post_error("Error while marking TAF as superseded.", 500,
                          get_lineno(), err)
    return False


def requires_draft_deletion(new_status: Status) -> bool:
    "Check if the drafts for a given TAF must be deleted"
    return new_status in (Status.CANCELLED, Status.AMENDED, Status.CORRECTED)


def find_and_supersede_previous(parent_uuid: str, crud_session: CRUD) -> bool:
    "Find previous TAF and mark as superseded"
    try:
        element_found: TafDbModel | None = crud_session.read_one(parent_uuid)
    except ReadError as err:
        handle_post_error("Error while retrieving TAF from database.", 500,
                          get_lineno(), err)
    # We should have found one item, no items found means an error
    if element_found is None:
        handle_post_error("Provided TAF does not have a valid previous UUID.",
                          400, get_lineno())
        return False

    return mark_as_superseded(element_found, crud_session)


def can_overwrite_taf(old_status: Status, new_status: Status) -> bool:
    "Situations where the previous status was DRAFT or NEW"
    return ((new_status in (Status.DRAFT, Status.DRAFT_AMENDED,
                            Status.DRAFT_CORRECTED)) and
            new_status == old_status) or (old_status == Status.NEW) or (
                new_status == Status.PUBLISHED and old_status
                == Status.DRAFT) or (new_status == Status.AMENDED and
                                     old_status == Status.DRAFT_AMENDED) or (
                                         new_status == Status.CORRECTED and
                                         old_status == Status.DRAFT_CORRECTED)


def delete_drafts(previous_uuid: str, crud_session: CRUD):
    "Delete drafts of latest TAFs"

    if isinstance(all_tafs := crud_session.read_many(), list):
        for item in all_tafs:
            if 'previousId' in item.taf:
                if item.taf['previousId'] == previous_uuid and (
                        item.taf['status'] == Status.DRAFT_AMENDED or
                        item.taf['status'] == Status.DRAFT_CORRECTED):
                    try:
                        crud_session.delete(item.taf['uuid'])
                    except DeleteError as err:
                        handle_post_error(
                            "Error while deleting TAF from the database", 500,
                            get_lineno(), err)


async def publish(taf: TafPublish, user: str) -> None:
    '''Publish the TAF as TAC or IWXXM 3'''
    publisher = TAFPublisher()
    success, messages = await publisher.publish_taf(taf, user)
    if success is False:
        raise HTTPException(status_code=400, detail=str("\n".join(messages)))


async def manage_post_taf(payload: PayloadFromFE, username: str,
                          crud_session: CRUD) -> TafDbModel | None:
    "Handle the lifecycle when a TAF is posted"
    # pylint: disable=too-many-statements,too-many-branches

    # Ref time used for testing the function with mock data and past dates
    reference_timestamp = datetime.now(timezone.utc)

    # Check if the taf is expired, in that case abort post operation
    if is_expired(payload.taf, reference_timestamp):
        handle_post_error("Cannot make updates to an expired TAF", 400,
                          get_lineno())

    # UUID is a requirement
    if payload.taf.uuid is None:
        err_msg = "Provided TAF does not have a valid UUID"
        handle_post_error(err_msg, 400, get_lineno())

    # Retrieve from database
    try:
        if not isinstance(old_data := crud_session.read_one(payload.taf.uuid),
                          TafDbModel):
            err_msg = "Error while retrieving TAF from database"
            handle_post_error(err_msg, 400, get_lineno())
    except ReadError as err:
        err_msg = "Error while retrieving TAF from database"
        handle_post_error(err_msg, 400, get_lineno(), err)

    # Check if the operation is allowed (still the editor and status change allowed)
    # Can only update the TAF if you are currently assigned as the editor
    if old_data is not None and payload.changeStatusTo is not None:
        if (old_data.editor is not None) and (old_data.editor != username):
            logging.info("Trying to update locked taf")
            # This should remain a warning message for compatibility with FE
            raise EditorWarning(
                status_code=400,
                detail="You are no longer the editor for this TAF")

        old_status = old_data.taf['status']
        new_status = payload.changeStatusTo
        if new_status and not is_allowed_status_change(old_status, new_status):
            # API call error
            handle_post_error("Status change not allowed", 500, get_lineno())

        # Operation is allowed
        payload.editor = username
        # If we are further modifying a DRAFT or publishing after it,
        # we can overwrite the database item.
        # We can also overwrite items with NEW status
        # In this case, there will be no other drafts that we need to delete
        if new_status and can_overwrite_taf(old_status, new_status):
            modified_data = payload
            modified_data.taf.status = new_status
            try:
                logging.info(
                    f"Status change, new status: {modified_data.taf.status}")
                set_editor = modified_data.editor
                if modified_data.editor and requires_publish(new_status):
                    # First try and publish item before updating the db
                    modified_data.taf.issueDate = datetime.now(timezone.utc)
                    # Last validation before publishing
                    if isinstance(modified_data.taf, TafPublish):
                        await publish(modified_data.taf, modified_data.editor)
                        crud_session.update_editor(old_data, None)

                response = crud_session.update(
                    old_data,
                    TafInterface(taf=modified_data.taf,
                                 creationDate=datetime.now(timezone.utc),
                                 editor=set_editor))
                logging.info("Status change successfully done")
                # The publish and update worked, now see if we need to delete/supersede
                if requires_publish(new_status):
                    # TODO, Improve readability of the cases that require superseding
                    if requires_superseding(old_status, new_status):
                        mark_as_superseded(old_data, crud_session)
                    elif payload.taf.previousId and new_status in (
                            Status.AMENDED, Status.CORRECTED):
                        # We might still need to supersede the TAF preceding the current one
                        find_and_supersede_previous(payload.taf.previousId,
                                                    crud_session)
                    if payload.taf.previousId and requires_draft_deletion(
                            new_status):
                        delete_drafts(payload.taf.previousId, crud_session)
                return response
            except UpdateError as err:
                err_msg = "Error while saving TAF"
                handle_post_error(err_msg, 400, get_lineno(), err)

    if payload.changeStatusTo is None:
        handle_post_error("Error: missing changeStatusTo field", 400,
                          get_lineno())
        return None

    # All other cases, we will mark the old TAF as superseded and create a new TAF in the database
    if not isinstance(
            new_data := TafInterface(creationDate=datetime.now(timezone.utc),
                                     editor=username,
                                     taf=payload.taf), TafInterface):
        handle_post_error("Error: creating new TAF in database", 400,
                          get_lineno())
    new_data.taf.uuid = str(uuid.uuid1())
    new_data.taf.status = new_status

    if requires_publish(new_status):
        new_data.editor = None
        # Always set issuedate for now
        new_data.taf.issueDate = datetime.now(timezone.utc)
        # Only if status is amended, update validDateStart value to now
        # (which is equal to issueDate)
        if new_data.taf.status == Status.AMENDED:
            # set microseconds to 0 for correct deserialization of date
            new_data.taf.validDateStart = new_data.taf.issueDate.replace(
                microsecond=0, tzinfo=timezone.utc)
    else:
        # Do not propagate issueDate from the previous taf this draft modifies
        new_data.taf.issueDate = None
    if isinstance(old_data, TafDbModel):
        # convert dates to datetime-object to set date attributes with correct types
        setattr(
            new_data.taf, 'previousValidDateStart',
            datetime.strptime(old_data.taf['validDateStart'],
                              "%Y-%m-%dT%H:%M:%SZ"))
        setattr(
            new_data.taf, 'previousValidDateEnd',
            datetime.strptime(old_data.taf['validDateEnd'],
                              "%Y-%m-%dT%H:%M:%SZ"))
        setattr(new_data.taf, 'previousId', old_data.taf['uuid'])

    # If requires_publish, try to publish new TAF first before setting TAF as published in table
    if requires_publish(new_status):
        taf_to_publish = TafPublish.model_validate(new_data.taf)
        await publish(taf_to_publish, username)
    try:
        response = crud_session.create(new_data)
    except (CreateError, ValidationError) as err:
        handle_post_error("Error while saving TAF to database", 500,
                          get_lineno(), err)

    if requires_superseding(old_status, new_status) and isinstance(
            old_data, TafDbModel):
        if not mark_as_superseded(old_data, crud_session):
            handle_post_error(
                "Error while superseding after saving TAF to database", 500,
                get_lineno())
    if new_data.taf.previousId and requires_draft_deletion(new_status):
        delete_drafts(new_data.taf.previousId, crud_session)
    return response
