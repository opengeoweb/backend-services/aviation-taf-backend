# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""Router for TAF related routes"""
from __future__ import annotations

import json
import logging

from fastapi import APIRouter, Depends, Header, HTTPException, Request, \
    Response
from fastapi.responses import PlainTextResponse
from pydantic import ValidationError
from sqlmodel import Session

from app.crud.errors import ReadError, UpdateError
from app.crud.taf import CRUD
from app.db import get_session
from app.models import CanBe, DataToFE, PayloadFromFE, Status, TafBase, \
    TafDbModel, TafPublish
from app.routers.post_taf import manage_post_taf
from app.routers.warnings import EditorWarning
from app.utils.taf_lifecycle import get_allowed_actions, set_taf_config
from app.utils.taf_utils import get_lineno, taf2tac, taf2tac_adverse
from app.utils.utils import format_data_for_fe

router = APIRouter()


@router.patch("/taf", status_code=200)
async def patch_taf(data: PayloadFromFE,
                    response: Response,
                    force: bool = False,
                    username: str | None = Header(alias='Geoweb-Username',
                                                  default=None),
                    session: Session = Depends(get_session)):
    "Lock or unlock a TAF"
    logging.info('Received request: PATCH TAF')

    _check_authenticated(response, username)

    # Get the UUID of the existing TAF, send the frontend
    taf_id = data.taf.uuid
    logging.info(f'Received patch_taf with TAF uuid "{taf_id}"')

    # load existing TAF
    crud = CRUD(session)
    try:
        logging.info("Checking for TAF id %s", taf_id)
        tafdb: TafDbModel | None = crud.read_one(taf_id)
    except ReadError as err:
        err_msg = "Error while retrieving TAF from database"
        logging.error(err_msg, extra={"line": get_lineno()})
        raise HTTPException(status_code=500, detail=err_msg) from err
    if tafdb is None:
        err_msg = "TAF not found"
        logging.error(err_msg, extra={'taf_id': taf_id})
        raise HTTPException(status_code=400, detail=err_msg)

    logging.info(f'Received patch_taf with authenticated username "{username}"')
    logging.info(f'The PATCH TAF has editor [{data.editor}]')
    logging.info(f'The DB TAF has editor [{tafdb.editor}]')

    if not data.editor and tafdb.editor == username:
        # Unlock your own TAF, set editor to None.
        try:
            logging.info('Update taf with no user ("editor":null)')
            crud.update_editor(tafdb, None)
        except UpdateError as err:
            logging.info('ERROR: Unable to update taf with user None')
            raise HTTPException(
                status_code=400,
                detail="Could not unlock TAF, unable to remove TAF editor"
            ) from err
        return

    if data.editor == username:

        # if force=false and de DB TAF has an editor that is different than the username,
        # raise an 409 error
        if not force and tafdb.editor and tafdb.editor != username:
            msg = "Cannot assign editor of TAF that is being edited " +\
                                "by another user."
            raise HTTPException(status_code=409, detail=msg)

        # Lock TAF to the current user, the TAF should be already unlocked (editor is None)
        try:
            logging.info(f'Update taf with user {tafdb.editor} to {username})')
            crud.update_editor(tafdb, username)
        except UpdateError as err:
            raise HTTPException(status_code=400,
                                detail="Could not lock TAF") from err
        return

    # None of the conditions above was met, throw Error
    raise HTTPException(status_code=400, detail="Could not lock/unlock TAF")


@router.get("/taf/{taf_id}",
            response_model=DataToFE,
            response_model_exclude_unset=True)
async def get_taf(taf_id: str,
                  response: Response,
                  username: str | None = Header(alias='Geoweb-Username',
                                                default=None),
                  session: Session = Depends(get_session)):
    "Get a TAF"
    logging.info('Received request: GET TAF')

    _check_authenticated(response, username)

    # load existing TAF
    crud = CRUD(session)
    try:
        logging.info("Checking for TAF id %s", taf_id)
        data: TafDbModel | None = crud.read_one(taf_id)
    except ReadError as err:
        err_msg = "Error while retrieving TAF from database"
        logging.error(err_msg, extra={"line": get_lineno()})
        raise HTTPException(status_code=500, detail=err_msg) from err
    if data is None:
        raise HTTPException(status_code=400, detail='Not found')

    # Convert TafDbModel to DataToFE
    try:
        return format_data_for_fe(data, get_allowed_actions(data.taf['status']))
    except ValidationError as err:
        logging.warning("Invalid TAF retrieved from database",
                        extra={
                            "info": str(err),
                            "taf_id": taf_id
                        })


@router.post("/taf")
async def post_taf(response: Response,
                   data: PayloadFromFE,
                   username: str | None = Header(alias='Geoweb-Username',
                                                 default=None),
                   session: Session = Depends(get_session)):
    "Store a TAF"
    logging.info('Received request: POST TAF')

    _check_authenticated(response, username)

    crud = CRUD(session)

    # Retrieve from database, uuid should be a preexisting one
    try:
        logging.info('Checking for TAF id %s', data.taf.uuid)
        old_taf: TafDbModel | None = crud.read_one(data.taf.uuid)
    except ReadError as err:
        err_msg = "Error while retrieving TAF from database"
        logging.error(err_msg, extra={"line": get_lineno()})
        raise HTTPException(status_code=500, detail=err_msg) from err

    if old_taf is None:
        err_msg = "Error while retrieving TAF from database"
        logging.error(err_msg, extra={"line": get_lineno()})
        raise HTTPException(status_code=400, detail=err_msg)

    if old_taf.editor != username:
        ## guarantee compatibility with current/old TAF BE
        ## and FE here. With a code 400 & warning message,
        ## a warning error should show up
        raise EditorWarning(status_code=400,
                            detail="You are no longer the editor for this TAF")
    if username is not None:
        await manage_post_taf(payload=data,
                              username=username,
                              crud_session=crud)

    # set location header
    response.headers['Location'] = router.url_path_for('get_taf',
                                                       taf_id=data.taf.uuid)


@router.post("/taf2tac",
             responses={
                 200: {
                     "content": {
                         "text/plain": {
                             "schema": ""
                         },
                         "application/json": {}
                     },
                     "description": "Return TAF as text or JSON",
                 }
             })
async def post_taf2tac(data: TafPublish, request: Request):
    "Convert a TAF JSON to a TAC message"

    # display filtered data here
    # could also remove this log statement
    logging.info(f"Received taf2tac with json: {data.model_dump()}")

    accept_header = request.headers.get('Accept')
    if accept_header and 'application/json' in accept_header:
        try:
            return Response(json.dumps(await taf2tac_adverse(data)),
                            headers={'Content-Type': 'application/json'})
        except Exception as err:
            raise HTTPException(status_code=400, detail=str(err)) from err
    else:
        try:
            return PlainTextResponse(await taf2tac(data))
        except Exception as err:
            raise HTTPException(status_code=400, detail=str(err)) from err


def adjust_allowed_actions(session: Session, taf: TafBase) -> list[CanBe]:
    "Remove allowed actions when related draft exists"
    allowed_actions = get_allowed_actions(taf.status)

    # Check if a draft amendment or correction exists
    draft_amendment_exists = CRUD(session).has_draft(taf, Status.DRAFT_AMENDED)
    draft_correction_exists = CRUD(session).has_draft(taf,
                                                      Status.DRAFT_CORRECTED)
    if taf.status in {
            Status.PUBLISHED, Status.AMENDED, Status.CORRECTED, Status.CANCELLED
    }:
        if draft_amendment_exists:
            allowed_actions.remove(CanBe.AMENDED)
    if taf.status in {Status.PUBLISHED, Status.AMENDED, Status.CORRECTED}:
        if draft_correction_exists:
            allowed_actions.remove(CanBe.CORRECTED)

    return allowed_actions


@router.get("/taflist",
            response_model=list[DataToFE],
            response_model_exclude_unset=True)
async def get_taflist(response: Response,
                      username: str | None = Header(default=None,
                                                    alias='Geoweb-Username'),
                      session: Session = Depends(get_session)):
    "Handler for retrieving a list of TAFs"
    logging.info('Received request: GET TAFLIST')

    _check_authenticated(response, username)

    # get view-tafs for user
    # calculate a time cutoff as the base time of

    _, airports = set_taf_config()

    try:
        query: list[TafDbModel] | None = CRUD(session).read_visible(airports)
    except ReadError as err:
        err_msg = "Error while retrieving taflist from database"
        logging.error(err_msg, extra={"line": get_lineno()})
        raise HTTPException(status_code=500, detail=err_msg) from err

    if query is None:
        raise HTTPException(
            status_code=400,
            detail=str("Database error trying to retrieve TAFs"))

    # convert TAFs to DataToFE
    tafs_from_be: list[DataToFE] = []
    for item in query:
        try:
            tafs_from_be.append(
                format_data_for_fe(
                    item, adjust_allowed_actions(session, TafBase(**item.taf))))
        except ValidationError as err:
            logging.warning("Invalid TAF retrieved from database",
                            extra={
                                "info": str(err),
                                "taf_id": item.taf_id
                            })
    return tafs_from_be


def _check_authenticated(response: Response,
                         username: str | None = None) -> None:
    """Checks if user is authenticated"""
    if not username:
        raise HTTPException(status_code=401, detail="Not authenticated")
    response.headers['username'] = username
