# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""
This modules defines the base endpoints for this application.
"""
import logging

from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import PlainTextResponse
from sqlmodel import Session

from app.config import settings
from app.db import get_session
from app.utils.health_check import check_health

router = APIRouter()


@router.get("/")
async def get_root():
    """Returns a welcome message"""
    return PlainTextResponse('GeoWeb Aviation TAF API')


@router.get('/healthcheck')
async def get_healthcheck(session: Session = Depends(get_session)):
    """Healthcheck endpoint

    Returns:
        JSON containing the healthcheck status

    Examples:
        Successful response::

            {
                "status": "OK",
                "service": "TAF",
                "database": {
                    "status": "OK",
                    "taf-table": {
                        "status": "OK"
                    }
                },
                "message-service": {
                    "status": "OK"
                },
                "publisher": {
                    "status": "OK"
                }
            }
    Raises:
        HTTPException: If any healthcheck fails
    """
    message: dict = {
        'status': 'UNKNOWN',
        'service': 'TAF',
        "database": {
            "status": "UNKNOWN",
            "taf-table": {
                "status": "UNKNOWN"
            }
        },
        "message-service": {
            "status": "UNKNOWN"
        },
        "publisher": {
            "status": "UNKNOWN"
        }
    }
    if await check_health(session, message):
        message["status"] = "OK"
    else:
        message["status"] = "FAILED"
        logging.error(message)
        raise HTTPException(status_code=500, detail=message)

    logging.info(message)
    return message


@router.get("/version")
async def get_version():
    """Returns the application's version"""
    return {'version': settings.version}


@router.get("/aviversion")
async def get_aviversion():
    """Returns the avi-messageconverter's version"""
    return {'version': settings.avi_version}
