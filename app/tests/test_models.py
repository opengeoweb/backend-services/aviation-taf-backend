# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"Models mapping to the database"
import json
from datetime import datetime

import pytest
import pytz

from app.models import TafDbModel, TafInterface
from app.utils.utils import DATE_FORMAT

# pylint: disable=redefined-outer-name


@pytest.fixture
def data() -> TafDbModel:
    """Returns a TAF fixture with status NEW."""
    mockdata = {
        "uuid": "unique-id",
        "creationDate": "2023-01-28T00:00:00Z",
        "canbe": ["DRAFTED", "DISCARDED", "PUBLISHED"],
        "taf": {
            "uuid": "unique-id",
            "baseForecast": {
                "valid": {
                    "end": "2023-01-29T06:00:00Z",
                    "start": "2023-01-28T00:00:00Z"
                }
            },
            "baseTime": "2023-01-28T00:00:00Z",
            "location": "TNCB",
            "messageType": "ORG",
            "status": "NEW",
            "validDateEnd": "2023-01-29T06:00:00Z",
            "validDateStart": "2023-01-28T00:00:00Z"
        },
        "editor": ""
    }
    return TafDbModel(TafInterface(**mockdata))  #type: ignore


def test_init_taf(data: TafDbModel) -> None:
    """Tests that derived attributes are set."""
    assert data.taf_id == "unique-id"
    assert data.creationDate == pytz.utc.localize(
        dt=datetime.strptime("2023-01-28T00:00:00Z", DATE_FORMAT))

    assert data.baseTime == pytz.utc.localize(
        datetime.strptime("2023-01-28T00:00:00Z", DATE_FORMAT))
    assert data.editor == ""


def test_update_taf(data: TafDbModel) -> None:
    """Tests that derived attributes are updated."""
    mockdata = {
        "uuid": "unique-id",
        "creationDate": "2023-01-28T00:00:00Z",
        "canbe": ["CANCELLED", "AMENDED", "CORRECTED", "DRAFTED"],
        "taf": {
            "uuid": "unique-id",
            "baseTime": "2023-01-28T00:00:00Z",
            "validDateStart": "2023-01-28T00:00:00Z",
            "validDateEnd": "2023-01-29T06:00:00Z",
            "location": "TNCB",
            "status": "PUBLISHED",
            "type": "NORMAL",
            "messageType": "ORG",
            "baseForecast": {
                "cavOK": True,
                "valid": {
                    "end": "2023-01-29T06:00:00Z",
                    "start": "2023-01-28T00:00:00Z"
                },
                "wind": {
                    "direction": 180,
                    "speed": 10,
                    "unit": "KT"
                }
            }
        },
        "editor": "user1"
    }
    data.update(TafInterface(**mockdata), data.taf_id)  #type: ignore
    # not updated
    assert data.taf_id == "unique-id"
    assert data.taf["uuid"] == "unique-id"
    assert data.editor != "user1"

    # updated
    assert data.taf["status"] == "PUBLISHED"


def test_taf_structure_validation() -> None:
    """Validate the output TAF structure alongside inner structure"""
    with open('app/tests/testdata/amended.json', 'rb') as fh:
        # get the data as dictionary
        TafInterface(**json.load(fh))
    with open('app/tests/testdata/corrected.json', 'rb') as fh:
        # get the data as dictionary
        TafInterface(**json.load(fh))
    with open('app/tests/testdata/draft_amended.json', 'rb') as fh:
        # get the data as dictionary
        TafInterface(**json.load(fh))
    with open('app/tests/testdata/draft_corrected.json', 'rb') as fh:
        # get the data as dictionary
        TafInterface(**json.load(fh))
    with open('app/tests/testdata/draft.json', 'rb') as fh:
        # get the data as dictionary
        TafInterface(**json.load(fh))
    with open('app/tests/testdata/new.json', 'rb') as fh:
        # get the data as dictionary
        TafInterface(**json.load(fh))
    with open('app/tests/testdata/published.json', 'rb') as fh:
        # get the data as dictionary
        TafInterface(**json.load(fh))
    with open('app/tests/testdata/taflist.json', 'rb') as fh:
        # get the data as dictionary
        taflist = json.load(fh)
        for taf in taflist:
            TafInterface(**taf)
