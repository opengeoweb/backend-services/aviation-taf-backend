# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""Test TAF publisher"""

import json
from datetime import datetime, timezone

import pytest
from freezegun import freeze_time

from app.config import settings
from app.models import Status, TafPublish
from app.utils.taf_publisher import TAFPublisher
from app.utils.utils import DATE_FORMAT


def test_taf_publisher_get_status():
    """Check TAFPublisher.get_status function"""
    assert TAFPublisher().get_status(Status.AMENDED) == 'AMD'
    assert TAFPublisher().get_status(Status.PUBLISHED) == ''
    assert TAFPublisher().get_status(Status.CANCELLED) == 'CNL'
    assert TAFPublisher().get_status(Status.CORRECTED) == 'COR'


def test_taf_publisher_get_taf_headertime():
    """Check TAFPublisher.taf_headertime function"""
    with open('app/tests/testdata/taf1.json', 'rb') as test_data:
        taf_body = json.load(test_data)['taf']
        taf = TafPublish(**taf_body)
        taf.baseTime = datetime.strptime(
            "2023-01-28T00:00:00Z", DATE_FORMAT).replace(tzinfo=timezone.utc)
        assert TAFPublisher().get_taf_headertime(taf) == datetime(
            2023, 1, 27, 23, 0).replace(tzinfo=timezone.utc)


def test_taf_publisher_make_tac_header():
    """Check TAFPublisher.make_tac_header function"""
    with open('app/tests/testdata/taf1.json', 'rb') as test_data:
        taf_body = json.load(test_data)['taf']
        taf_body['baseTime'] = "2023-01-28T00:00:00Z"
        taf_body['status'] = Status.CORRECTED
        taf = TafPublish(**taf_body)
        assert TAFPublisher().make_tac_header(taf) == "FTNL99 EHDB 272300 COR\n"


@pytest.mark.asyncio
async def test_taf_publisher_publish_tac(httpx_mock, capsys):
    """Check TAFPublisher.publish_tac function"""

    with freeze_time('2022-01-01T14:55:00Z'):

        with open('app/tests/testdata/taf1.json', 'rb') as test_data:
            taf_body = json.load(test_data)['taf']
            taf_body['baseTime'] = "2023-01-28T00:00:00Z"
            taf_body['status'] = Status.CORRECTED
            taf_body['location'] = "EHAM"
            taf_body['uuid'] = "theuuid"

            taf = TafPublish(**taf_body)

            settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
            settings.aviation_taf_publish_host = 'taf-publisher'

            mock_url_taf2tac = "http://" + settings.geoweb_knmi_avi_messageservices_host
            mock_url_publishtofs = "http://" + settings.aviation_taf_publish_host

            tac_body_to_mock = "TACFROMTAF2TAC"

            httpx_mock.add_response(url=mock_url_taf2tac + "/gettaftac",
                                    method="POST",
                                    text=tac_body_to_mock,
                                    status_code=200)

            httpx_mock.add_response(
                url=mock_url_publishtofs +
                "/publish/TAF_EHAM_2023-01-272300T20220101145500.tac",
                method="POST",
                status_code=200)

            assert await TAFPublisher().publish_tac(taf, 'testuser') is True
            captured = capsys.readouterr()
            assert captured.out == "audit-logging - 2022-01-01T14:55:00Z - " +\
                "theuuid - taf - published - testuser\n"


@pytest.mark.asyncio
async def test_taf_publisher_publish_iwxxm30(httpx_mock, capsys):
    """Check TAFPublisher.publish_iwxxm30function"""

    with freeze_time('2022-01-01T14:55:00Z'):

        with open('app/tests/testdata/taf1.json', 'rb') as test_data:
            taf_body = json.load(test_data)['taf']
            taf_body['baseTime'] = "2023-01-28T00:00:00Z"
            taf_body['status'] = Status.CORRECTED
            taf_body['location'] = "EHAM"
            taf_body['uuid'] = "theuuid"

            taf = TafPublish(**taf_body)

            settings.geoweb_knmi_avi_messageservices_host = 'iwxxm30-converter'
            settings.aviation_taf_publish_host = 'taf-publisher'

            mock_url_taf2iwxxm30 = "http://" + settings.geoweb_knmi_avi_messageservices_host
            mock_url_publishtofs = "http://" + settings.aviation_taf_publish_host

            iwxxm30_body_to_mock = "<xml>tafcontents</xml>"

            httpx_mock.add_response(url=mock_url_taf2iwxxm30 + "/gettafiwxxm30",
                                    method="POST",
                                    text=iwxxm30_body_to_mock,
                                    status_code=200)

            httpx_mock.add_response(
                url=mock_url_publishtofs +
                "/publish/A_LTNL99EHDB272300COR_C_EHAM_20220101145500.xml",
                method="POST",
                status_code=200)

            assert await TAFPublisher().publish_iwxxm30(taf, 'testuser') is True
            captured = capsys.readouterr()
            assert captured.out == "audit-logging - 2022-01-01T14:55:00Z - " + \
                "theuuid - taf - published - testuser\n"
