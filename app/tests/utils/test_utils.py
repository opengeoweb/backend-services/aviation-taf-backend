# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""Tests for TAF utils"""

from app.utils.utils import split_into_lines

SIGMET_TAC = '''ZCZC
WSNL31 EHDB 121456
EHAA SIGMET 1 VALID 121524/121824 EHDB-
EHAA AMSTERDAM FIR EMBD TS OBS AT 1454Z WI N5402 E00440 - N5607 E00440 - N5444 E00808 - N5254 E00630 - N5325 E00500 - N5402 E00440 FL200/400 MOV SSE 100KT WKN=
NNNN'''

SIGMET_TAC_AFTER = '''ZCZC
WSNL31 EHDB 121456
EHAA SIGMET 1 VALID 121524/121824 EHDB-
EHAA AMSTERDAM FIR EMBD TS OBS AT 1454Z WI N5402 E00440 - N5607
E00440 - N5444 E00808 - N5254 E00630 - N5325 E00500 - N5402 E00440
FL200/400 MOV SSE 100KT WKN=
NNNN'''

AIRMET_TAC = '''ZCZC
WANL31 EHDB 121727
EHAA AIRMET 1 VALID 121734/121934 EHDB-
EHAA AMSTERDAM FIR ISOL TS FCST AT 1744Z WI N5423 E00246 - N5423 E00502 - N5136 E00502 - N5136 E00205 - N5255 E00309 - N5423 E00246 - N5423 E00246 FL050 STNR INTSF=
NNNN'''

AIRMET_TAC_AFTER = '''ZCZC
WANL31 EHDB 121727
EHAA AIRMET 1 VALID 121734/121934 EHDB-
EHAA AMSTERDAM FIR ISOL TS FCST AT 1744Z WI N5423 E00246 - N5423
E00502 - N5136 E00502 - N5136 E00205 - N5255 E00309 - N5423 E00246 -
N5423 E00246 FL050 STNR INTSF=
NNNN'''

TAF_TAC = '''FTNL99 EHDB 121808
TAF EHRD 121808Z 1300/1406 20050KT 1000 BR BKN010 BKN020 BKN030
PROB40 TEMPO 1301/1401 20050KT 2000 BR='''

TAF_TAC_AFTER = '''FTNL99 EHDB 121808
TAF EHRD 121808Z 1300/1406 20050KT 1000 BR BKN010 BKN020 BKN030
PROB40 TEMPO 1301/1401 20050KT 2000 BR='''


def check_all_shorter_than_69(full_string):
    """check_all_shorter_than_69"""
    return not any(len(line) > 69 for line in full_string.splitlines())


def test_split_into_lines():
    '''Check that TACs are split correctly '''

    assert (check_all_shorter_than_69(split_into_lines(SIGMET_TAC)))
    assert split_into_lines(SIGMET_TAC) == SIGMET_TAC_AFTER
    assert (check_all_shorter_than_69(split_into_lines(AIRMET_TAC)))
    assert split_into_lines(AIRMET_TAC) == AIRMET_TAC_AFTER
    assert (check_all_shorter_than_69(split_into_lines(TAF_TAC)))
    assert split_into_lines(TAF_TAC) == TAF_TAC_AFTER
