# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""Fixtures for tests"""
from collections.abc import Generator

import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient
from sqlmodel import Session, SQLModel, create_engine
from sqlmodel.pool import StaticPool

from app.db import get_session
from app.main import app as app_for_testing
from app.utils.utils import _pydantic_json_serializer

# pylint: disable=redefined-outer-name


@pytest.fixture
def session():
    """Yields a database session fixture"""
    engine = create_engine('sqlite://',
                           connect_args={"check_same_thread": False},
                           poolclass=StaticPool,
                           json_serializer=_pydantic_json_serializer)
    SQLModel.metadata.create_all(engine)
    with Session(engine) as session:
        yield session


@pytest.fixture
def app() -> Generator[FastAPI]:
    """Yields the application configured in testing mode"""
    yield app_for_testing


@pytest.fixture
def client(app: FastAPI, session: Session) -> Generator[TestClient]:
    """Yields a test client"""

    def get_session_override():
        return session

    app.dependency_overrides[get_session] = get_session_override
    client = TestClient(app)
    yield client
    app.dependency_overrides.clear()


@pytest.fixture
def test_dummytest() -> None:
    """Should at least execute one test."""
    assert True
