# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
# pylint: disable=too-many-lines
"""Module to test TAF-specific routers"""
import json
import logging
import sys
import uuid
from datetime import datetime, timezone
from unittest.mock import patch

import httpx
from fastapi.testclient import TestClient
from freezegun import freeze_time
from sqlmodel import Session

from app.config import settings
from app.crud.errors import ReadError
from app.crud.taf import CRUD
from app.models import PayloadFromFE, Status, TafInterface
from app.utils.utils import DATE_FORMAT

# log stream handling
logger = logging.getLogger()
logger.level = logging.INFO
stream_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(stream_handler)


###########################
## Some helper functions ##
###########################
def str_to_timezone_aware(datestr: str):
    """Turn string into timezone aware datetime object"""
    return datetime.strptime(datestr, DATE_FORMAT).replace(tzinfo=timezone.utc)


def get_taflist(client: TestClient, username: str = 'test-user-1'):
    """Call GET /taflist for use in tests"""
    return client.get('/taflist', headers={'Geoweb-Username': username}).json()


def post_taf(client: TestClient,
             payload: PayloadFromFE,
             username: str = 'test-user-1'):
    """Call POST /taf for use in tests"""
    return client.post('/taf',
                       json=payload,
                       headers={'Geoweb-Username': username})


def patch_taf(client: TestClient,
              payload: PayloadFromFE,
              username: str = 'test-user-1',
              force=False):
    """Call PATCH /taf for use in tests"""
    return client.patch(f'/taf?force={force}',
                        json=payload,
                        headers={'Geoweb-Username': username})


def get_taf(client: TestClient, taf_id: str, username: str = 'test-user-1'):
    """Call GET /taf for use in tests"""
    return client.get(f'/taf/{taf_id}', headers={'Geoweb-Username': username})


def fill_table_for_get_taflist(session: Session):
    '''
    Function to fill table with TAFs from multiple locations
    with previous, current and upcoming timeslots
    '''
    airports = ['EHAM', 'EHBK', 'EHGG']

    crud = CRUD(session)

    with open('app/tests/testdata/expired_published.json', 'rb') as fh2:
        # fill table with expired TAFs
        expired_taf_published = TafInterface(**json.load(fh2))
        expired_taf_published.taf.location = airports[0]
        # generate id
        expired_taf_published.taf.uuid = str(uuid.uuid1())
        crud.create(expired_taf_published)

    with open('app/tests/testdata/placeholder_expired.json', 'rb') as fh3:
        expired_taf_placeholder = TafInterface(**json.load(fh3))
        expired_taf_placeholder.taf.location = airports[1]
        # generate id
        expired_taf_placeholder.taf.uuid = str(uuid.uuid1())
        crud.create(expired_taf_placeholder)

    with open('app/tests/testdata/expired_amended.json', 'rb') as fh4:
        expired_taf_amended = TafInterface(**json.load(fh4))
        expired_taf_amended.taf.location = airports[2]
        # generate id
        expired_taf_amended.taf.uuid = str(uuid.uuid1())
        crud.create(expired_taf_amended)

    # fill table with published TAFs and placeholders for upcoming timeframe
    for airport in airports:
        with open('app/tests/testdata/current_published.json', 'rb') as fh5:
            placeholder_taf = TafInterface(**json.load(fh5))
            # set location
            placeholder_taf.taf.location = airport
            # generate id
            placeholder_taf.taf.uuid = str(uuid.uuid1())
            crud.create(placeholder_taf)
        with open('app/tests/testdata/placeholder_upcoming.json', 'rb') as fh:
            placeholder_taf = TafInterface(**json.load(fh))
            # set location
            placeholder_taf.taf.location = airport
            # generate id
            placeholder_taf.taf.uuid = str(uuid.uuid1())
            crud.create(placeholder_taf)


def has_no_none_fields(data):
    """Check that there are no None fields, except for editor"""
    if isinstance(data, dict):
        for key, value in data.items():
            if key == 'editor':
                continue
            if value is None or not has_no_none_fields(value):
                return False
    elif isinstance(data, list):
        for item in data:
            if not has_no_none_fields(item):
                return False
    return True


def fill_table(session: Session, use_two=False):
    '''
    Function to fill table with one or two placeholder TAFs
    '''
    crud = CRUD(session)
    with open('app/tests/testdata/placeholder_taf1.json', 'rb') as fh1:
        crud.create(TafInterface(**json.load(fh1)))
    if use_two:
        with open('app/tests/testdata/placeholder_taf2.json', 'rb') as fh2:
            crud.create(TafInterface(**json.load(fh2)))


###########################
###### Test taflist  ######
###########################
def test_get_taflist_empty_list(client: TestClient, capsys) -> None:
    """Tests listing tafs, when the database is empty"""

    # redirect stream
    stream_handler.stream = sys.stdout

    # retrieve taflist
    response = client.get('/taflist/',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)

    # check logs
    out, _ = capsys.readouterr()
    assert "Received request: GET TAFLIST" in out


def test_get_taflist_db_error(client: TestClient, capsys) -> None:
    '''Check response if there is an issue with the database'''

    with patch('app.crud.taf.CRUD.read_visible', side_effect=ReadError("mock")):
        response = client.get('/taflist/',
                              headers={'Geoweb-Username': 'test-user-1'})

    # redirect stream
    stream_handler.stream = sys.stdout

    # check response
    assert response.status_code == 500
    assert response.json() == {
        "message": "Error while retrieving taflist from database"
    }

    # check logs
    _, err = capsys.readouterr()
    assert "Received request: GET TAFLIST" in err
    assert "Error while retrieving taflist from database" in err


@freeze_time("2020-10-18T14:55:00.500Z")
def test_get_taflist_ordering(client: TestClient, session: Session):
    '''
    Test the orders of TAFs returned in the taflist.
    Should be sorted by upcoming, current & expired timeslots
    '''

    fill_table_for_get_taflist(session)

    # verify the size of the table
    taflist = get_taflist(client)
    assert len(taflist) == 9

    # verify correct timeslot
    # first three should be for the upcoming timeslot and new
    for taf in taflist[:3]:
        assert taf['taf']['baseTime'] == "2020-10-18T18:00:00Z"
        assert taf['taf']['status'] == Status.NEW
    # tafs 4 - 6 should be for the current timeslot and published
    for taf in taflist[3:6]:
        assert taf['taf']['baseTime'] == "2020-10-18T12:00:00Z"
        assert taf['taf']['status'] == Status.PUBLISHED
    # the last three should be for the previous timeslot and expired
    for taf in taflist[6:]:
        assert taf['taf']['baseTime'] == "2020-10-18T06:00:00Z"
        assert taf['taf']['status'] == Status.EXPIRED


@freeze_time("2020-10-18T14:55:00Z")
def test_get_taflist_ordering_published_upcoming_taf(httpx_mock,
                                                     client: TestClient,
                                                     session: Session, capsys):
    '''
    Check order of TAF upcoming, current & expired timeslots
    after publishing an upcoming TAF
    '''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table_for_get_taflist(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB181700_C_EHAM_20201018145500.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-10-181700T20201018145500.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    username = 'username'

    # save an upcoming TAF as DRAFT
    taflist = get_taflist(client)

    # lock TAF
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    response = patch_taf(client, postbody, username)
    assert response.status_code == 200

    # save DRAFT TAF
    postbody['taf']['baseForecast']['cavOK'] = True
    postbody['changeStatusTo'] = 'DRAFT'
    response = post_taf(client, postbody, username)
    assert response.status_code == 200

    # assert that TAF was saved with status DRAFT
    taflist = get_taflist(client)
    assert len(taflist) == 9
    assert taflist[0]['taf']['status'] == 'DRAFT'

    # check logs processing
    out, _ = capsys.readouterr()
    assert "Default config file" in out
    assert "Updating TAF editor in database" in out
    assert f"Status change, new status: {Status.DRAFT}" in out
    assert "Status change successfully done" in out

    # verify timeslot ordering is still correct after saving DRAFT
    for taf in taflist[:3]:
        assert taf['taf']['baseTime'] == "2020-10-18T18:00:00Z"
    for taf in taflist[3:6]:
        assert taf['taf']['baseTime'] == "2020-10-18T12:00:00Z"
    for taf in taflist[6:]:
        assert taf['taf']['baseTime'] == "2020-10-18T06:00:00Z"

    # publish DRAFT TAF
    postbody['changeStatusTo'] = 'PUBLISHED'
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    response = post_taf(client, postbody, username)
    assert response.status_code == 200

    # assert that TAF was saved with status PUBLISHED
    taflist = get_taflist(client)
    assert len(taflist) == 9
    assert taflist[0]['taf']['status'] == 'PUBLISHED'
    assert taflist[0]['editor'] is None

    # check publishing logs
    out, _ = capsys.readouterr()
    assert "Updating TAF editor in database" in out
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 181700" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    # verify timeslot ordering is still correct after publishing TAF
    for taf in taflist[:3]:
        assert taf['taf']['baseTime'] == "2020-10-18T18:00:00Z"
    for taf in taflist[3:6]:
        assert taf['taf']['baseTime'] == "2020-10-18T12:00:00Z"
    for taf in taflist[6:]:
        assert taf['taf']['baseTime'] == "2020-10-18T06:00:00Z"

    # check that the only null field is 'editor'
    assert has_no_none_fields(taflist)


@freeze_time("2020-10-18T14:55:00Z")
def test_get_taflist_ordering_cancel_taf(httpx_mock, client: TestClient,
                                         session: Session, capsys):
    '''
    Check order of TAF upcoming, current & expired timeslots
    after cancelling a published TAF
    '''

    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table_for_get_taflist(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB181100CNL_C_EHBK_20201018145500.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHBK_2020-10-181100T20201018145500.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    username = 'username'

    # cancel a published TAF
    taflist = get_taflist(client)

    # retrieve TAF and verify that it has been published
    postbody = get_taf(client, taflist[4]['taf']['uuid']).json()
    assert postbody['taf']['status'] == Status.PUBLISHED
    # lock TAF
    postbody['editor'] = username
    response = patch_taf(client, postbody, username)
    assert response.status_code == 200

    # cancel taf
    postbody['changeStatusTo'] = 'CANCELLED'
    response = post_taf(client, postbody, username)
    assert response.status_code == 200

    # check publishing logs
    out, _ = capsys.readouterr()
    assert "Updating TAF editor in database" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 181100 CNL" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    # verify that number of TAFs in database has remained equal
    taflist = get_taflist(client)
    assert len(taflist) == 9
    assert taflist[3]['taf']['status'] == 'CANCELLED'

    # verify timeslot ordering is still correct after cancelling TAF
    for taf in taflist[:3]:
        assert taf['taf']['baseTime'] == "2020-10-18T18:00:00Z"
    for taf in taflist[3:6]:
        assert taf['taf']['baseTime'] == "2020-10-18T12:00:00Z"
    for taf in taflist[6:]:
        assert taf['taf']['baseTime'] == "2020-10-18T06:00:00Z"

    # verify date formatting for previousValidDate... fields
    assert taflist[3]['taf']['previousValidDateStart'] == '2020-10-18T12:00:00Z'
    assert taflist[3]['taf']['previousValidDateEnd'] == '2020-10-19T18:00:00Z'


@freeze_time("2020-10-18T14:55:00Z")
def test_get_taflist_ordering_correcting_taf(httpx_mock, client: TestClient,
                                             session: Session, capsys):
    '''
    Check order of TAF upcoming, current & expired timeslots after
    a TAF has been corrected
    '''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table_for_get_taflist(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB181100COR_C_EHAM_20201018145500.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-10-181100T20201018145500.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    username = 'username'

    # correct a published taf
    taflist = get_taflist(client)
    assert len(taflist) == 9

    # retrieve TAF and verify it has been published
    taf1_uuid = taflist[3]['taf']['uuid']
    postbody = get_taf(client, taf1_uuid).json()
    assert postbody['taf']['status'] == 'PUBLISHED'

    # lock TAF
    postbody['editor'] = username
    response = patch_taf(client, postbody, username)
    assert response.status_code == 200

    # publish corrected TAF
    postbody['changeStatusTo'] = 'CORRECTED'
    response = post_taf(client, postbody, username)
    assert response.status_code == 200

    # check publishing logs
    out, _ = capsys.readouterr()
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 181100 COR" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    # check number of TAFs
    taflist = get_taflist(client)
    assert len(taflist) == 9
    assert taflist[3]['taf']['validDateStart'] == "2020-10-18T12:00:00Z"
    assert taflist[3]['taf']['status'] == "CORRECTED"

    # verify timeslot ordering is still correct after correcting TAF
    for taf in taflist[:3]:
        assert taf['taf']['baseTime'] == "2020-10-18T18:00:00Z"
    for taf in taflist[3:6]:
        assert taf['taf']['baseTime'] == "2020-10-18T12:00:00Z"
    for taf in taflist[6:]:
        assert taf['taf']['baseTime'] == "2020-10-18T06:00:00Z"


@freeze_time("2020-10-18T14:55:00Z")
def test_get_taflist_ordering_amending_taf(httpx_mock, client: TestClient,
                                           session: Session, capsys):
    '''
    Check order of TAF upcoming, current & expired timeslots after
    first a DRAFT_AMENDED has been posted and then after an
    AMENDED TAF has been published
    '''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table_for_get_taflist(session)

    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB181100AMD_C_EHAM_20201018145500.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-10-181100T20201018145500.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    username = 'username'

    # correct a published taf by creating a DRAFT_AMENDED
    taflist = get_taflist(client)

    # retrieve TAF and verify it has been published
    taf1_uuid = taflist[3]['taf']['uuid']
    postbody = get_taf(client, taf1_uuid).json()
    assert postbody['taf']['status'] == Status.PUBLISHED
    # lock TAF
    postbody['editor'] = username
    response = patch_taf(client, postbody, username)
    assert response.status_code == 200

    # publish amended TAF
    assert postbody['taf']['validDateStart'] == "2020-10-18T12:00:00Z"
    postbody['changeStatusTo'] = 'AMENDED'
    response = post_taf(client, postbody, username)
    assert response.status_code == 200

    # check publishing logs
    out, _ = capsys.readouterr()
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 181100 AMD" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    # check number of TAFs
    taflist = get_taflist(client)
    assert len(taflist) == 9
    assert taflist[3]['taf']['validDateStart'] == "2020-10-18T14:55:00Z"
    assert taflist[3]['taf']['status'] == "AMENDED"

    # check order of taflist after publishing TAF
    for taf in taflist[:3]:
        assert taf['taf']['baseTime'] == "2020-10-18T18:00:00Z"
    for taf in taflist[3:6]:
        assert taf['taf']['baseTime'] == "2020-10-18T12:00:00Z"
    for taf in taflist[6:]:
        assert taf['taf']['baseTime'] == "2020-10-18T06:00:00Z"


###########################
##### Test PATCH TAF ######
###########################
@freeze_time("2020-12-17T10:00:00Z")
def test_lock_taf(client: TestClient, session: Session):
    '''Try to lock a TAF'''

    fill_table(session)
    username = "testuser"

    # lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # retrieve locked taf
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    assert postbody['editor'] == username


@freeze_time("2020-12-17T10:00:00Z")
def test_unlock_taf(client: TestClient, session: Session):
    '''Try to lock and unlock a TAF'''

    fill_table(session)
    username = "testuser"

    # lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # retrieve locked taf
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    assert postbody['editor'] == username

    # unlock taf
    postbody['editor'] = None
    patch_taf(client, postbody, username)

    # check taf was unlocked
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    assert postbody['editor'] is None


@freeze_time("2020-12-17T10:00:00Z")
def test_post_locked_taf(client: TestClient, session: Session):
    '''Try to post TAF that was locked by another user'''

    fill_table(session)
    username = "testuser"

    # lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # retrieve locked taf
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    assert postbody['editor'] == username

    # Now try to post TAF with other username
    postbody['editor'] = 'other-user'
    postbody['changeStatusTo'] = 'DRAFT'
    response = post_taf(client, postbody, 'other-user')
    assert "You are no longer the editor for this TAF" in response.text
    assert response.status_code == 400


@freeze_time("2020-12-17T10:00:00Z")
def test_takeover_locked_taf_with_force(client: TestClient, session: Session):
    '''Try to take over TAF that was locked by other user'''

    fill_table(session)
    username1 = "test-user-1"
    username2 = "test-user-2"

    # Lock TAF for first user
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username1
    patch_taf(client, postbody, username1)

    # Verify locking
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    assert postbody['editor'] == username1

    # Take over TAF lock
    postbody['editor'] = username2
    response = patch_taf(client, postbody, username2, True)
    assert response.status_code == 200
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    assert postbody['editor'] == username2


@freeze_time("2020-12-17T10:00:00Z")
def test_takeover_locked_taf_without_force(client: TestClient,
                                           session: Session):
    '''Try to take over TAF that was locked by other user'''

    fill_table(session)
    username1 = "test-user-1"
    username2 = "test-user-2"

    # Lock TAF for first user
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username1
    patch_taf(client, postbody, username1)

    # Verify locking
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    assert postbody['editor'] == username1

    # Take over TAF lock
    postbody['editor'] = username2
    response = patch_taf(client, postbody, username2, False)
    # This should not be possible
    assert response.status_code == 409


@freeze_time("2020-12-17T10:00:00Z")
def test_lock_taf_different_usernames(client: TestClient, session: Session):
    '''Try to lock a TAF where editor name and username are different'''

    fill_table(session)
    username1 = "test-user-1"
    username2 = "test-user-2"

    # Try to lock with with different usernames for
    # authentication header and editor
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username1
    response = patch_taf(client, postbody, username2)
    assert response.status_code == 400
    assert "Could not lock/unlock TAF" in response.text


@freeze_time("2020-12-17T10:00:00Z")
def test_taf_unlock_after_publishing(httpx_mock, client: TestClient,
                                     session: Session):
    '''Verify that a TAF is unlocked after publishing'''

    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    fill_table(session)
    username = "testuser"

    # Lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Verify TAF locked
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    assert postbody['editor'] == username

    # Publish TAF
    postbody['changeStatusTo'] = 'PUBLISHED'
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    response = post_taf(client, postbody, username)
    assert response.status_code == 200

    # Verify TAF is unlocked
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    assert postbody['editor'] is None


###########################
# Test POST TAF lifecycle #
###########################
@freeze_time("2020-12-17T10:00:00Z")
def test_post_draft_taf(client: TestClient, session: Session, capsys):
    '''Test posting a TAF DRAFT'''

    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    taflist = get_taflist(client)
    username = 'username'

    # retrieve TAF
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()

    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # post TAF DRAFT
    postbody['changeStatusTo'] = 'DRAFT'
    post_taf(client, postbody, username)

    # check update logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.DRAFT}" in out
    assert "Status change successfully done" in out

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'DRAFT'
    # The DRAFT will replace the default NEW, leaving us with one element
    assert len(tafs1) == 1


@freeze_time("2020-12-17T10:00:00Z")
def test_publish_taf(httpx_mock, client: TestClient, session: Session, capsys):
    '''Test publishing a NEW TAF'''

    # redirect stream
    stream_handler.stream = sys.stdout
    fill_table(session)

    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    username = 'username'
    taflist = get_taflist(client)

    # retrieve TAF contents
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # publish TAF
    postbody['changeStatusTo'] = 'PUBLISHED'
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    response = post_taf(client, postbody, username)
    assert response.status_code == 200

    # check publishing logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'PUBLISHED'
    assert tafs1[0]['taf']['issueDate'] == '2020-12-17T10:00:00Z'
    assert tafs1[0]['editor'] is None
    # The PUBLISH will replace the default NEW, leaving us with one element
    assert len(tafs1) == 1


@freeze_time("2020-12-17T10:00:00Z")
def test_publish_draft_taf(httpx_mock, client: TestClient, session: Session,
                           capsys):
    '''Test publishing a DRAFT TAF'''

    # redirect stream
    stream_handler.stream = sys.stdout
    fill_table(session)

    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    username = 'username'

    # retrieve TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()

    # lock TAF
    postbody['editor'] = 'username'
    patch_taf(client, postbody, username)

    # post TAF DRAFT
    postbody['changeStatusTo'] = 'DRAFT'
    post_taf(client, postbody, username)

    # retrieve TAF and check contents
    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'DRAFT'
    assert len(tafs1) == 1

    # post TAF PUBLISH
    postbody['changeStatusTo'] = 'PUBLISHED'
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs2 = get_taflist(client)
    assert tafs2[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    # Still one element because the PUBLISHED replaces the DRAFT
    assert len(tafs2) == 1
    assert tafs2[0]['taf']['status'] == 'PUBLISHED'
    assert tafs2[0]['taf']['issueDate'] == '2020-12-17T10:00:00Z'
    assert tafs2[0]['editor'] is None


@freeze_time("2020-12-17T10:00:00Z")
def test_post_cancel_taf(httpx_mock, client: TestClient, session: Session,
                         capsys):
    '''Post and then cancel a TAF'''

    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100CNL_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        is_reusable=True,
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            is_reusable=True,
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            is_reusable=True,
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    username = "testuser"

    # retrieve TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # publish TAF
    postbody['changeStatusTo'] = 'PUBLISHED'
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    post_taf(client, postbody, username)

    # make sure TAF was published
    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'PUBLISHED'
    assert tafs1[0]['taf']['issueDate'] == '2020-12-17T10:00:00Z'
    assert tafs1[0]['editor'] is None

    # again lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # and now CANCEL TAF
    postbody['changeStatusTo'] = 'CANCELLED'
    post_taf(client, postbody, username)

    # check cancelling logs
    out, _ = capsys.readouterr()
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100 CNL" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs2 = get_taflist(client)
    # One element showing, now that the PUBLISHED has been superseded by the CANCELLED
    assert tafs2[0]['taf']['status'] == 'CANCELLED'
    assert tafs2[0]['taf']['issueDate'] == '2020-12-17T10:00:00Z'
    assert len(tafs2) == 1
    assert tafs2[0]['editor'] is None


@freeze_time("2020-12-17T10:00:00Z")
def test_post_two_tafs(httpx_mock, client: TestClient, session: Session,
                       capsys):
    '''Post two TAFs'''
    # pylint: disable = too-many-statements
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session, use_two=True)

    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB170700_C_EHRD_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHRD_2020-12-170700T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            },
                            is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            },
                            is_reusable=True)
    username = "testuser"

    # Test presence of two placeholder TAFs
    taflist = get_taflist(client)
    assert len(taflist) == 2

    # retrieve TAF
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']

    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)
    # Test publishing first TAF
    postbody['changeStatusTo'] = 'PUBLISHED'
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    # The DRAFT will replace the default NEW, leaving us with two elements
    tafs1 = get_taflist(client)
    assert len(tafs1) == 2
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[0]['editor'] is None
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'PUBLISHED'
    assert tafs1[1]['editor'] is None

    # Test publishing second TAF
    postbody = get_taf(client, tafs1[0]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)
    # Test publishing first TAF
    postbody['changeStatusTo'] = 'PUBLISHED'
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 170700" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    # The DRAFT will replace the default NEW, leaving us with two elements
    tafs2 = get_taflist(client)
    assert len(tafs2) == 2
    assert tafs2[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs2[0]['taf']['status'] == 'PUBLISHED'
    assert tafs2[0]['taf']['issueDate'] == '2020-12-17T10:00:00Z'
    assert tafs2[0]['editor'] is None
    assert tafs2[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs2[1]['taf']['status'] == 'PUBLISHED'
    assert tafs2[1]['taf']['issueDate'] == '2020-12-17T10:00:00Z'
    assert tafs2[1]['editor'] is None


@freeze_time("2020-12-17T10:00:00Z")
def test_post_amend_taf(httpx_mock, client: TestClient, session: Session,
                        capsys):
    '''Publish and then amend a TAF'''
    # pylint: disable = too-many-statements
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100AMD_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        },
        is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            },
                            is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            },
                            is_reusable=True)
    username = "testuser"

    # lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Test publishing of a draft
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']

    postbody['changeStatusTo'] = 'PUBLISHED'
    postbody['uuid'] = postbody['taf']['uuid']
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'PUBLISHED'
    assert len(tafs1) == 1
    assert tafs1[0]['editor'] is None

    # retrieve published TAF
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    # Lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)
    # post TAF
    postbody['changeStatusTo'] = 'DRAFT_AMENDED'
    post_taf(client, postbody, username)
    tafs2 = get_taflist(client)
    # We should have a PUBLISHED and a DRAFT_AMENDED
    assert len(tafs2) == 2
    assert tafs2[0]['taf']['status'] == 'DRAFT_AMENDED'
    assert tafs2[1]['taf']['status'] == 'PUBLISHED'
    assert 'issueDate' not in tafs2[0]['taf']
    assert tafs2[0]['editor'] == username

    print("###########")
    print(tafs2[0]['taf']['uuid'])
    print(tafs2[1]['taf']['uuid'])
    print(postbody)
    print("###########")

    # Publish DRAFT_AMENDED
    postbody['changeStatusTo'] = 'AMENDED'
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100 AMD" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs3 = get_taflist(client)
    # print(tafs3)
    assert tafs3[0]['taf']['status'] == 'AMENDED'
    assert tafs3[0]['taf']['issueDate'] == '2020-12-17T10:00:00Z'
    assert len(tafs3) == 1
    assert tafs3[0]['editor'] is None


@freeze_time("2020-12-17T10:00:00Z")
def test_post_correct_taf(httpx_mock, client: TestClient, session: Session,
                          capsys):
    '''Publish and then correct a TAF'''
    # pylint: disable = too-many-statements
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100COR_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        },
        is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            },
                            is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            },
                            is_reusable=True)
    username = "testuser"

    # Test saving of a draft
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'PUBLISHED'
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs1 = get_taflist(client)
    assert 'editor' in tafs1[0]
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'PUBLISHED'
    assert len(tafs1) == 1
    assert tafs1[0]['editor'] is None

    # Save DRAFT_CORRECTED
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)
    postbody['changeStatusTo'] = 'DRAFT_CORRECTED'
    post_taf(client, postbody, username)

    tafs2 = get_taflist(client)
    # We should have a PUBLISHED and a DRAFT_CORRECTED
    assert len(tafs2) == 2
    assert tafs2[0]['taf']['status'] == 'DRAFT_CORRECTED'
    assert tafs2[0]['editor'] == username
    assert tafs2[1]['taf']['status'] == 'PUBLISHED'
    assert 'issueDate' not in tafs2[0]['taf']

    # publish TAF CORRECTED
    postbody['changeStatusTo'] = 'CORRECTED'
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100 COR" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs3 = get_taflist(client)
    assert tafs3[0]['taf']['status'] == 'CORRECTED'
    assert tafs3[0]['taf']['issueDate'] == '2020-12-17T10:00:00Z'
    assert len(tafs3) == 1
    assert tafs3[0]['editor'] is None


@freeze_time("2020-12-17T10:00:00Z")
def test_post_publish_cancel_amend_taf(httpx_mock, client: TestClient,
                                       session: Session, capsys):
    '''Publish, cancel, and then amend a TAF'''
    # pylint: disable = too-many-statements
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100CNL_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100AMD_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        },
        is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            },
                            is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            },
                            is_reusable=True)
    username = "testuser"

    # Test presence of one placeholder TAFs
    taflist = get_taflist(client)
    assert len(taflist) == 1

    # Publish TAF
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']

    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # publish TAF
    postbody['changeStatusTo'] = 'PUBLISHED'
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'PUBLISHED'
    assert len(tafs1) == 1
    assert tafs1[0]['editor'] is None

    # Cancel TAF (should show the CANCELLED TAF and an empty slot)
    postbody = get_taf(client, tafs1[0]['taf']['uuid']).json()

    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # cancel TAF
    postbody['changeStatusTo'] = 'CANCELLED'
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100 CNL" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs2 = get_taflist(client)
    assert tafs2[0]['taf']['status'] == 'CANCELLED'
    assert tafs2[0]['taf']['previousId'] == '5f9583fjdf0f337925'
    assert len(tafs2[0]['canbe']) == 1
    assert tafs2[0]['canbe'] == ['AMENDED']
    assert len(tafs2) == 1
    assert tafs2[0]['editor'] is None

    # Publish second TAF after cancellation (this is equivalent to a regular amendment)
    postbody = get_taf(client, tafs2[0]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Publish TAF
    postbody['changeStatusTo'] = 'AMENDED'
    postbody['taf']['baseForecast']['cavOK'] = True
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.AMENDED}"
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100 AMD" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs3 = get_taflist(client)
    assert len(tafs3) == 1
    assert tafs3[0]['taf']['status'] == 'AMENDED'
    assert tafs3[0]['taf']['previousId'] == tafs2[0]['taf']['uuid']
    assert tafs3[0]['editor'] is None


@freeze_time("2020-12-17T10:00:00Z")
def test_cancel_new_taf(client: TestClient, session: Session, capsys):
    '''Test trying to post a TAF with invalid status change:
            try to cancel a new TAF'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    username = "testuser"

    # lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Try to cancel a NEW taf
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['changeStatusTo'] = 'CANCELLED'
        postbody['taf'] = json.load(fh1)['taf']

    response = post_taf(client, postbody, username)
    assert response.status_code == 500
    assert "Status change not allowed" in response.text

    # check logs
    out, _ = capsys.readouterr()
    assert "Status change not allowed" in out


@freeze_time("2020-12-17T10:00:00Z")
def test_draft_to_new_taf(client: TestClient, session: Session, capsys):
    '''Test trying to post a TAF with invalid status change:
            try to change status of DRAFT to NEW'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    username = "testuser"

    # lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Save as DRAFT
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'DRAFT'
    post_taf(client, postbody, username)
    # check logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.DRAFT}" in out
    assert "Status change successfully done" in out

    # Try to change status to NEW
    postbody['changeStatusTo'] = 'NEW'
    response = post_taf(client, postbody, username)
    assert response.status_code == 500
    assert "Status change not allowed" in response.text

    # check logs
    out, _ = capsys.readouterr()
    assert "Status change not allowed" in out


@freeze_time("2020-12-17T20:00:00Z")
def test_post_expired_taf(client: TestClient, session: Session, capsys):
    '''Try to update an expired taf'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    username = "testuser"

    # retrieve TAF, verify it is marked as expired
    # in the taflist
    taflist = get_taflist(client)
    assert taflist[0]['taf']['status'] == 'EXPIRED'
    # lock TAF
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Try to publish an expired taf
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['changeStatusTo'] = 'PUBLISHED'
        postbody['taf'] = json.load(fh1)['taf']

    response = post_taf(client, postbody, username)
    assert response.status_code == 400
    assert "Cannot make updates to an expired TAF" in response.text

    # check logs
    out, _ = capsys.readouterr()
    assert "Cannot make updates to an expired TAF" in out


###########################
## Test TAF Changegroups ##
###########################
@freeze_time("2020-12-17T10:00:00Z")
def test_post_tafs_with_changegroup_prob(httpx_mock, client: TestClient,
                                         session: Session, capsys):
    '''Post TAF with probability in changegroup'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    username = "testuser"

    # Test publishing TAF with probability changegroup only
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # publish TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['changeStatusTo'] = 'PUBLISHED'
        postbody['taf'] = json.load(fh1)['taf']
        postbody['taf']['changeGroups'] = [{
            "valid": {
                "start": "2020-12-17T16:00:00Z",
                "end": "2020-12-17T18:00:00Z"
            },
            "wind": {
                "direction": 120,
                "speed": 20,
                "unit": "KT"
            },
            "visibility": {
                "range": 9999,
                "unit": "M"
            },
            "weather": {
                "weather1": "RA"
            },
            "cloud": {
                "cloud1": {
                    "coverage": "FEW",
                    "height": 1,
                    "type": "CB"
                }
            },
            "probability": "PROB30"
        }]
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs1 = get_taflist(client)
    assert len(tafs1) == 1
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'PUBLISHED'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_tafs_with_changegroup_prob_change(httpx_mock, client: TestClient,
                                                session: Session, capsys):
    '''Post TAF with probability and change in changegroup'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session, use_two=True)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    username = "testuser"

    # Test TAF with probability and change changegroup
    taflist = get_taflist(client)
    assert len(taflist) == 2
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # publish TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['changeStatusTo'] = 'PUBLISHED'
        postbody['taf'] = json.load(fh1)['taf']
        postbody['taf']['changeGroups'] = [{
            "valid": {
                "start": "2020-12-17T16:00:00Z",
                "end": "2020-12-17T18:00:00Z"
            },
            "wind": {
                "direction": 120,
                "speed": 20,
                "unit": "KT"
            },
            "visibility": {
                "range": 9999,
                "unit": "M"
            },
            "weather": {
                "weather1": "RA"
            },
            "cloud": {
                "cloud1": {
                    "coverage": "FEW",
                    "height": 1,
                    "type": "CB"
                }
            },
            "probability": "PROB30",
            "change": "TEMPO"
        }]
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'PUBLISHED'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_tafs_with_changegroup_prob_change_invalid(client: TestClient,
                                                        session: Session):
    '''Post TAF with invalid probability and change in changegroup'''
    fill_table(session, use_two=True)

    username = "testuser"

    # Test TAF with invalid probability and change changegroup combination
    taflist = get_taflist(client)
    assert len(taflist) == 2
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # publish TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh2:
        postbody['taf'] = json.load(fh2)['taf']

    postbody['changeStatusTo'] = 'PUBLISHED'
    postbody['taf']['changeGroups'] = [{
        "valid": {
            "start": "2020-12-17T16:00:00Z",
            "end": "2020-12-17T18:00:00Z"
        },
        "wind": {
            "direction": 120,
            "speed": 20,
            "unit": "KT"
        },
        "visibility": {
            "range": 9999,
            "unit": "M"
        },
        "weather": {
            "weather1": "RA"
        },
        "cloud": {
            "cloud1": {
                "coverage": "FEW",
                "height": 1,
                "type": "CB"
            }
        },
        "probability": "PROB40",
        "change": "BECMG"
    }]
    response = post_taf(client, postbody, username)
    assert response.status_code == 400
    assert "A probability is only allowed in combination" + \
        " with TEMPO or an empty Change input" in response.text

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'NEW'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_tafs_with_changegroup_fm(httpx_mock, client: TestClient,
                                       session: Session):
    '''Post TAF with FM changegroup'''

    fill_table(session, use_two=True)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    username = "testuser"

    # Test presence of two placeholder TAFs
    taflist = get_taflist(client)
    assert len(taflist) == 2

    # Test TAF with change FM only
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['changeStatusTo'] = 'PUBLISHED'
        postbody['taf'] = json.load(fh1)['taf']
        postbody['taf']['changeGroups'] = [{
            "valid": {
                "start": "2020-12-17T22:30:00Z"
            },
            "wind": {
                "direction": 90,
                "speed": 5,
                "unit": "KT"
            },
            "visibility": {
                "range": 9999,
                "unit": "M"
            },
            "weather": {
                "weather1": "RA"
            },
            "cloud": {
                "cloud1": {
                    "coverage": "FEW",
                    "height": 5,
                    "type": "CB"
                }
            },
            "change": "FM"
        }]
    post_taf(client, postbody, username)

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'PUBLISHED'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_tafs_with_changegroup_bcmg(httpx_mock, client: TestClient,
                                         session: Session):
    '''Post TAF with BCMG changegroup'''
    fill_table(session, use_two=True)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    username = "testuser"

    # Test TAF with change BECMG only
    taflist = get_taflist(client)
    assert len(taflist) == 2
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # publish TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['changeStatusTo'] = 'PUBLISHED'
        postbody['taf'] = json.load(fh1)['taf']
        postbody['taf']['changeGroups'] = [{
            "valid": {
                "start": "2020-12-17T23:00:00Z",
                "end": "2020-12-18T02:00:00Z"
            },
            "wind": {
                "direction": 90,
                "speed": 10,
                "unit": "KT"
            },
            "visibility": {
                "range": 9999,
                "unit": "M"
            },
            "weather": {
                "weather1": "RA"
            },
            "cloud": {
                "cloud1": {
                    "coverage": "FEW",
                    "height": 1
                }
            },
            "change": "BECMG"
        }]
    post_taf(client, postbody, username)

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'PUBLISHED'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_tafs_with_changegroup_tempo(httpx_mock, client: TestClient,
                                          session: Session):
    '''Post TAF with TEMPO changegroup'''
    fill_table(session, use_two=True)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })

    username = "testuser"

    # Test presence of two placeholder TAFs
    taflist = get_taflist(client)
    assert len(taflist) == 2

    # Test TAF with change TEMPO only
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # publish TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['changeStatusTo'] = 'PUBLISHED'
        postbody['taf'] = json.load(fh1)['taf']
        postbody['taf']['changeGroups'] = [{
            "valid": {
                "start": "2020-12-17T23:00:00Z",
                "end": "2020-12-18T02:00:00Z"
            },
            "wind": {
                "direction": 90,
                "speed": 10,
                "unit": "KT"
            },
            "visibility": {
                "range": 9999,
                "unit": "M"
            },
            "weather": {
                "weather1": "RA"
            },
            "cloud": {
                "cloud1": {
                    "coverage": "FEW",
                    "height": 1
                }
            },
            "change": "TEMPO"
        }]
    post_taf(client, postbody, username)

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'PUBLISHED'


@freeze_time("2020-12-17T10:00:00Z")
def test_save_draft_changegroup_none(client: TestClient, session: Session):
    '''Save a DRAFT with invalid changegroup value None'''
    # redirect stream
    stream_handler.stream = sys.stdout
    fill_table(session)
    username = "testuser"

    # Lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'DRAFT'
    # Set the changegroup to invalid None value
    postbody['taf']['changeGroups'] = [None]

    # Post TAF and check response
    response = post_taf(client, postbody, username)
    assert "changeGroups" in response.text
    assert "Input should be a valid dictionary or object to extract fields from" in response.text
    assert response.status_code == 400
    # Status should still be "NEW", not "DRAFT"
    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['status'] == 'NEW'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_tafs_invalid_changegroup(client: TestClient, session: Session):
    '''Post TAFs with invalid changeGroup contents'''
    fill_table(session, use_two=True)

    username = "testuser"

    # Test presence of two placeholder TAFs
    taflist = get_taflist(client)
    assert len(taflist) == 2

    # Test saving a  TAF with invalid changegroup
    # (change & probability missing)
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)
    # save TAF as DRAFT
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']

    postbody['taf']['changeGroups'] = [{
        "valid": {
            "start": "2020-12-17T16:00:00Z",
            "end": "2020-12-17T18:00:00Z"
        },
        "wind": {
            "direction": 120,
            "speed": 20,
            "unit": "KT"
        },
        "visibility": {
            "range": 9999,
            "unit": "M"
        },
        "weather": {
            "weather1": "RA"
        },
        "cloud": {
            "cloud1": {
                "coverage": "FEW",
                "height": 1,
                "type": "CB"
            }
        }
    }]
    postbody['changeStatusTo'] = 'DRAFT'

    # save as draft to check partial draft validation is OK
    response = post_taf(client, postbody, username)
    assert response.status_code == 200
    tafs1 = get_taflist(client)
    # verify that taf is saved as draft
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'DRAFT'

    # check that publishing does fail as TAF is incomplete
    postbody['changeStatusTo'] = 'PUBLISHED'
    response = post_taf(client, postbody, username)
    assert response.status_code == 400
    assert "Provided TAF cannot be published due to missing data " +\
                "(change or prob are missing)" in response.text

    tafs1 = get_taflist(client)
    # verify that taf was not published
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'DRAFT'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_tafs_invalid_change_value(client: TestClient, session: Session):
    '''Post TAFs invalid change value'''

    fill_table(session, use_two=True)

    username = "testuser"

    # Test presence of two placeholder TAFs
    taflist = get_taflist(client)
    assert len(taflist) == 2

    # Test saving a  TAF with invalid changegroup:
    #   change field has invalid value
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']

    postbody['changeStatusTo'] = 'PUBLISHED'
    postbody['taf']['changeGroups'] = [{
        "valid": {
            "start": "2020-12-17T16:00:00Z",
            "end": "2020-12-17T18:00:00Z"
        },
        "wind": {
            "direction": 120,
            "speed": 20,
            "unit": "KT"
        },
        "visibility": {
            "range": 9999,
            "unit": "M"
        },
        "weather": {
            "weather1": "RA"
        },
        "cloud": {
            "cloud1": {
                "coverage": "FEW",
                "height": 1,
                "type": "CB"
            }
        },
        'change': 'INVALID'
    }]
    response = post_taf(client, postbody, username)
    assert "Input should be 'FM', 'BECMG' or 'TEMPO'" in response.text
    assert response.status_code == 400

    tafs1 = get_taflist(client)
    # verify that taf is not saved as draft
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'NEW'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_tafs_invalid_probability_value(client: TestClient,
                                             session: Session):
    '''Post TAFs invalid probability value'''
    fill_table(session, use_two=True)

    username = "testuser"

    # Test saving a  TAF with invalid changegroup:
    #   probability field has invalid value
    taflist = get_taflist(client)
    assert len(taflist) == 2
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    # lock TAF
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']

    postbody['changeStatusTo'] = 'PUBLISHED'
    postbody['taf']['changeGroups'] = [{
        "valid": {
            "start": "2020-12-17T16:00:00Z",
            "end": "2020-12-17T18:00:00Z"
        },
        "wind": {
            "direction": 120,
            "speed": 20,
            "unit": "KT"
        },
        "visibility": {
            "range": 9999,
            "unit": "M"
        },
        "weather": {
            "weather1": "RA"
        },
        "cloud": {
            "cloud1": {
                "coverage": "FEW",
                "height": 1,
                "type": "CB"
            }
        },
        'probability': 'PROB20'
    }]

    response = post_taf(client, postbody, username)
    assert "Input should be 'PROB30' or 'PROB40'" in response.text
    assert response.status_code == 400

    tafs1 = get_taflist(client)
    # verify that taf is not published
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'NEW'


###########################
### Test draft deletions ##
###########################
@freeze_time("2020-12-17T10:00:00Z")
def test_delete_unused_drafts_correct(httpx_mock, client: TestClient,
                                      session: Session, capsys):
    '''Delete unused drafts after a TAF has been published'''
    # pylint: disable = too-many-statements
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100COR_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        },
        is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            },
                            is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            },
                            is_reusable=True)
    username = "testuser"

    # lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Publish TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['changeStatusTo'] = 'PUBLISHED'
        postbody['taf'] = json.load(fh1)['taf']
        post_taf(client, postbody, username)

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'PUBLISHED'
    assert len(tafs1) == 1

    # Create a DRAFT_AMENDMENT
    # lock TAF
    postbody = get_taf(client, tafs1[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # post TAF DRAFT_AMENDMENT
    postbody['changeStatusTo'] = 'DRAFT_AMENDED'
    post_taf(client, postbody, username)

    # verify results
    tafs2 = get_taflist(client)
    assert tafs2[0]['taf']['status'] == 'DRAFT_AMENDED'
    assert tafs2[0]['taf']['previousId'] == '5f9583fjdf0f337925'
    assert len(tafs2) == 2
    # Check that AMENDED is not an option for the canbe of the PUBLISHED TAF
    assert tafs2[1]['taf']['status'] == 'PUBLISHED'
    assert tafs2[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs2[0]['taf']['baseTime'] == tafs2[1]['taf']['baseTime']
    assert 'AMENDED' not in tafs2[1]['canbe']
    assert 'CORRECTED' in tafs2[1]['canbe']
    assert 'CANCELLED' in tafs2[1]['canbe']

    # Create a DRAFT_CORRECTED
    postbody['changeStatusTo'] = 'DRAFT_CORRECTED'
    postbody['taf'] = tafs2[1]['taf']
    postbody['taf']['baseForecast']['cavOK'] = True
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    post_taf(client, postbody, username)

    # verify results
    tafs3 = get_taflist(client)
    assert len(tafs3) == 3
    assert tafs3[1]['taf']['status'] == 'DRAFT_CORRECTED'
    assert tafs3[1]['taf']['previousId'] == '5f9583fjdf0f337925'
    # Check that neither AMENDED nor CORRECTED are options for the canbe of the PUBLISHED TAF
    assert tafs3[2]['taf']['status'] == 'PUBLISHED'
    assert tafs3[2]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert 'AMENDED' not in tafs3[2]['canbe']
    assert 'CORRECTED' not in tafs3[2]['canbe']
    assert 'CANCELLED' in tafs3[2]['canbe']

    # Publish the CORRECTED TAF and check that the DRAFT_AMENDED disappears
    postbody['changeStatusTo'] = 'CORRECTED'
    postbody['taf'] = tafs3[2]['taf']
    postbody['taf']['baseForecast']['cavOK'] = True
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    post_taf(client, postbody, username)

    # verify results
    tafs4 = get_taflist(client)
    assert tafs4[0]['taf']['status'] == 'CORRECTED'
    assert tafs4[0]['taf']['previousId'] == '5f9583fjdf0f337925'
    assert len(tafs4) == 1
    # Check the canbe options for this new CORRECTED TAF are correct
    assert 'AMENDED' in tafs4[0]['canbe']
    assert 'CORRECTED' in tafs4[0]['canbe']
    assert 'CANCELLED' in tafs4[0]['canbe']

    # check that two TAFs are deleted from the database
    out, _ = capsys.readouterr()
    assert "Deleting TAF from database" in out
    assert out.count("Deleting TAF from database") == 2


@freeze_time("2020-12-17T10:00:00Z")
def test_delete_unused_draft_amend(httpx_mock, client: TestClient,
                                   session: Session, capsys):
    '''Delete unused drafts after a TAF has been amended'''
    # pylint: disable = too-many-statements
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100AMD_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        },
        is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            },
                            is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            },
                            is_reusable=True)
    username = "testuser"

    # lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Publish TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['changeStatusTo'] = 'PUBLISHED'
        postbody['taf'] = json.load(fh1)['taf']
        post_taf(client, postbody, username)

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'PUBLISHED'
    assert len(tafs1) == 1

    # Create a DRAFT_AMENDMENT
    # lock TAF
    postbody = get_taf(client, tafs1[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # post TAF DRAFT_AMENDMENT
    postbody['changeStatusTo'] = 'DRAFT_AMENDED'
    post_taf(client, postbody, username)

    # verify results
    tafs2 = get_taflist(client)
    assert tafs2[0]['taf']['status'] == 'DRAFT_AMENDED'
    assert tafs2[0]['taf']['previousId'] == '5f9583fjdf0f337925'
    assert len(tafs2) == 2
    # Check that AMENDED is not an option for the canbe of the PUBLISHED TAF
    assert tafs2[1]['taf']['status'] == 'PUBLISHED'
    assert tafs2[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs2[0]['taf']['baseTime'] == tafs2[1]['taf']['baseTime']
    assert 'AMENDED' not in tafs2[1]['canbe']
    assert 'CORRECTED' in tafs2[1]['canbe']
    assert 'CANCELLED' in tafs2[1]['canbe']

    # Create a DRAFT_CORRECTED
    postbody['changeStatusTo'] = 'DRAFT_CORRECTED'
    postbody['taf'] = tafs2[1]['taf']
    postbody['taf']['baseForecast']['cavOK'] = True
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    post_taf(client, postbody, username)

    # verify results
    tafs3 = get_taflist(client)
    assert len(tafs3) == 3
    assert tafs3[1]['taf']['status'] == 'DRAFT_CORRECTED'
    assert tafs3[1]['taf']['previousId'] == '5f9583fjdf0f337925'
    # Check that neither AMENDED nor CORRECTED are options for the canbe of the PUBLISHED TAF
    assert tafs3[2]['taf']['status'] == 'PUBLISHED'
    assert tafs3[2]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert 'AMENDED' not in tafs3[2]['canbe']
    assert 'CORRECTED' not in tafs3[2]['canbe']
    assert 'CANCELLED' in tafs3[2]['canbe']

    # Publish the AMENDED TAF and check that the drafts disappear
    postbody['changeStatusTo'] = 'AMENDED'
    postbody['taf'] = tafs3[2]['taf']
    postbody['taf']['baseForecast']['cavOK'] = True
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    post_taf(client, postbody, username)

    # verify results
    tafs4 = get_taflist(client)
    assert tafs4[0]['taf']['status'] == 'AMENDED'
    assert tafs4[0]['taf']['previousId'] == '5f9583fjdf0f337925'
    assert len(tafs4) == 1
    # Check the canbe options for this new AMENDED TAF are correct
    assert 'AMENDED' in tafs4[0]['canbe']
    assert 'CORRECTED' in tafs4[0]['canbe']
    assert 'CANCELLED' in tafs4[0]['canbe']

    # check that two TAFs are deleted from the database
    out, _ = capsys.readouterr()
    assert "Deleting TAF from database" in out
    assert out.count("Deleting TAF from database") == 2


@freeze_time("2020-12-17T10:00:00Z")
def test_delete_unused_drafts_cancel(httpx_mock, client: TestClient,
                                     session: Session, capsys):
    '''Delete unused drafts after a TAF has been cancelled'''
    # pylint: disable = too-many-statements
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100CNL_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        },
        is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            },
                            is_reusable=True)

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            },
                            is_reusable=True)
    username = "testuser"

    # Lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Publish TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['changeStatusTo'] = 'PUBLISHED'
        postbody['taf'] = json.load(fh1)['taf']
        post_taf(client, postbody, username)

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'PUBLISHED'
    assert len(tafs1) == 1

    # Create a DRAFT_AMENDMENT

    # first lock TAF
    postbody = get_taf(client, tafs1[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # post DRAFT_AMENDED
    postbody['changeStatusTo'] = 'DRAFT_AMENDED'
    postbody['taf'] = tafs1[0]['taf']
    post_taf(client, postbody, username)

    # verify results
    tafs2 = get_taflist(client)
    assert tafs2[0]['taf']['status'] == 'DRAFT_AMENDED'
    assert tafs2[0]['taf']['previousId'] == '5f9583fjdf0f337925'
    assert len(tafs2) == 2

    # Create a DRAFT_CORRECTED
    postbody['changeStatusTo'] = 'DRAFT_CORRECTED'
    postbody['taf'] = tafs1[0]['taf']
    postbody['taf']['baseForecast']['cavOK'] = True
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    post_taf(client, postbody, username)
    # verify results
    tafs3 = get_taflist(client)
    assert len(tafs3) == 3
    assert tafs3[1]['taf']['status'] == 'DRAFT_CORRECTED'
    assert tafs3[1]['taf']['previousId'] == '5f9583fjdf0f337925'

    # Cancel the original TAF and check that the DRAFT_AMENDED and DRAFT_CORRECTED disappear
    postbody['changeStatusTo'] = 'CANCELLED'
    postbody['taf'] = tafs1[0]['taf']
    post_taf(client, postbody, username)
    # verify result
    tafs4 = get_taflist(client)
    assert tafs4[0]['taf']['status'] == 'CANCELLED'
    assert tafs4[0]['taf']['previousId'] == '5f9583fjdf0f337925'
    assert len(tafs4[0]['canbe']) == 1
    assert tafs4[0]['canbe'][0] == 'AMENDED'
    assert len(tafs4) == 1

    # check that two TAFs are deleted from the database
    out, _ = capsys.readouterr()
    assert "Deleting TAF from database" in out
    assert out.count("Deleting TAF from database") == 2


@freeze_time("2020-12-17T10:00:00Z")
def test_save_drafts(httpx_mock, client: TestClient, session: Session, capsys):
    '''Publish a TAF. Then, save draft correction and
    draft amendment without publishing a new file'''
    # pylint: disable = too-many-statements
    # redirect stream
    stream_handler.stream = sys.stdout
    fill_table(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })
    username = "testuser"

    # Lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Publish TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'PUBLISHED'
    post_taf(client, postbody, username)

    # check publishing logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'PUBLISHED'
    assert len(tafs1) == 1
    assert tafs1[0]['editor'] is None

    # Create a DRAFT_AMENDMENT
    # lock TAF
    postbody = get_taf(client, tafs1[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)
    # post TAF
    postbody['changeStatusTo'] = 'DRAFT_AMENDED'
    post_taf(client, postbody, username)

    # verify draft was saved
    tafs2 = get_taflist(client)
    assert tafs2[0]['taf']['status'] == 'DRAFT_AMENDED'
    assert tafs2[0]['taf']['previousId'] == '5f9583fjdf0f337925'
    assert tafs2[0]['editor'] == username

    # Create a DRAFT_CORRECTED
    postbody = get_taf(client, tafs1[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)
    # post TAF
    postbody['changeStatusTo'] = 'DRAFT_CORRECTED'
    postbody['taf']['baseForecast']['cavOK'] = True
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    post_taf(client, postbody, username)

    # verify another draft was saved
    tafs3 = get_taflist(client)
    assert tafs3[1]['taf']['status'] == 'DRAFT_CORRECTED'
    assert tafs3[1]['taf']['previousId'] == '5f9583fjdf0f337925'
    assert tafs3[1]['editor'] == username

    # check that two drafts are added to the database
    out, _ = capsys.readouterr()
    assert out.count("inserting TAF into database") == 2


@freeze_time("2020-12-17T10:00:00Z")
def test_save_and_list_partial_drafts(httpx_mock, client: TestClient,
                                      session: Session, capsys):
    '''Save drafts that are incomplete without publishing a new file'''
    # pylint: disable = too-many-statements
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })

    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })
    username = "testuser"

    # Lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'DRAFT'
    # Remove the base forecast
    baseforecast = postbody['taf']['baseForecast']
    postbody['taf'].pop("baseForecast")

    # Save draft
    post_taf(client, postbody, username)
    # check logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.DRAFT}" in out
    assert "Status change successfully done" in out

    tafs0 = get_taflist(client)
    assert tafs0[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs0[0]['taf']['status'] == 'DRAFT'
    assert 'issueDate' not in tafs0[0]['taf']
    assert tafs0[0]['editor'] == username

    # Add base forecast again
    postbody['taf']['baseForecast'] = baseforecast
    postbody['changeStatusTo'] = 'PUBLISHED'

    # Publish TAF
    post_taf(client, postbody, username)
    # check logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "TAC_body.status_code: 200" in out
    assert "result: FTNL99 EHDB 171100" in out
    assert "IWXXM30_body.status_code: 200" in out
    assert "audit-logging" in out
    assert "Updating TAF editor in database" in out

    tafs1 = get_taflist(client)
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'PUBLISHED'
    assert len(tafs1) == 1
    assert tafs1[0]['editor'] is None

    # Create a DRAFT_AMENDMENT
    postbody = get_taf(client, tafs1[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    postbody['changeStatusTo'] = 'DRAFT_AMENDED'
    # Remove the base forecast again
    baseforecast = postbody['taf']['baseForecast']
    postbody['taf'].pop("baseForecast")

    # Everything should still work
    post_taf(client, postbody, username)

    # check logs
    out, _ = capsys.readouterr()
    assert "inserting TAF into database" in out
    assert out.count("inserting TAF into database") == 1

    tafs2 = get_taflist(client)
    assert tafs2[0]['taf']['status'] == 'DRAFT_AMENDED'
    assert tafs2[0]['taf']['previousId'] == '5f9583fjdf0f337925'
    assert tafs2[0]['editor'] == username
    assert "'baseForecast'" not in postbody['taf']

    # Create a DRAFT_CORRECTED
    postbody = get_taf(client, tafs1[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    postbody['changeStatusTo'] = 'DRAFT_CORRECTED'
    postbody['taf']['baseForecast'] = baseforecast
    postbody['taf']['baseForecast']['cavOK'] = True
    postbody['taf']['baseForecast']['wind'] = {
        "direction": 180,
        "speed": 10,
        "unit": "KT"
    }
    post_taf(client, postbody, username)

    # check logs
    out, _ = capsys.readouterr()
    assert "inserting TAF into database" in out
    assert out.count("inserting TAF into database") == 1

    tafs3 = get_taflist(client)
    assert tafs3[1]['taf']['status'] == 'DRAFT_CORRECTED'
    assert tafs3[1]['taf']['previousId'] == '5f9583fjdf0f337925'
    assert tafs3[1]['editor'] == username


###########################
### Test error catching ###
###########################
@freeze_time("2020-12-17T10:00:00Z")
def test_post_taf_not_found(client: TestClient, capsys):
    '''Try to update a taf that is not present in the database'''
    # redirect stream
    stream_handler.stream = sys.stdout

    username = "testuser"

    # Check that database is empty
    assert len(get_taflist(client)) == 0

    # Try to publish a TAF that does not exist in the database
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody = {'changeStatusTo': 'PUBLISHED', 'taf': json.load(fh1)['taf']}
        response = post_taf(client, postbody, username)  # type: ignore

        assert response.status_code == 400
        # check logs
        out, _ = capsys.readouterr()
        assert "Error while retrieving TAF from database" in out
        # check response
        assert json.loads(response.content.decode(
            "utf-8"))['message'] == "Error while retrieving TAF from database"


@freeze_time("2020-12-17T10:00:00Z")
def test_post_fail_tac(httpx_mock, client: TestClient, session: Session,
                       capsys):
    '''Post a TAF and ensure db doesn't get updated if tac generation fails'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={},
                            status_code=500)
    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })
    username = "testuser"

    # Lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Test publishing first TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'PUBLISHED'

    response = post_taf(client, postbody, username)
    assert "Publishing of TAC failed" in response.text
    assert response.status_code == 400

    # check logs
    out, _ = capsys.readouterr()
    assert "4XX or 500 error while calling TAF TAC service" in out

    tafs1 = get_taflist(client)
    assert len(tafs1) == 1

    # Check that the TAF has not been published
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'NEW'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_fail_db_connection(client: TestClient, session: Session, capsys):
    '''Post a TAF and ensure db doesn't get updated if tac generation fails
    due to connection error'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session)

    username = "testuser"
    assert len(get_taflist(client)) == 1

    # Lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Test publishing first TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'PUBLISHED'

    with patch('app.crud.taf.CRUD.read_one', side_effect=ReadError("mock")):
        response = post_taf(client, postbody, username)

    # check logs
    out, _ = capsys.readouterr()
    assert "Error while retrieving TAF from database" in out
    # check response
    assert response.status_code == 500
    assert response.json() == {
        'message': 'Error while retrieving TAF from database'
    }

    tafs1 = get_taflist(client)
    assert len(tafs1) == 1

    # Check that the TAF has not been published
    assert tafs1[0]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[0]['taf']['status'] == 'NEW'


@freeze_time("2020-12-17T10:00:00Z")
def test_patch_fail_db_connection(client: TestClient, session: Session, capsys):
    '''Patch a TAF and ensure db doesn't get updated if there is no
    connection with the database'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session, use_two=True)

    username = "testuser"
    assert len(get_taflist(client)) == 2

    # Try to lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    postbody['editor'] = username
    with patch('app.crud.taf.CRUD.read_one', side_effect=ReadError("mock")):
        response = patch_taf(client, postbody, username)

    # check logs
    out, _ = capsys.readouterr()
    assert "Received request: PATCH TAF" in out
    assert "Error while retrieving TAF from database" in out
    # check response
    assert response.status_code == 500
    assert response.json() == {
        'message': 'Error while retrieving TAF from database'
    }

    tafs1 = get_taflist(client)
    assert len(tafs1) == 2

    # Check that the TAF has not been published
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[0]['editor'] is None
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['editor'] is None


# @pytest.mark.httpx_mock(assert_all_requests_were_expected=False)
@freeze_time("2020-12-17T10:00:00Z")
def test_post_fail_tac_connection(httpx_mock, client: TestClient,
                                  session: Session, capsys):
    '''Post a TAF and ensure db doesn't get updated if tac
    generation fails due to request exceptions'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session, use_two=True)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_exception(url="http://tac-converter/gettaftac",
                             exception=httpx.RequestError("mock request error"))
    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            })
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "IWXXM 3.0 CONTENT"
        })

    username = "testuser"
    taflist = get_taflist(client)
    assert len(taflist) == 2
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Test publishing TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'PUBLISHED'

    response = post_taf(client, postbody, username)
    assert "Publishing of TAC failed" in response.text
    assert response.status_code == 400

    # check logs
    out, _ = capsys.readouterr()
    assert f"Status change, new status: {Status.PUBLISHED}" in out
    assert "Error while calling TAF TAC service" in out

    tafs1 = get_taflist(client)
    assert len(tafs1) == 2

    # Check that the TAF has not been published
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'NEW'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_fail_iwxxm30_converter_500(httpx_mock, client: TestClient,
                                         session: Session, capsys):
    '''Post a TAF and ensure db doesn't get updated if iwxxm30 generation fails'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session, use_two=True)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })
    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={},
                            status_code=500)
    username = "testuser"

    taflist = get_taflist(client)
    assert len(taflist) == 2
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Test publishing first TAF with HTTPError
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'PUBLISHED'
    post_taf(client, postbody, username)

    # check logs
    out, _ = capsys.readouterr()
    assert "4XX or 500 error while calling TAF IWXXM30 service" in out

    # Verify TAF has been published
    tafs1 = get_taflist(client)
    assert len(tafs1) == 2
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'PUBLISHED'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_fail_iwxxm30_converter_timeout(httpx_mock, client: TestClient,
                                             session: Session, capsys):
    '''Post a TAF and ensure db doesn't get updated if iwxxm30 generation fails'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session, use_two=True)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })
    httpx_mock.add_exception(url="http://tac-converter/gettafiwxxm30",
                             exception=httpx.TimeoutException("mock timeout"))
    username = "testuser"

    taflist = get_taflist(client)
    assert len(taflist) == 2
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # publish TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'PUBLISHED'
    post_taf(client, postbody, username)

    # check IWXXM30 Timeout error occurred while publishing TAF
    out, _ = capsys.readouterr()
    assert "Timeout error while calling TAF IWXXM30 service" in out

    # Verify TAF has been published
    tafs1 = get_taflist(client)
    assert len(tafs1) == 2
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'PUBLISHED'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_fail_iwxxm30_converter_requestexception(httpx_mock,
                                                      client: TestClient,
                                                      session: Session, capsys):
    '''Post a TAF and ensure db doesn't get updated if iwxxm30 generation fails'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session, use_two=True)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })
    httpx_mock.add_exception(url="http://tac-converter/gettafiwxxm30",
                             exception=httpx.RequestError("mock request error"))
    username = "testuser"

    taflist = get_taflist(client)
    assert len(taflist) == 2
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # mock RequestException error while publishing TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'PUBLISHED'
    post_taf(client, postbody, username)

    # check logs
    out, _ = capsys.readouterr()
    assert "Error while calling TAF IWXXM30 service" in out

    # Verify TAF has been published
    tafs1 = get_taflist(client)
    assert len(tafs1) == 2
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'PUBLISHED'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_fail_502_tac(httpx_mock, client: TestClient, session: Session,
                           capsys):
    '''Post a TAF and ensure we retry on 502 error'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session, use_two=True)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={},
                            status_code=502,
                            is_reusable=True)
    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={
                                "data": "DUMMY IWXXM 3.0",
                                "status_code": 200,
                                "content": "IWXXM 3.0 CONTENT"
                            },
                            status_code=200)
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/A_LTNL99EHDB171100_C_EHAM_20201217100000.xml",
        method="POST",
        json={
            "data": "DUMMY IWXXM 3.0",
            "status_code": 200,
            "content": "TAC IWXXM 3.0"
        })

    username = "testuser"

    # lock TAF
    taflist = get_taflist(client)
    assert len(taflist) == 2
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Test publishing first TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'PUBLISHED'

    response = post_taf(client, postbody, username)
    assert "Publishing of TAC failed" in response.text
    assert response.status_code == 400

    # check logs
    out, _ = capsys.readouterr()
    assert "Error while retrying to call TAF TAC service" in out
    assert "Error code: 502" in out

    tafs1 = get_taflist(client)
    assert len(tafs1) == 2

    # Check that the TAF has not been published
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'NEW'


@freeze_time("2020-12-17T10:00:00Z")
def test_post_fail_iwxxm30(httpx_mock, client: TestClient, session: Session,
                           capsys):
    '''Post a TAF and ensure db doesn't get updated if iwxxm30 generation fails'''
    # redirect stream
    stream_handler.stream = sys.stdout

    fill_table(session, use_two=True)
    settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
    settings.aviation_taf_publish_host = 'taf-publisher'
    httpx_mock.add_response(url="http://tac-converter/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })
    httpx_mock.add_response(
        url=
        "http://taf-publisher/publish/TAF_EHAM_2020-12-171100T20201217100000.tac",
        method="POST",
        json={
            "data": "DUMMY TAC",
            "status_code": 200,
            "content": "TAC CONTENT"
        })
    httpx_mock.add_response(url="http://tac-converter/gettafiwxxm30",
                            method="POST",
                            json={},
                            status_code=500)
    username = "testuser"

    # lock TAF
    taflist = get_taflist(client)
    assert len(taflist) == 2
    postbody = get_taf(client, taflist[1]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Test publishing first TAF
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'PUBLISHED'

    response = post_taf(client, postbody, username)
    # iwxxm30 was not published, but TAC was
    # so publishing should be successfull
    assert response.status_code == 200

    # check logs
    out, _ = capsys.readouterr()
    assert "4XX or 500 error while calling TAF IWXXM30 service" in out

    tafs1 = get_taflist(client)
    assert len(tafs1) == 2

    # Check that the TAF has not been published
    assert tafs1[0]['taf']['uuid'] == '6523462362456sw45g'
    assert tafs1[0]['taf']['status'] == 'NEW'
    assert tafs1[1]['taf']['uuid'] == '5f9583fjdf0f337925'
    assert tafs1[1]['taf']['status'] == 'PUBLISHED'


###########################
##### Test validations ####
###########################
def test_validation_decode_error(client: TestClient):
    """ Tests for bad JSON """

    invalid_json = """
    {
        "taf": {
        "messageType": "ORG",
        "location": "EHAM",
        "baseForecast": {
            "valid": {
            "start": "2020-12-17T12:00:00Z"
            "end": "2020-12-18T18:00:00Z"
            }
        },
        "baseTime": "2020-12-17T12:00:00Z",
        "uuid": "5f9583fjdf0f337925",
        "validDateStart": "2020-12-17T12:00:00Z",
        "validDateEnd": "2020-12-18T18:00:00Z",
        "status": "NEW"
        },
        "changeStatusTo": "PUBLISHED"
    }"""

    # Test JSONDecodeError
    response = post_taf(client, invalid_json, 'test_user')  # type: ignore
    assert response.status_code == 400
    assert "Input should be a valid dictionary or object to extract fields from" in response.text


@freeze_time("2020-10-18T14:55:00.500Z")
def test_validation_exceptions(client: TestClient):
    """ Tests for bad TAF format"""

    # Test for ValidationError --> wind is missing
    with open('app/tests/testdata/badvalidation.json', 'rb') as fh:

        # post taf
        response = post_taf(client, json.load(fh), 'test_user')
        assert response.status_code == 400
        assert "Input should be a valid dictionary or instance of Wind" in response.text


@freeze_time("2020-12-17T10:00:00Z")
def test_post_taf_missing_baseforecast(client: TestClient, session: Session):
    '''Test trying to post a TAF with missing baseforecast'''

    fill_table(session)
    username = "testuser"

    # Lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Post a TAF with missing field
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'DRAFT'

    # remove baseforecast field
    del postbody['taf']['baseForecast']
    # verify field is missing
    assert not 'baseForecast' in postbody['taf']

    # check you can post draft
    response = post_taf(client, postbody, username)
    assert response.status_code == 200

    # check that you cannot publish this TAF
    postbody['changeStatusTo'] = 'PUBLISHED'
    response = post_taf(client, postbody, username)
    assert "baseForecast" in response.text
    assert "Input should be a valid dictionary or instance of BaseForecast" in response.text
    assert response.status_code == 400


@freeze_time("2020-12-17T10:00:00Z")
def test_post_taf_incomplete_baseforecast(client: TestClient, session: Session):
    '''Test trying to post a TAF with incomplete baseforecast'''

    fill_table(session)
    username = "testuser"

    # Lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Post a TAF with missing field
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
        postbody['changeStatusTo'] = 'DRAFT'

        # add incomplete baseforecast missing wind field
        postbody['taf']['baseForecast'] = {}
        postbody['taf']['baseForecast']['cavOK'] = True
        postbody['taf']['baseForecast']['valid'] = {
            "start": "2020-12-17T12:00:00Z",
            "end": "2020-12-18T18:00:00Z"
        }

        # check you can post draft
        response = post_taf(client, postbody, username)
        assert response.status_code == 200

        # check that you cannot publish this TAF
        postbody['changeStatusTo'] = 'PUBLISHED'
        response = post_taf(client, postbody, username)
        assert "Input should be a valid dictionary or instance of Wind" in response.text
        assert response.status_code == 400


@freeze_time("2020-12-17T10:00:00Z")
def test_post_taf_invalid_exclusiverequiredbaseforecast(client: TestClient,
                                                        session: Session):
    '''Test trying to post a TAF with invalid exclusiveRequiredBaseForecast'''

    fill_table(session)
    username = "testuser"

    # Lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Post a TAF with missing field
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
        postbody['changeStatusTo'] = 'DRAFT'

        # try to post with an invalid exclusiveRequiredBaseForecast
        postbody['taf']['baseForecast']['wind'] = {
            "direction": 180,
            "speed": 10,
            "unit": "KT"
        }
        postbody['taf']['baseForecast']['visibility'] = {
            "range": 6000,
            "unit": "M"
        }

        # check you can post draft
        response = post_taf(client, postbody, username)
        assert response.status_code == 200

        # check that you cannot publish this TAF
        postbody['changeStatusTo'] = 'PUBLISHED'
        response = post_taf(client, postbody, username)
        assert "Value error, 'cavOK' and 'visibility' are mutually exclusive" in response.text
        assert response.status_code == 400


@freeze_time("2020-12-17T10:00:00Z")
def test_post_taf_without_uuid(client: TestClient, session: Session):
    '''Test trying to post a TAF without UUID'''

    fill_table(session)
    username = "testuser"

    # Lock TAF
    taflist = get_taflist(client)
    postbody = get_taf(client, taflist[0]['taf']['uuid']).json()
    postbody['editor'] = username
    patch_taf(client, postbody, username)

    # Post a TAF with missing field
    with open('app/tests/testdata/taf1.json', 'rb') as fh1:
        postbody['taf'] = json.load(fh1)['taf']
    postbody['changeStatusTo'] = 'DRAFT'

    # try to post a TAF without uuid
    del postbody['taf']['uuid']
    assert 'uuid' not in postbody
    assert 'uuid' not in postbody['taf']

    # check posting fails
    response = post_taf(client, postbody, username)
    assert "'TafPublish', 'uuid'" in response.text
    assert "'Field required'" in response.text
    assert response.status_code == 400
