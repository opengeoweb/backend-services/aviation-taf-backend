# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""Module to test TAF2TAC"""
import json
import logging
import sys

from fastapi.testclient import TestClient

from app.config import settings

# log stream handling
logger = logging.getLogger()
logger.level = logging.INFO
stream_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(stream_handler)

settings.geoweb_knmi_avi_messageservices_host = 'tac-converter'
mock_url = "http://" + (settings.geoweb_knmi_avi_messageservices_host or "")


def test_taf2tac(httpx_mock, client: TestClient, capsys):
    '''Test taf2tac connection'''
    # redirect stream to check logging messages
    stream_handler.stream = sys.stdout

    httpx_mock.add_response(url=mock_url + "/gettaftac",
                            method="POST",
                            json={
                                "data": "DUMMY TAC",
                                "status_code": 200,
                                "content": "TAC CONTENT"
                            })

    with open('app/tests/testdata/draft_taf_fortaf2tac.json', 'rb') as fh:
        response = client.post("/taf2tac", json=json.load(fh))
        assert response.status_code == 200
        # check logs
        captured = capsys.readouterr()
        assert "Received taf2tac with json" in captured.out
        assert "Calling http://tac-converter/gettaftac" in captured.out
        assert "http://tac-converter/gettaftac" in captured.out
        assert "TAC_body.status_code: 200" in captured.out


def test_taf2tac_error_400(httpx_mock, client: TestClient, capsys):
    '''Test taf2tac error 400 response'''
    # redirect stream to check logging messages
    stream_handler.stream = sys.stdout

    httpx_mock.add_response(url=mock_url + "/gettaftac",
                            method="POST",
                            json={},
                            status_code=400)

    with open('app/tests/testdata/draft_taf_fortaf2tac.json', 'rb') as fh:
        response = client.post("/taf2tac", json=json.load(fh))
        assert response.status_code == 400
        assert "TAF to TAC conversion failed" in response.text
        # check logs
        captured = capsys.readouterr()
        assert "Received taf2tac with json" in captured.out
        assert "TAC_body.status_code: 400" in captured.out
        assert "4XX or 500 error while calling TAF TAC service: " + \
            "{}. Error code: 400. Line" in captured.out


def test_taf2tac_error_500(httpx_mock, client: TestClient, capsys):
    '''Test taf2tac error 500 response'''
    # redirect stream to check logging messages
    stream_handler.stream = sys.stdout

    httpx_mock.add_response(url=mock_url + "/gettaftac",
                            method="POST",
                            json={},
                            status_code=500)

    with open('app/tests/testdata/draft_taf_fortaf2tac.json', 'rb') as fh:
        response = client.post("/taf2tac", json=json.load(fh))
        assert response.status_code == 400
        assert "TAF to TAC conversion failed" in response.text
        # check logs
        captured = capsys.readouterr()
        assert "Received taf2tac with json" in captured.out
        assert "TAC_body.status_code: 500" in captured.out
        assert "4XX or 500 error while calling TAF TAC service: {}. " + \
            "Error code: 500. Line" in captured.out


def test_taf2tac_error_502(httpx_mock, client: TestClient, capsys):
    '''Test taf2tac error 502 (>500) response'''
    # redirect stream to check logging messages
    stream_handler.stream = sys.stdout

    httpx_mock.add_response(url=mock_url + "/gettaftac",
                            method="POST",
                            json={},
                            status_code=502,
                            is_reusable=True)

    with open('app/tests/testdata/draft_taf_fortaf2tac.json', 'rb') as fh:
        response = client.post("/taf2tac", json=json.load(fh))
        assert response.status_code == 400
        assert "TAF to TAC conversion failed" in response.text
        # check logs
        captured = capsys.readouterr()
        assert "Received taf2tac with json" in captured.out
        assert "TAC_body.status_code: 502" in captured.out
        assert "Error while retrying to call TAF TAC service: {}. " +\
            "Error code: 502. Line" in captured.out
