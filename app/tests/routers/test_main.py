# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""Module to test main routers"""
import re

from fastapi.testclient import TestClient

from app.config import settings


def test_root_endpoint(client: TestClient) -> None:
    "Test root endpoint"

    resp = client.get('/')
    assert resp.status_code == 200
    assert resp.text == 'GeoWeb Aviation TAF API'


def test_version_endpoint(client: TestClient) -> None:
    "Test version endpoint"

    resp = client.get('/version')
    assert resp.status_code == 200
    assert resp.json() == {'version': settings.version}


def test_healthcheck_endpoint(client: TestClient, httpx_mock) -> None:
    """Test healthcheck endpoint"""
    if settings.aviation_taf_publish_host is None:
        settings.aviation_taf_publish_host = "publisher-host"
    if settings.geoweb_knmi_avi_messageservices_host is None:
        settings.geoweb_knmi_avi_messageservices_host = "message-service-host"
    httpx_mock.add_response(
        url=f"http://{settings.aviation_taf_publish_host}/healthcheck",
        method="GET",
        status_code=200)
    httpx_mock.add_response(
        url=f"http://{settings.geoweb_knmi_avi_messageservices_host}",
        method="GET",
        status_code=200)
    resp = client.get('/healthcheck')
    resp_json = resp.json()
    assert resp.status_code == 200
    assert resp_json['status'] == 'OK'
    assert resp_json['service'] == 'TAF'
    assert resp_json["publisher"]["status"] == "OK"
    assert resp_json["message-service"]["status"] == "OK"
    assert resp_json["database"]["status"] == "OK"


def test_aviversion_endpoint(client: TestClient) -> None:
    "Test aviversion endpoint"

    resp = client.get('/aviversion')
    assert resp.status_code == 200
    assert resp.json() == {'version': settings.avi_version}
