# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
# pylint: disable=singleton-comparison
"""This module handles the interaction with the database"""
from __future__ import annotations

import logging
from datetime import datetime, timedelta, timezone

from sqlalchemy import or_, select
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session

from app.models import Status, TafBase, TafDbModel, TafInterface
from app.utils.taf_lifecycle import get_previous_timeslot
from app.utils.taf_utils import get_lineno

from .errors import CreateError, DeleteError, ReadError, UpdateError


class CRUD:
    """CRUD operations for TAF"""

    _session: Session

    def __init__(self, session: Session) -> None:
        self._session = session

    def read_many(self) -> list[TafDbModel] | None:
        """:returns: a list of TAFs
        """

        # get result
        try:
            logging.info("Loading TAF from database")
            stmt = select(TafDbModel).order_by(TafDbModel.taf_id)
            return self._session.scalars(stmt).all()  # type: ignore
        except SQLAlchemyError as err:
            logging.error("Error while loading TAF from database",
                          extra={
                              "err": err,
                              "line": get_lineno(),
                          })
        return None

    # Read three latest tafs for each unique airport where status has not been superseded.
    def read_visible(self, airports: dict) -> list[TafDbModel] | None:
        """ Retrieve all TAFs that would be visible in the FE
        :airports: airport configuration (duration of slot and frequency)
        :returns: a list of TAFs that should be displayed by the frontend
        :raises ReadError:
        The upcoming, current and previous timeslots will be returned.
        """

        try:
            logging.info("Loading TAF from database")
            #Retrieve all valid TAFs (that is non-superseded Tafs and not expired)
            #ordered by creationDate and location

            # Make queries for each location and filter and sort the result in Python
            taflist: list[TafDbModel] = []
            reference_timestamp = datetime.now(timezone.utc)
            for location, timeslot_info in airports.items():
                start_cutoff = get_previous_timeslot(
                    timeslot_info, reference_timestamp)['start']

                stmt = select(TafDbModel).filter(
                    TafDbModel.taf["location"].as_string() == location,
                    TafDbModel.baseTime >= start_cutoff,
                    or_(
                        TafDbModel.superseded == None,  # type: ignore
                        TafDbModel.superseded == False))  # type: ignore

                res = self._session.scalars(stmt).all()

                # Check tafs and mark expired ones by replacing status field with EXPIRED
                for item in res:
                    # Retrieve baseTime without parsingobject and set tzinfo
                    basetime = item.baseTime.replace(tzinfo=timezone.utc)
                    if (basetime + timedelta(hours=int(timeslot_info['period']))
                       ) < reference_timestamp:
                        item.taf['status'] = Status.EXPIRED
                taflist.extend(res)
            # Sort by date before returning
            sortedtaflist = sorted(
                taflist,
                key=lambda x: (-x.creationDate.timestamp(), x.taf['location']))
            return sortedtaflist
        except SQLAlchemyError as err:
            err_msg = "Error while retrieving recent TAFs from database"
            logging.error(err_msg, extra={
                "err": err,
                "line": get_lineno(),
            })
            raise ReadError(err_msg) from err

    def read_one(self, taf_id: str) -> TafDbModel | None:
        """Reads one TAF.
        :param taf_id: ID of the TAF
        :returns TafDbModel|None:
        :raises ReadError:
        """

        logging.info("Loading TAF from database", extra={"taf_id": taf_id})
        try:
            stmt = select(TafDbModel).filter(
                TafDbModel.taf_id == taf_id)  # type: ignore
            return self._session.scalars(stmt).first()

        except SQLAlchemyError as err:
            err_msg = "Error while loading TAF from database"
            logging.error(err_msg, extra={
                "taf_id": taf_id,
                "err": err,
            })
            raise ReadError(err_msg) from err

    def count_rows(self) -> int:
        """Return the number of database rows on taf-table

        Returns:
            int: number of rows or -1 if there was an error
        """
        try:
            stmt = select(TafDbModel)
            return len(self._session.execute(stmt).all())
        except SQLAlchemyError as err:
            logging.error(err)
            return -1

    def create(self, data: TafInterface) -> TafDbModel:
        """Creates a TAF.

        :param data: TafInterface
        :returns: the created TAF
        :raises CreateError: if an error occurs
        """

        taf = TafDbModel(data)
        logging.info("inserting TAF into database")

        try:
            self._session.add(taf)
            self._session.commit()
        except SQLAlchemyError as err:
            err_msg = "Error while inserting TAF into database"
            logging.error(err_msg, extra={"err": err, "line": get_lineno()})
            # Log more details about the exception
            # https://stackoverflow.com/questions/36438052/using-a-custom-json-encoder-for-sqlalchemys-postgresql-jsonb-implementation
            logging.error("SQLAlchemyError details: %s", err, exc_info=True)
            raise CreateError(err_msg) from err
        return taf

    def update(self, taf: TafDbModel, data: TafInterface) -> TafDbModel:
        """Updates the provided TAF
        :param taf: the existing TAF
        :param data: the data to update
        :returns: the updated TAF.
        :raises UpdateError: if an error occurs
        """

        # update TAF
        logging.info("Updating TAF in database", extra={"taf_id": taf.taf_id})
        try:
            taf.update(data, taf.taf_id)
            self._session.commit()
        except SQLAlchemyError as err:
            err_msg = "Error while updating TAF in database"
            logging.error(err_msg,
                          extra={
                              "taf_id": taf.taf_id,
                              "err": err,
                              "line": get_lineno()
                          })
            raise UpdateError(err_msg) from err

        return taf

    def update_editor(self, taf: TafDbModel, editor: str | None) -> TafDbModel:
        """Updates the TAF editor
        :param taf: the existing TAF
        :param editor: the new editor
        :returns: the updated TAF.
        :raises UpdateError: if an error occurs
        """

        # update TAF
        logging.info("Updating TAF editor in database",
                     extra={
                         "taf_id": taf.taf_id,
                         "new_editor": editor,
                         "old_editor": taf.editor
                     })
        try:
            taf.update_editor(editor)
            self._session.commit()
        except SQLAlchemyError as err:
            err_msg = "Error while updating TAF editor in database"
            logging.error(err_msg,
                          extra={
                              "taf_id": taf.taf_id,
                              "new_editor": editor,
                              "old_editor": taf.editor,
                              "err": err,
                              "line": get_lineno()
                          })
            raise UpdateError(err_msg) from err

        return taf

    def delete(self, taf_id: str) -> bool:
        """Deletes the TAF with the given taf_id.
        :param taf_id: ID of the TAF
        :raises DeleteError: if not successfully deleted
        """

        # find TAF
        stmt = select(TafDbModel).filter(
            TafDbModel.taf_id == taf_id)  # type: ignore
        taf: TafDbModel | None = self._session.scalars(stmt).all()[
            0]  # type: ignore

        if not taf:
            err_msg = "TAF cannot be deleted by current user"
            logging.error(err_msg, extra={"taf_id": taf_id})
            raise DeleteError(err_msg)

        # delete TAF
        logging.info("Deleting TAF from database",
                     extra={
                         "taf_id": taf.taf_id,
                     })
        try:
            self._session.delete(taf)
            self._session.commit()
        except SQLAlchemyError as err:
            err_msg = "Error while deleting TAF from database"
            logging.error(err_msg,
                          extra={
                              "taf_id": taf.taf_id,
                              "err": err,
                              "line": get_lineno()
                          })
            raise DeleteError(err_msg) from err
        return True

    def has_draft(self, previous_taf: TafBase, draft_status: Status) -> bool:
        """Get candidate TAFs based on baseTime and superseded criteria"""

        # Retrieve draft candidates with matching baseTime
        stmt = select(TafDbModel).filter(
            TafDbModel.baseTime == previous_taf.baseTime,  # type: ignore
            TafDbModel.superseded == False)  # type: ignore
        candidate_tafs = self._session.scalars(stmt).all()

        # Filter the candidates based on the draft_status and previousId condition
        return any(
            candidate.taf.get('previousId', None) == previous_taf.uuid and
            Status(candidate.taf['status']) == draft_status
            for candidate in candidate_tafs)
