# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""TAF utils module"""

import logging
from datetime import datetime, timezone
from inspect import currentframe

import httpx
from fastapi import HTTPException

from app.config import settings
from app.models import TafPublish
from app.utils.client import AsyncHttpxClientWrapper
from app.utils.utils import DATE_FORMAT


class ConversionException(Exception):
    """Represents an error in the conversion"""


class Taf2Iwxxm30Exception(Exception):
    """Represents an error in the taf2iwxxm30 conversion"""


class Taf2TacException(Exception):
    """Represents an error in the taf2tac conversion"""


def get_lineno() -> str:
    """Helper function to get line number of warning/error"""
    curr_frame = currentframe()
    if curr_frame and curr_frame.f_back:
        return str(curr_frame.f_back.f_lineno)
    return ""


class Converter:
    '''Class for conversion of TAF to IWXXM21, IWXXM30 and TAC'''
    # pylint: disable=too-few-public-methods
    FORMAT_IWXXM21 = 'IWXXM21'
    FORMAT_IWXXM30 = 'IWXXM30'
    FORMAT_TAC = 'TAC'

    _tac_converter_endpoint: str

    def __init__(self, tac_converter_endpoint: str):
        self._tac_converter_endpoint = tac_converter_endpoint

    def _get_endpoint(self, conversion_type: str) -> str:
        if conversion_type == Converter.FORMAT_IWXXM21:
            return self._tac_converter_endpoint + '/gettafiwxxm21'
        if conversion_type == Converter.FORMAT_IWXXM30:
            return self._tac_converter_endpoint + '/gettafiwxxm30'
        if conversion_type == Converter.FORMAT_TAC:
            return self._tac_converter_endpoint + '/gettaftac'
        raise Exception('Unknown conversion type: ' + conversion_type)

    async def _do_request(self,
                          taf: TafPublish,
                          conversion_type: str,
                          headers=dict) -> httpx.Response:

        endpoint = self._get_endpoint(conversion_type)
        logging.info(f'Calling {endpoint} with {taf}')
        client = await AsyncHttpxClientWrapper.get_httpx_client()
        logging.info(taf.model_dump(mode='json', exclude_none=True))

        try:
            response = await client.post(endpoint,
                                         json=taf.model_dump(mode='json',
                                                             exclude_none=True),
                                         headers=headers)
            logging.info(
                f"{conversion_type}_body.status_code: {response.status_code}")
            return response
        except httpx.ConnectError as err:
            err_msg = "Connection error while calling TAF " + conversion_type + " service: " + str(
                err)
            logging.error(err_msg, extra={"line": get_lineno()})
            raise ConversionException(err_msg) from err
        except httpx.TimeoutException as err:
            err_msg = "Timeout error while calling TAF " + conversion_type + " service: " + str(
                err)
            logging.error(err_msg, extra={"line": get_lineno()})
            raise ConversionException(err_msg) from err
        except httpx.RequestError as err:
            err_msg = "Error while calling TAF " + conversion_type + " service: " + str(
                err)
            logging.error(err_msg, extra={"line": get_lineno()})
            raise ConversionException(err_msg) from err
        except httpx.HTTPError as err:
            err_msg = "HTTP error while calling TAF " + conversion_type + " service: " + str(
                err)
            logging.error(err_msg, extra={
                "line": get_lineno(),
            })
            raise ConversionException(err_msg) from err

    async def convert(self, taf: TafPublish, conversion_type: str,
                      headers: dict) -> httpx.Response:
        """Convert TAF into conversion type product"""
        response = await self._do_request(taf, conversion_type, headers)

        # For 4xx and 500 errors - log an error and don't retry
        if response.status_code >= 400 and response.status_code <= 500:
            err_msg = "4XX or 500 error while calling TAF " + conversion_type + \
                " service: " + response.text + ". Error code: " + str(
                response.status_code)
            logging.error(f"{err_msg}. Line {get_lineno()}")
            raise ConversionException(err_msg)

        # For 502, 503, 504 do a retry, if still fails log an error
        if response.status_code > 500:
            logging.info(
                (f"5XX error while calling TAF {conversion_type} services: \
                {response.text}. Error code: {response.status_code}. \
                Line {get_lineno()}. Retrying....."))
            # Try again to see if that fixes it
            response = await self._do_request(taf, conversion_type, headers)
            if response.status_code != 200:
                err_msg = "Error while retrying to call TAF " + \
                    conversion_type + " service: " + response.text
                logging.error(
                    f"{err_msg}. Error code: {response.status_code}. Line {get_lineno()}"
                )
                raise ConversionException(err_msg)

        return response


async def taf2tac(data: TafPublish):
    "Convert a TAF JSON to a TAC message"

    # check if messagesservice has been set up correctly
    if not isinstance(
            geoweb_avi_service := settings.geoweb_knmi_avi_messageservices_host,
            str):
        raise HTTPException(
            status_code=400,
            detail="GEOWEB_KNMI_AVI_MESSAGESERVICES_HOST not set")
    tac_converter = "http://" + geoweb_avi_service

    # set up converter and convert
    converter = Converter(tac_converter)

    headers = {'Content-Type': 'application/json', 'Accept': 'plain/text'}
    try:
        tac_body = await converter.convert(data, Converter.FORMAT_TAC, headers)
        # check response and try to return
        if tac_body.status_code != 200:
            logging.warning((
                "Error while calling TAF to plain TAC "+\
                    "service: %s. Line {get_lineno()}", tac_body.text
            ))
        return tac_body.text
    except ConversionException as exc:
        logging.error((
            f"Error while calling TAF to plain TAC service: {exc}. Line {get_lineno()}"
        ))
        raise Taf2TacException("TAF to TAC conversion failed") from exc


async def taf2tac_adverse(data: TafPublish):
    "Convert a TAF JSON to a TAC message"

    # check if messagesservice has been set up correctly
    if not isinstance(
            geoweb_avi_service := settings.geoweb_knmi_avi_messageservices_host,
            str):
        raise HTTPException(
            status_code=400,
            detail="GEOWEB_KNMI_AVI_MESSAGESERVICES_HOST not set")
    tac_converter = "http://" + geoweb_avi_service

    # set up converter and convert
    converter = Converter(tac_converter)

    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    try:
        tac_body = await converter.convert(data, Converter.FORMAT_TAC, headers)
        # check response and try to return
        if tac_body.status_code != 200:
            logging.warning((
                "Error while calling TAF to adverse TAC "+\
                    "service: %s. Line {get_lineno()}", tac_body.text
            ))
        return tac_body.json()
    except ConversionException as exc:
        logging.error((
            f"Error while calling TAF to adverse TAC service: {exc}. Line {get_lineno()}"
        ))
        raise Taf2TacException("TAF to Adverse TAC conversion failed") from exc


async def taf2iwxxm30(data: TafPublish):
    "Convert a TAF JSON to a IWXXM message"

    # check if messagesservice has been set up correctly
    if not isinstance(
            geoweb_avi_service := settings.geoweb_knmi_avi_messageservices_host,
            str):
        raise HTTPException(
            status_code=400,
            detail="GEOWEB_KNMI_AVI_MESSAGESERVICES_HOST not set")
    iwxxm30_converter = "http://" + geoweb_avi_service

    # set up converter and convert
    converter = Converter(iwxxm30_converter)

    headers = {'Content-Type': 'application/json'}
    try:
        iwxxm30_body = await converter.convert((data), Converter.FORMAT_IWXXM30,
                                               headers)
        # check response and try to return
        if iwxxm30_body.status_code != 200:
            logging.warning((
                f"Error while calling TAF to IWXXM: {iwxxm30_body.text}. Line {get_lineno()}"
            ))
        return iwxxm30_body.text
    except ConversionException as exc:
        logging.error(
            (f"Error while calling TAF to IWXXM: {exc}. Line {get_lineno()}"))
        raise Taf2Iwxxm30Exception("TAF to IWXXM30 conversion failed") from exc


def audit_log(product_id, label, action, user_id):
    """ Audit logs """
    ts = datetime.now(timezone.utc).strftime(DATE_FORMAT)
    print(
        f"audit-logging - {ts} - {product_id} - {label} - {action} - {user_id}")
