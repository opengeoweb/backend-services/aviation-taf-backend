# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
""" Utils """

import json
import logging
from datetime import datetime, timezone

from pydantic_core import to_jsonable_python

from app.config import settings
from app.models import CanBe, DataToFE, TafDbModel, TafDraft
from app.utils.client import AsyncHttpxClientWrapper


def _pydantic_json_serializer(*args, **kwargs) -> str:
    """
    Encodes json in the same way as pydantic.
    """
    return json.dumps(*args, default=to_jsonable_python, **kwargs)


async def post_file_to_service(filename: str, data: str) -> bool:
    """Post a string to given service, returns True when successfull"""
    if isinstance((publish_host := settings.aviation_taf_publish_host), str):
        headers = {'Content-Type': 'application/json'}
        client = await AsyncHttpxClientWrapper.get_httpx_client()
        try:
            response = await client.post("http://" + publish_host +
                                         '/publish/' + filename,
                                         json={"data": data},
                                         headers=headers)
            if response.status_code != 200:
                logging.error(
                    'Unable to post to service %s, got response code %s',
                    publish_host, response.status_code)
                return False
        except Exception as exc:
            logging.error('Unable to post to service %s, error %s',
                          publish_host, exc)
            return False
        return True
    # publish_host has not been retrieved
    logging.error('Unable to retrieve publish service')
    return False


def split_into_lines(tac):
    """Splits a TAC into lines of max 69 characters"""
    maxlen = 69
    input_lines = tac.splitlines()
    output_lines = []
    for input_line in input_lines:
        if len(input_line) <= maxlen:
            output_lines.append(input_line)
        else:
            output_line = ""
            for word in input_line.split():
                if len(output_line) + len(word) + 1 <= maxlen:
                    if len(output_line) > 0:
                        output_line += " " + word
                    else:
                        output_line = word
                else:
                    output_lines.append(output_line)
                    output_line = word
            if len(output_line) > 0:
                output_lines.append(output_line)
    return "\n".join(output_lines)


# ISO 8601 date format, in UTC
DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'


def format_data_for_fe(data: TafDbModel, list_can_be: list[CanBe]) -> DataToFE:
    """Data transformer to return TAF to FE"""
    # this funtion is intended to be a temporary solution to make sure
    # that data is sent to the FE with the correct (datetime)
    # formatting
    # with Pydantic V2 we could escape the need to use of taf_encoder()
    # followed by a json.loads statement
    return DataToFE(
        taf=TafDraft(**data.taf).model_dump(exclude_none=True),  # type: ignore
        creationDate=data.creationDate or datetime.now(timezone.utc),
        editor=data.editor,
        canbe=list_can_be)
