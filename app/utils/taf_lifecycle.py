# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""TAF lifecylce utils"""

import configparser
import logging
import math
from configparser import ConfigParser
from datetime import datetime, timedelta, timezone

from app.config import settings
from app.models import CanBe, Status


def get_start_end_timeslot(airport: dict,
                           reference_timestamp: datetime) -> dict:
    '''Get start and end timeslot'''
    current_hour = reference_timestamp.hour
    # find start time for slot
    base = int(airport['period'])
    # Number will be between 0 and 23
    start_hour = base * math.floor(current_hour / base) % 24
    start: datetime = reference_timestamp.replace(hour=start_hour,
                                                  minute=0,
                                                  second=0,
                                                  microsecond=0,
                                                  tzinfo=timezone.utc)
    end: datetime = start + timedelta(hours=int(airport['validity']))
    return {'start': start, 'end': end}


def get_upcoming_timeslot(airport: dict, reference_timestamp: datetime) -> dict:
    '''Get upcoming timeslot'''
    return get_start_end_timeslot(
        airport, reference_timestamp + timedelta(hours=int(airport['period'])))


def get_current_timeslot(airport: dict, reference_timestamp: datetime) -> dict:
    '''Get current timeslot'''
    return get_start_end_timeslot(airport, reference_timestamp)


def get_previous_timeslot(airport: dict, reference_timestamp: datetime) -> dict:
    '''Get previous timeslot'''
    return get_start_end_timeslot(
        airport, reference_timestamp - timedelta(hours=int(airport['period'])))


def set_taf_config(path: str | None = None) -> tuple[ConfigParser, dict]:
    "Config the TAF BE based on the config file"
    # Airport default configuration (must invoke set_config to set it up)
    airports = {}
    taf_config = configparser.ConfigParser()
    if path is None:
        logging.info('Default config file')
        taf_config.read(settings.taf_config)
    else:
        taf_config.read(str(path))
    for section in taf_config.sections():
        if section not in ('LOCATION_INDICATOR_MWO', 'TAF_HEADER',
                           "BACKEND_CONFIG"):
            airports[section] = {
                'validity': taf_config[section]['validity_duration'],
                'period': taf_config[section]['activity_period']
            }
    return taf_config, airports


def get_allowed_actions(status: Status) -> list[CanBe]:
    """Calculate canbe vector according to status"""
    # pylint: disable=too-many-return-statements
    if status == Status.NEW:
        return [CanBe.DRAFTED, CanBe.DISCARDED, CanBe.PUBLISHED]
    if status == Status.DRAFT:
        return [CanBe.DRAFTED, CanBe.DISCARDED, CanBe.PUBLISHED]
    if status == Status.PUBLISHED:
        return [CanBe.CANCELLED, CanBe.AMENDED, CanBe.CORRECTED]
    if status == Status.AMENDED:
        return [CanBe.CANCELLED, CanBe.AMENDED, CanBe.CORRECTED]
    if status == Status.DRAFT_AMENDED:
        return [CanBe.DRAFTED, CanBe.DISCARDED, CanBe.AMENDED]
    if status == Status.CORRECTED:
        return [CanBe.CANCELLED, CanBe.AMENDED, CanBe.CORRECTED]
    if status == Status.DRAFT_CORRECTED:
        return [CanBe.DRAFTED, CanBe.DISCARDED, CanBe.CORRECTED]
    if status == Status.CANCELLED:
        return [CanBe.AMENDED]
    return []
