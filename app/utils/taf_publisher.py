# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""TAF publisher module"""

import configparser
import logging
# Datetime formats
from datetime import datetime, timedelta, timezone

from pyparsing import MutableMapping

from app.config import settings
from app.models import Status, TafPublish
from app.utils.taf_utils import Taf2Iwxxm30Exception, Taf2TacException, \
    audit_log, taf2iwxxm30, taf2tac
from app.utils.utils import post_file_to_service, split_into_lines

DEFAULT_DT_FMT = "%Y-%m-%dT%H:%M:%SZ"
BULLETIN_DT_FMT = "%Y-%m-%d%H%M"
PUBLISH_DT_FMT = "%Y%m%d%H%M%S"
HEADER_DT_FMT = "%d%H%M"


class TAFPublisher:
    """TAF Publisher"""

    config: MutableMapping

    def __init__(self):
        """ Init """
        self.config = configparser.ConfigParser()
        self.config.read(settings.taf_config)

    def get_status(self, tafstatus: Status) -> str:
        """ Return status string """
        if tafstatus == Status.AMENDED:
            return 'AMD'
        if tafstatus == Status.CORRECTED:
            return 'COR'
        if tafstatus == Status.CANCELLED:
            return 'CNL'
        return ""

    def get_taf_headertime(self, taf: TafPublish) -> datetime:
        """ Return TAF headertime """
        return taf.baseTime - timedelta(hours=1)

    def make_tac_header(self, taf: TafPublish) -> str:
        """ Return TAC header """
        headertime = self.get_taf_headertime(taf)
        headertime_formatted = datetime.strftime(headertime, HEADER_DT_FMT)

        taf_groupname = self.config['TAF_HEADER']['TAFGROUPNAME']
        taf_location_indicator = self.config['LOCATION_INDICATOR_MWO'][
            'indicator']

        # If status is COR / AMD / CNL and NOT an emptry string,
        # a space should be added in front for the creation
        # of a TAC
        # if it is an empty string, nothing should be added
        if isinstance(status := self.get_status(taf.status),
                      str) and status != "":
            status = f' {status}'

        return f"{taf_groupname} {taf_location_indicator} {headertime_formatted}{status}\n"

    async def publish_tac(self, taf: TafPublish, username: str) -> bool:
        """ Publish a TAF as TAC, returns True on success """

        # Valid TAF TAC name is: TAF_CCCC_yyyy-mm-dduummTyyyymmdduummss.tac
        # With:
        # CCCC = ICAO station identifier
        # yyyy-mm-dduumm = validity/bulletin header time (basetime - 1 hr)
        # yyyymmdduummss = time of publishing TAF

        headertime = self.get_taf_headertime(taf)
        bulletin_date = datetime.strftime(headertime, BULLETIN_DT_FMT)
        publish_date = datetime.strftime(datetime.now(timezone.utc),
                                         PUBLISH_DT_FMT)

        tac_name = "TAF_" + taf.location + '_' + bulletin_date + 'T' + publish_date + '.tac'
        tac_header = self.make_tac_header(taf)
        try:
            response = await taf2tac(taf)
            taf_tac = response.rstrip("\n")
        except Taf2TacException:
            return False
        tac_footer = '\n'

        tac_with_header_and_footer = f"{tac_header}{taf_tac}{tac_footer}"

        full_tac = split_into_lines(tac_with_header_and_footer)

        logging.info(f"result: {full_tac}")

        success = await post_file_to_service(
            tac_name,
            full_tac,
        )

        if not success:
            logging.error(
                "[ERROR] Error trying post_file_to_service: Published tac failed"
            )
            return False

        logging.info("[OK] post_file_to_service: Succesfully published tac")
        audit_log(taf.uuid, "taf", "published", username)
        return True

    async def publish_iwxxm30(self, taf: TafPublish, username: str) -> bool:
        """ Publish a TAF as an IWXXM3.0 product, returns True on success"""
        try:
            response = await taf2iwxxm30(taf)
            taf_as_iwxxmm = response.rstrip('\n')
        except Taf2Iwxxm30Exception:
            return False
        logging.info(f"full_iwxxm size: {str(len(taf_as_iwxxmm))}")
        logging.info(f">>>\n {taf_as_iwxxmm}")

        # Valid iwxxm30 name should be: A_LTNL99EHDByyGGggBBB_C_EH??_yyyymmdduummss.xml
        # A_LTNL99EHDB = fixed
        # yyGGgg = day of month, hour, min of bulletin time (baseTime - 1 hour)
        # BBB = type in case of correction (AMD/COR/CNL), not present if original TAF
        # _C_ = fixed
        # CCCC = ICAO station identifier
        # yyyymmdduummss = year-month-day-hour-second of issuing TAF

        # set yyGGgg (day of month-hour-min) as bulletin time (basetime - 1 hour)
        headertime = headertime = self.get_taf_headertime(taf)
        bulletin_date = datetime.strftime(headertime, HEADER_DT_FMT)
        publish_date = datetime.strftime(datetime.now(timezone.utc),
                                         PUBLISH_DT_FMT)

        taf_location_indicator = self.config['LOCATION_INDICATOR_MWO'][
            'indicator']
        status = self.get_status(taf.status)
        location = taf.location
        iwxxm30_bull_header = f"A_LTNL99{taf_location_indicator}{bulletin_date}"
        iwxxm30_name = f"{iwxxm30_bull_header}{status}_C_{location}_{publish_date}.xml"
        logging.info(f"Saving iwxxm30 to file {iwxxm30_name}")

        success = await post_file_to_service(
            iwxxm30_name,
            taf_as_iwxxmm,
        )
        if not success:
            logging.error(
                "[ERROR] Error trying post_file_to_service: Published iwxxm30 failed"
            )
            return False

        logging.info("[OK] post_file_to_service: Succesfully published iwxxm30")
        audit_log(taf.uuid, "taf", "published", username)
        return True

    async def publish_taf(self, taf: TafPublish,
                          user: str) -> tuple[bool, list[str]]:
        """Publish a taf, returns True on success"""

        # Publish IWXXM30
        iwxxm30_success = await self.publish_iwxxm30(taf, user)
        if (iwxxm30_success is False) and self.config['BACKEND_CONFIG'][
                'exception_on_iwxxm30_publish_fail'] == 'True':
            return False, [
                "Unable to publish TAF", "Publishing of IWXXM30 failed"
            ]

        # Publish TAC
        tac_success = await self.publish_tac(taf, user)
        if (tac_success is False) and self.config['BACKEND_CONFIG'][
                'exception_on_tac_publish_fail'] == 'True':
            return False, ["Unable to publish TAF", "Publishing of TAC failed"]

        return True, []
