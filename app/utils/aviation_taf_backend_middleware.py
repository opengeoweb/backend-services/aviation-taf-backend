# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""AddAviationTafBackendMiddleWare"""

import logging
import sys

from starlette.types import ASGIApp

from app.config import settings


class AddAviationTafBackendMiddleWare:
    """ AddAviationTafBackendMiddleWare """

    # pylint: disable=too-few-public-methods

    def __init__(
        self,
        app: ASGIApp,
    ) -> None:
        self.app = app

    async def __call__(self, scope, receive, send):
        if "pytest" not in sys.modules and settings.geoweb_username:
            logging.info(
                f"Overriding GEOWEB_USERNAME from env with \"{settings.geoweb_username}\""
            )
            assert scope["type"] == "http"
            headers = dict(scope["headers"])
            headers[b"geoweb-username"] = settings.geoweb_username.encode()
            scope["headers"] = list(headers.items())
            await self.app(scope, receive, send)
        else:
            await self.app(scope, receive, send)
