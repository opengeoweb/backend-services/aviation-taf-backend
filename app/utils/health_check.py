# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""AddAviationTafBackendMiddleWare"""

import logging

from httpx import HTTPError
from sqlmodel import Session

from app.config import settings
from app.crud.taf import CRUD
from app.utils.client import AsyncHttpxClientWrapper


def alert_with_message(message):
    """Log a warning """
    # if health check fails, log a descriptive warning
    # but keep application running
    logging.warning(
        f"{message}. Application works but this (micro-)service is unavailable")


async def check_webservice_status(host, check_url=""):
    """ Checks the status of a webservice"""

    status_code = 0
    url = f"http://{host}{check_url}"

    client = await AsyncHttpxClientWrapper.get_httpx_client()

    try:
        resp = await client.get(url)
        status_code = resp.status_code
    except HTTPError as err:
        logging.error(err)

    if status_code != 200:
        alert_with_message(
            f"Service with url {url} did not return status code 200, "
            f"status code was: {status_code}")
        return False
    logging.info(f"[OK] \"{host}\"")
    return True


def check_database(session: Session, message: dict) -> bool:
    """Checks database health by counting table rows

    Args:
        session (Session): database session
        message (dict): status message used to report healthcheck

    Returns:
        bool: True if database operation is successful
    """
    healthy: bool
    try:
        crud = CRUD(session)
        if crud.count_rows() >= 0:
            message["database"]["taf-table"] = {"status": "OK"}
            message["database"].update({"status": "OK"})
            healthy = True
        else:
            message["database"]["taf-table"] = {"status": "FAILED"}
            message["database"].update({"status": "FAILED"})
            healthy = False
    except TypeError as te:
        message["database"]["taf-table"] = {"status": "FAILED"}
        message["database"].update({"status": "FAILED"})
        logging.error(te)
        healthy = False

    return healthy


async def check_health(session: Session, message: dict) -> bool:
    """Function to check health of all subsystems

    Args:
        session (Session): database session
        message (dict): status message used to report healthcheck

    Returns:
        bool: True if all subsystems are operational
    """
    healthy = True
    services = {
        "message-service": [settings.geoweb_knmi_avi_messageservices_host],
        "publisher": [settings.aviation_taf_publish_host, "/healthcheck"]
    }
    for service_name, params in services.items():
        if not await check_webservice_status(*params):
            message[service_name] = {"status": "FAILED"}
            healthy = False
        else:
            message[service_name] = {"status": "OK"}

    if not check_database(session, message):
        message["database"].update({"status": "FAILED"})
        healthy = False

    return healthy
