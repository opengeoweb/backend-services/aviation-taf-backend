# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""Module for application-wide settings"""
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    """BaseSettings class"""

    model_config = SettingsConfigDict(env_nested_delimiter="__",
                                      env_file=".env",
                                      extra="ignore",
                                      title="Env Config")

    version: str = "0.0.0"
    avi_version: str = "0.0.0"
    geoweb_knmi_avi_messageservices_host: str | None = None
    aviation_taf_publish_host: str | None = None
    aviation_taf_backend_db: str = (
        "postgresql://geoweb:geoweb@aviation-taf-backend-db:5432/"
        "aviationtafbackenddb")
    geoweb_username: str | None = None
    taf_config: str = "config.ini"
    application_root_path: str = ""
    application_doc_root: str = "/api"


settings = Settings()
