# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""Main module, defines the application"""

from contextlib import asynccontextmanager

from fastapi import FastAPI, Request
from fastapi.exceptions import HTTPException, RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse

from app.config import settings
from app.logger import configure_logger
from app.routers import main, taf
from app.routers.warnings import EditorWarning
from app.utils.aviation_taf_backend_middleware import \
    AddAviationTafBackendMiddleWare
from app.utils.client import AsyncHttpxClientWrapper

configure_logger()


# set up async client on startup and shutdown with lifespan
@asynccontextmanager
async def lifespan(app: FastAPI):
    """Lifespan specification"""
    # pylint: disable = redefined-outer-name,unused-argument
    # Get async client
    await AsyncHttpxClientWrapper.get_httpx_client()
    yield
    # Clean up async client
    await AsyncHttpxClientWrapper.close_httpx_client()


app = FastAPI(root_path=settings.application_root_path,
              docs_url=settings.application_doc_root,
              lifespan=lifespan)
app.include_router(main.router)
app.include_router(taf.router)
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["OPTIONS", "GET", "POST", "PATCH"],
    expose_headers=["Authorization", "Content-Type", "Location"],
    allow_headers=["*"],
)

app.add_middleware(AddAviationTafBackendMiddleWare)


@app.middleware("http")
async def add_headers(request: Request, call_next):
    """Adds security headers"""
    response = await call_next(request)
    response.headers['X-Content-Type-Options'] = "nosniff"
    response.headers[
        'Strict-Transport-Security'] = "max-age=31536000; includeSubDomains"
    return response


@app.exception_handler(HTTPException)
async def http_exception_handler(_: Request, exc: HTTPException):
    """Converts a HTTPException to a HTTP response"""

    return JSONResponse({'message': exc.detail},
                        status_code=exc.status_code,
                        headers=exc.headers)


@app.exception_handler(EditorWarning)
async def editor_warning_handler(_: Request, exc: EditorWarning):
    """Returns a warning message in case of TAF takeover"""
    return JSONResponse({'WarningMessage': exc.detail},
                        status_code=exc.status_code,
                        headers=exc.headers)


@app.exception_handler(RequestValidationError)
async def request_validation_error_handler(_: Request,
                                           exc: RequestValidationError):
    """Validation error response"""
    return JSONResponse({'message': str(exc)}, status_code=400, headers={})
