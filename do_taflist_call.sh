#!/usr/bin/env python
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)

# HOW TO RUN: you can run this script with the command: bash do_taflist_call.sh "aviation URL" "contents of bearer token without 'Bearer'"

echo "Retrieve taflist"

# remove previous file if exists
rm -f migrationdata/taflist.json

# create directory
mkdir -p migrationdata

# retrieve taflist
echo "Base URL: ${1}"
taflist=$(curl -v  GET $1/taflist -H "Authorization: Bearer $2")

echo "Save result: $taflist"

# save to file
echo "Save file"
echo $taflist >migrationdata/taflist.json
