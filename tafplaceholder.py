# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"Code to generate TAF placeholders"
from __future__ import annotations

import logging
import os
import uuid
from datetime import datetime, timedelta, timezone
from time import sleep

from sqlalchemy.orm import Session

from app.crud.errors import CreateError
from app.crud.taf import CRUD
from app.db import engine, get_session
from app.logger import configure_logger
from app.models import DraftBaseForecast, MessageType, Status, TafDbModel, \
    TafDraft, TafInterface, Valid
from app.utils.taf_lifecycle import get_current_timeslot, \
    get_previous_timeslot, get_upcoming_timeslot, set_taf_config

# pylint: disable=redefined-outer-name,not-context-manager


def config_logs():
    """Helper function: based on value of CONFIG_LOGS variable,
    configure the logs explicitly"""
    return os.getenv("CONFIG_LOGS", "").lower() in {"true", "1"}


# If running as a container service, the logging should be configured here explicitly.
# If this code is run for testing (either locally or on gitlab_ci), the tafplaceholder
# code is run together along with the code in the `app` directory. Then the logs are
# already configured for the tafplaceholder code and do not have to be configured again
if config_logs():
    configure_logger()


def make_placeholder(airport: str, timeslot: dict) -> TafInterface:
    '''
    This function creates a placeholder TAF for a given airport and timeslot
    '''

    ## create placeholder
    placeholder_uuid = str(uuid.uuid1())
    forecast_valid = Valid(start=timeslot['start'], end=timeslot['end'])
    base_forecast = DraftBaseForecast(valid=forecast_valid)
    taf_inner = TafDraft(uuid=placeholder_uuid,
                         baseTime=timeslot['start'],
                         validDateStart=timeslot['start'],
                         validDateEnd=timeslot['end'],
                         location=airport,
                         status=Status('NEW'),
                         messageType=MessageType('ORG'),
                         baseForecast=base_forecast)
    placeholder = TafInterface(creationDate=timeslot['start'], taf=taf_inner)
    return placeholder


def make_and_store_placeholder(airport: str,
                               timeslot: dict,
                               session: Session | None = None) -> None:
    '''
    This function tries to save a taf placeholder for a specified airport
    and timeslot in the database
    '''

    if session is None:
        with get_session() as default_session:
            session = default_session

    # generate placeholder
    taf_placeholder = make_placeholder(airport, timeslot)
    logging.info("TAF_PH %s %s", taf_placeholder.creationDate,
                 type(taf_placeholder.creationDate))

    # save placeholder in database
    try:
        CRUD(session).create(taf_placeholder)
    except CreateError:
        logging.error('Error while inserting TAF placeholder in database')


def cleanup_algorithm(cleanup_timestamp: datetime,
                      session: Session | None = None) -> bool | None:
    '''Code for cleanup algorithm'''

    if session is None:
        with get_session() as default_session:
            session = default_session

    if not isinstance(query := CRUD(session).read_many(), list):
        return None

    logging.info("Cleaning up TAFs before %s", cleanup_timestamp)

    tafs_to_delete = [
        item.taf_id
        for item in query
        if item.taf['baseTime'] < cleanup_timestamp.isoformat()
    ]

    logging.info('Found %s tafs to delete', len(tafs_to_delete))

    for taf_id in tafs_to_delete:
        if not CRUD(session).delete(taf_id):
            logging.error('Error deleting TAF from database',
                          extra={'taf_id': taf_id})
            return False
    return True


def filter_tafs(location,
                basetime,
                session: Session | None = None) -> list[TafDbModel]:
    '''Filter tafs on location and basetime'''

    if session is None:
        with get_session() as default_session:
            session = default_session

    # this returns TAF DB model with dates as strings
    if not isinstance(query := CRUD(session).read_many(), list):
        return []
    return [
        item for item in query
        if item.taf['location'] == location and item.taf['baseTime'] == basetime
    ]


def run_placeholder_algorithm(session: Session | None = None) -> None:
    '''Trigger function to run algorithm'''
    if session is None:
        with get_session() as default_session:
            session = default_session
    reference_timestamp = datetime.now(timezone.utc)
    logging.info('Run placeholder & cleanup algorithm at %s',
                 reference_timestamp)

    logging.info('Loading config')
    _, airports = set_taf_config()
    if len(airports) <= 0:
        logging.info('Config file not found')

    # check presence of tafs per airport in config
    for airport, conf in airports.items():

        # get timeslots
        current_timeslot = get_current_timeslot(conf, reference_timestamp)
        upcoming_timeslot = get_upcoming_timeslot(
            conf, reference_timestamp=reference_timestamp)
        previous_timeslot = get_previous_timeslot(conf, reference_timestamp)

        ## query through database for TAFs with current timeslot as baseTime
        ## look for baseTime as this is never mutated
        current_tafs = filter_tafs(
            airport,
            datetime.strftime(current_timeslot['start'], "%Y-%m-%dT%H:%M:00Z"),
            session)
        logging.info('Found %s current TAF(s) for %s', len(current_tafs),
                     airport)
        if len(current_tafs) == 0:
            logging.info('Create TAF for current timeslot for %s', airport)
            make_and_store_placeholder(airport, current_timeslot, session)

        ## query through database for TAFs with upcoming timeslot as baseTime
        ## look for baseTime as this is never mutated
        upcoming_tafs = filter_tafs(
            airport,
            datetime.strftime(upcoming_timeslot['start'], "%Y-%m-%dT%H:%M:00Z"),
            session)
        logging.info('Found %s upcoming TAF(s) for %s', len(upcoming_tafs),
                     airport)
        if len(upcoming_tafs) == 0:
            logging.info('Create TAF for upcoming timeslot for %s', airport)
            make_and_store_placeholder(airport, upcoming_timeslot, session)

        ## query through database for TAFs with previous timeslot as baseTime
        ## look for baseTime as this is never mutated
        previous_tafs = filter_tafs(
            airport,
            datetime.strftime(previous_timeslot['start'], "%Y-%m-%dT%H:%M:00Z"),
            session)
        logging.info('Found %s previous TAF(s) for %s', len(previous_tafs),
                     airport)
        if len(previous_tafs) == 0:
            logging.info('Create TAF for previous timeslot for %s', airport)
            make_and_store_placeholder(airport, previous_timeslot, session)

    ## clean up TAFs older than 10 days
    cleanup_timestamp = reference_timestamp - timedelta(days=10)
    cleanup_algorithm(cleanup_timestamp.replace(tzinfo=timezone.utc), session)

    logging.info("Finished running TAF placeholder & clean-up algorithm")


if __name__ == '__main__':

    while True:
        with Session(engine) as session:

            # try database connection with taflist call
            tafs = CRUD(session).read_many()

            # continue if alright
            if isinstance(tafs, list):
                logging.info('Start algorithm')
                run_placeholder_algorithm(session)
                SET_SLEEP = 60

            # often during startup it is required to take a while
            # else retry in a short time
            else:
                logging.info('Invalid database connection - retry')
                SET_SLEEP = 2

        if os.getenv('TAFPLACEHOLDER_KEEPRUNNING', "false") == "false":
            logging.info('Done, exiting taf placeholder.')
            break

        sleep(SET_SLEEP)
