# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
'''Code to test tafplaceholder algorithm'''

from collections.abc import Generator
from datetime import timedelta

import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient
from freezegun import freeze_time
from sqlmodel import Session, SQLModel, create_engine
from sqlmodel.pool import StaticPool

from app.db import get_session
from app.main import app as app_for_testing
from app.utils.utils import _pydantic_json_serializer
from tafplaceholder import run_placeholder_algorithm

# pylint: disable=redefined-outer-name,duplicate-code


@pytest.fixture
def session():
    """Yields a database session fixture"""
    engine = create_engine('sqlite://',
                           connect_args={"check_same_thread": False},
                           poolclass=StaticPool,
                           json_serializer=_pydantic_json_serializer)
    SQLModel.metadata.create_all(engine)
    with Session(engine) as session:
        yield session


@pytest.fixture
def app() -> Generator[FastAPI, None, None]:
    """Yields the application configured in testing mode"""
    yield app_for_testing


@pytest.fixture
def client(app: FastAPI, session: Session) -> Generator[TestClient, None, None]:
    """Yields a test client"""

    def get_session_override():
        return session

    app.dependency_overrides[get_session] = get_session_override
    client = TestClient(app)
    yield client
    app.dependency_overrides.clear()


def test_placeholder_algorithm(client, session) -> None:
    '''Test placeholder algorithm'''

    with freeze_time('2022-01-01T14:55:00Z') as frozen_time:
        ## run the algorithm
        with session as test_session:
            run_placeholder_algorithm(test_session)

        # retrieve database contents with taflist
        response = client.get('/taflist',
                              headers={'Geoweb-Username': 'test-user-1'})
        # check the response
        assert response.status_code == 200
        assert response.headers.get('content-type') == 'application/json'

        # check the response contents
        tafs = response.json()
        assert isinstance(tafs, list)

        # check placeholder contents
        for ix in range(0, 6):
            assert tafs[ix]['taf']['status'] == 'NEW'
            assert tafs[ix]['creationDate'] == '2022-01-01T18:00:00Z'
            assert tafs[ix]['taf']['validDateStart'] == '2022-01-01T18:00:00Z'
            assert tafs[ix]['taf']['validDateEnd'] == '2022-01-03T00:00:00Z'
        for ix in range(7, 12):
            assert tafs[ix]['taf']['status'] == 'NEW'
            assert tafs[ix]['creationDate'] == '2022-01-01T12:00:00Z'
            assert tafs[ix]['taf']['validDateStart'] == '2022-01-01T12:00:00Z'
            assert tafs[ix]['taf']['validDateEnd'] == '2022-01-02T18:00:00Z'
        assert len(tafs) == 18

        # Execute a second time, verify no TAFs are added
        with session as test_session:
            run_placeholder_algorithm(test_session)
        response = client.get('/taflist',
                              headers={'Geoweb-Username': 'test-user-1'})
        tafs = response.json()
        assert len(tafs) == 18

        # make a jump in time, verify new taf placeholders have been added
        frozen_time.tick(delta=timedelta(hours=6))
        with session as test_session:
            run_placeholder_algorithm(test_session)
        response = client.get('/taflist',
                              headers={'Geoweb-Username': 'test-user-1'})
        tafs = response.json()
        assert len(tafs) == 18

        ## again check order and contents
        for ix in range(6):
            assert tafs[ix]['taf']['status'] == 'NEW'
            assert tafs[ix]['creationDate'] == '2022-01-02T00:00:00Z'
            assert tafs[ix]['taf']['validDateStart'] == '2022-01-02T00:00:00Z'
            assert tafs[ix]['taf']['validDateEnd'] == '2022-01-03T06:00:00Z'
        for ix in range(7, 12):
            assert tafs[ix]['taf']['status'] == 'NEW'
            assert tafs[ix]['creationDate'] == '2022-01-01T18:00:00Z'
            assert tafs[ix]['taf']['validDateStart'] == '2022-01-01T18:00:00Z'
            assert tafs[ix]['taf']['validDateEnd'] == '2022-01-03T00:00:00Z'
        for ix in range(13, 18):
            assert tafs[ix]['taf']['status'] == 'EXPIRED'
            assert tafs[ix]['creationDate'] == '2022-01-01T12:00:00Z'
            assert tafs[ix]['taf']['validDateStart'] == '2022-01-01T12:00:00Z'
            assert tafs[ix]['taf']['validDateEnd'] == '2022-01-02T18:00:00Z'
        ## jump forward 10 days to test deletion algorithm
        frozen_time.tick(delta=timedelta(days=10))
        with session as test_session:
            run_placeholder_algorithm(test_session)

        # this should generate 12 new tafs
        # so 12 tafs should be deleted & 12 tafs should be added
        # keeping the number of tafs equal to 18
        response = client.get('/taflist',
                              headers={'Geoweb-Username': 'test-user-1'})
        tafs = response.json()
        assert len(tafs) == 18
