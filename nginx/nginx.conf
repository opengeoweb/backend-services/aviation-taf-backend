user nginx;
worker_processes auto;
pcre_jit on;
error_log /var/log/nginx/error.log warn;
include /etc/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    js_path "/etc/nginx";
    js_import oauth2.js;
    include /etc/nginx/mime.types;
    default_type application/octet-stream;
    server_tokens off;
    client_max_body_size 100m;
    keepalive_timeout 20s;
    keepalive_time 20s;
    sendfile on;
    tcp_nodelay on;
    gzip_vary on;
    log_format main '$remote_addr - $remote_user [$time_local] "$request" '
            '$status $body_bytes_sent "$http_referer" '
            '"$http_user_agent" "$http_x_forwarded_for"';

    # General access log settings
    map ${NGINX_ENABLE_ACCESS_LOG} $access_log_enabled {
        default 1;
        FALSE 0;
    }

    access_log /var/log/nginx/access.log main if=$access_log_enabled;
    proxy_cache_path /var/cache/nginx/userinfo keys_zone=userinfo_responses:1m max_size=10m;
    proxy_cache_path /var/cache/nginx/jwks keys_zone=jwks_responses:1m max_size=10m;

    # Map request method and required claim
    map $request_method $valid_claim {
        OPTIONS "FALSE";
        HEAD  ${NGINX_GEOWEB_READ_PERMISSION};
        GET ${NGINX_GEOWEB_READ_PERMISSION};
        POST ${NGINX_GEOWEB_WRITE_PERMISSION};
        PUT ${NGINX_GEOWEB_WRITE_PERMISSION};
        DELETE ${NGINX_GEOWEB_WRITE_PERMISSION};
        PATCH ${NGINX_GEOWEB_WRITE_PERMISSION};
        default "FALSE";
    }

    # # Map endpoint and required claim (example configuration)
    # map $request_uri $valid_claim {
    #     ~^/endpoint1.* ${NGINX_GEOWEB_READ_PERMISSION};
    #     ~^/endpoint2.* ${NGINX_GEOWEB_WRITE_PERMISSION};
    #     default "FALSE";
    # }

    server {
        ${NGINX_ENABLE_SSL_COMMENT}listen ${NGINX_PORT_HTTPS} default_server ssl;
        ${NGINX_ENABLE_SSL_COMMENT}listen [::]:${NGINX_PORT_HTTPS} default_server ssl;
        listen 0.0.0.0:${NGINX_PORT_HTTP} default_server;
        listen [::]:${NGINX_PORT_HTTP} default_server;

        # catch-all server name, handle all requests
        server_name _;

        ${NGINX_ENABLE_SSL_COMMENT}ssl_certificate /cert/fullchain.pem;
        ssl_certificate_key /cert/privkey.pem;
        ssl_protocols TLSv1.3;
        ssl_session_cache shared:SSL:10m;
        ssl_session_timeout 10m;
        ssl_prefer_server_ciphers off;
        ssl_dhparam /cert/dhparam.pem;

        subrequest_output_buffer_size 512k;
        proxy_buffer_size 512k;
        proxy_buffers 4 512k;
        proxy_busy_buffers_size 512k;

        error_page 401 /error_401;
        error_page 403 /error_403;

        location = /error_401 {
            internal;
            add_header Access-Control-Allow-Origin $http_origin always;
            default_type application/json;
            return 401 '{"message":"Unauthorized: Authentication failed. Please verify your credentials."}';
        }
        location = /error_403 {
            internal;
            add_header Access-Control-Allow-Origin $http_origin always;
            default_type application/json;
            return 403 '{"message":"Forbidden: Your credentials do not allow you to perform this action."}';
        }

        # Root
        location / {
            proxy_pass http://${BACKEND_HOST}/;
        }

        # TAFLIST
        location /taflist {
            set $require_claim ${valid_claim};
            include /etc/nginx/auth.conf;

            if ($request_method = OPTIONS) {
                add_header Content-Length 0;
                add_header Content-Type text/plain;
                add_header Access-Control-Allow-Methods "GET, OPTIONS";
                add_header Access-Control-Allow-Origin "*";
                add_header Access-Control-Allow-Headers "Authorization, Content-Type, Location";
                add_header Access-Control-Allow-Credentials true;
                return 200;
            }

            proxy_pass http://${BACKEND_HOST}/taflist;
        }

        # TAF
        location /taf {
            set $require_claim ${valid_claim};
            include /etc/nginx/auth.conf;

            if ($request_method = OPTIONS) {
                add_header Content-Length 0;
                add_header Content-Type text/plain;
                add_header Access-Control-Allow-Methods "GET, POST, PATCH, OPTIONS";
                add_header Access-Control-Allow-Origin "*";
                add_header Access-Control-Allow-Headers "Authorization, Content-Type, Location";
                add_header Access-Control-Allow-Credentials true;
                return 200;
            }

            proxy_pass http://${BACKEND_HOST}/taf;
        }

        # TAF2TAC
        location /taf2tac {
            set $require_claim ${valid_claim};
            include /etc/nginx/auth.conf;

            if ($request_method = OPTIONS) {
                add_header Content-Length 0;
                add_header Content-Type text/plain;
                add_header Access-Control-Allow-Methods "POST, OPTIONS";
                add_header Access-Control-Allow-Origin "*";
                add_header Access-Control-Allow-Headers "Authorization, Content-Type, Location";
                add_header Access-Control-Allow-Credentials true;
                return 200;
            }

            proxy_pass http://${BACKEND_HOST}/taf2tac;
        }

        location = /_auth_token_validate {
            internal;
            proxy_ssl_session_reuse off;
            proxy_ssl_server_name on;
            proxy_ssl_protocols  TLSv1.2 TLSv1.3;
            resolver ${DNS_SERVER_IP} valid=30s ipv6=off;
            js_content oauth2.authorize;
        }

        location = /_auth_send_userinfo_request {
            internal;
            resolver ${DNS_SERVER_IP} valid=30s ipv6=off;
            set $NGINX_OAUTH2_USERINFO "${OAUTH2_USERINFO}";

            proxy_ssl_session_reuse off;
            proxy_ssl_protocols     TLSv1.2 TLSv1.3;
            proxy_ssl_server_name   on;
            proxy_method            GET;
            proxy_set_header        Content-Type "application/x-www-form-urlencoded";
            proxy_set_header        "Accept-Encoding" "";
            proxy_set_header        Accept "application/json";
            proxy_set_header        "Content-Length" "0";
            proxy_pass              $NGINX_OAUTH2_USERINFO;

            proxy_cache             userinfo_responses;
            proxy_cache_key         "$scheme$request_method$host$request_uri$http_authorization";
            proxy_cache_valid       200 10m;
            proxy_cache_lock        on;
            proxy_ignore_headers    Cache-Control Expires Set-Cookie;
        }

        # Healthcheck for NGINX
        location /health_check {
            add_header 'Content-Type' 'application/json';
            return 200 '{"status":"OK", "service":"NGINX"}';
        }

        location @handle_redirect {
            set $saved_redirect_location '$upstream_http_location';
            proxy_pass $saved_redirect_location;
        }
    }
}
