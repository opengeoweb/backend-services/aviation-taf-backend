FROM python:3.11-slim-bookworm AS base

ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache \
    POETRY_VIRTUALENVS_CREATE=1

# Upgrade pip
RUN pip install --no-cache-dir --upgrade pip  \
    && pip install --no-cache-dir poetry==1.8.3

ENV PATH="./.venv/bin:$PATH"

# Create non-root worker
RUN useradd --create-home worker

WORKDIR /app

# Install dependencies
ADD pyproject.toml ./
ADD poetry.lock ./
RUN poetry install --without dev --no-root

# Copy code
ADD . .

USER worker

CMD python tafplaceholder.py
