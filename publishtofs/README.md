# Publish to FS

This is a tiny webserver which listens to post requests from a publishing service and writes its contents to a filesystem.

NOTE: For development purposes ONLY!!!

## To start with python

```
source env/bin/activate
mkdir localpublishdir
export PUBLISH_DIR=./localpublishdir
./startpublishtofs.sh
```

## To start with Docker

```
docker build -t publishtofs .
mkdir localpublishdir
docker run -it -p 8080:8090  -v ./localpublishdir:/publishdir --name publishtofs publishtofs


```

## To POST files

```
curl -X POST --location 'http://127.0.0.1:8090/publish' -d '{"data":"Hello-World"}' -H 'Content-Type: application/json'
```

## List the files:

```
ls publishtofs/localpublishdir
```
