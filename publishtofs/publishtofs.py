# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""
Publishtofs is a (tiny) webservice which can be used to listen to POST requests
    from other services and then write files to a filesystem (fs).

This publishtofs service is used to connect a filesystem with the TAF backend.
When a TAF is published, a TAC and IWXXM are posted to the publishtofs service
    and then written to a directory.

This service is needed, because the TAF backend is a cloud agnostic microservice.
Therefore the TAF backend does not write to a filesystem or bucket,
     but posts its contents into a webservice.
"""

import os

from fastapi import FastAPI
from pydantic import BaseModel

publishtofs = FastAPI()


class Item(BaseModel):
    """Item"""
    data: str


def save_file_to_fs(fs_location: str, filename: str, data: str):
    """Writes a file to given fs_location"""
    file_to_write = os.path.join(fs_location, filename)
    with open(file_to_write, 'w', encoding='UTF-8') as out:
        out.write(data)
    print(f"[OK] Written \"{file_to_write}\" of [{len(data)}] characters")
    print(data)


@publishtofs.post('/publish/{filename}')
async def main(item: Item, filename: str):
    """Publisher"""
    if (publish_dir := os.getenv('PUBLISH_DIR')):
        save_file_to_fs(publish_dir, filename, item.data)
    else:
        print("[ERROR] PUBLISH_DIR not set")

    return item


@publishtofs.get('/')
async def welcome():
    """Publisher"""
    return "Welcome to publish to fs"


@publishtofs.get('/healthcheck')
async def get_healthcheck():
    """Returns a health check"""
    return {'status': 'OK', 'service': 'publisher'}
