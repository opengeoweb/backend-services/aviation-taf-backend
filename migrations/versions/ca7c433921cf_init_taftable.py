# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""init taftable

Revision ID: ca7c433921cf
Revises:
Create Date: 2023-01-24 16:51:27.028842

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'ca7c433921cf'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'taftable', sa.Column('taf_id', sa.String(length=50), nullable=False),
        sa.Column('creationDate', sa.TIMESTAMP(timezone=False), nullable=False),
        sa.Column('baseTime', sa.TIMESTAMP(timezone=False), nullable=False),
        sa.Column('editor', sa.String(length=50), nullable=True),
        sa.Column('taf', sa.JSON(), nullable=False),
        sa.PrimaryKeyConstraint('taf_id'))


def downgrade() -> None:
    op.drop_table('taftable')
