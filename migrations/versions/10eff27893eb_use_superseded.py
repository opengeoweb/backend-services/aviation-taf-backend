# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""Use superseded

Revision ID: 10eff27893eb
Revises: ca7c433921cf
Create Date: 2023-04-04 15:35:12.649599

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '10eff27893eb'
down_revision = 'ca7c433921cf'
branch_labels = None
depends_on = None


def upgrade() -> None:
    """Add the superseded boolean column"""
    op.add_column('taftable',
                  sa.Column('superseded', sa.Boolean(), nullable=True))


def downgrade() -> None:
    """Delete the superseded boolean column"""
    op.drop_column('taftable', 'superseded')
