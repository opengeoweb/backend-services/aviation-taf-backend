# Aviation TAF Backend

This repository contains code to set up an aviation TAF backend as microservice.

## Table of Contents

[[_TOC_]]

# Backend description

This repository contains the code to set up, develop and deploy the TAF backend: designed to support the saving and publishing of TAF products. Depending on the exact configuration, TAFs should be published within a configured timeslot (every .. hours) and are valid for a configured amount of time. The locations and time settings can be configured in [`config.ini`](/config.ini). The [tafplaceholder algorithm](/tafplaceholder.py) runs in the background and checks that placeholder TAFs are generated for upcoming timeslots. These placeholder TAFs, along with drafted and published TAFs are stored in a TAF database. During start-up of the service, an [`admin`](/bin/admin.sh) runs (in Docker this will be done in a separate container) to set up the database correctly.


When a TAF is published, the corresponding TAC message is generated. This is done with the help of the [`avi messageconverter`](https://gitlab.com/opengeoweb/avi-msgconverter/geoweb-knmi-avi-messageservices). After generating the TAC, the TAF is published with the help of a publisher service. For **local development only**, the publishing service can be found in the directory [`pubslishtofs`](/publishtofs/). The published files are stored in the directory [`tafpublishdir`](/tafpublishdir/). For deployment purposes, a separate publishing service should be created that will publish files to the desired location. These publishers can be retrieved from the [publisher-backend repository](https://gitlab.com/opengeoweb/backend-services/publisher-backend).

A quick overview backend and repository structure:

- In the file [`config.ini`](/config.ini) the configuration for different airports can be stored;
- In the directory [`/app`](/app) the internal logic of the backend can be found. The application is set up with FastAPI;
    - In the directory [`/app/routers`](/app/routers/), files can be found that describe the endpoints of the backend;
    - In the file [`models.py`](/app/models.py) a set of Pydantic models are described for interaction with the endpoints;
    - In the directory [`/app/crud`](app/crud/), database interactions are described;
- The directory [`/nginx`](/nginx/) contains the Nginx reverse proxy configuration file used with auth-backend Nginx code for load balancing and authentication;
- The file [`tafplaceholder.py`](/tafplaceholder.py) contains code to automatically generate placeholder TAFs (and removes old TAFs);
- In the directory [`migrations`](/migrations/), the database schema and database migrations can be found. More information about database migrations can be found in this [README](/migrations/README);
- The directory [`publishtofs`](/publishtofs/) contains the files to set up a publishing service for **local development purposes only**. Files are published into the [`tafpublishdir`](/tafpublishdir/).

Below, you can see a schematic overview of the different components of the backend:

```mermaid
flowchart TD

id1{{Frontend application}}:::setstroke<-->|Requests from and reponses to FE|id2[NGINX]
id2:::beclass<-->|Validated response to FE. \n\n Authenticated and authorized request \n to the main Fastapi BE container.|id3[TAF FastAPI BE]
id3:::beclass<-->|Retrieve and update TAF data|id4[(TAF Database)]:::beclass
id3-->|TAC and IWXXM products \n for publishing|id5[TAF Publisher]
id3<-->|Generate TAC and \n IWXXM products|id6[AVI Messageconverter]:::beclass
id7[TAF Placeholder]:::beclass-->|Generate placeholder TAFs \n for upcoming timeslots and \n clean up older TAFs|id4
id5:::beclass-->|Save output products \n in configured location|id8[/File system/]:::setstroke

classDef setstroke stroke-width:3px

classDef beclass fill:#bbe3a4,stroke:#597549,stroke-width:3px

```

Some additional information:
- If you start the application with Docker (see the section [Local installation for testing & development](#local-installation-for-testing--development)), Nginx will be set up. Otherwise, you will directly access the TAF BE container;
- The TAF BE container interacts with the database, the AVI message converter and the publish service;
- The TAF placholder & cleanup algorithm runs with a timer and only interacts with the database.

## TAF workflow

To help with developing and testing, it helps to have some knowledge about the TAF workflow:
- The [tafplaceholder.py algorithm](/tafplaceholder.py) checks if TAFs for the current and upcoming timeslots are available in the database - if not, these will be generated. It also checks if old TAFs (older than 10 days) are present in the database - if so, they are removed to keep the database size manageable;
- When a TAF is edited, it should first be "locked". An editor is assigned to the TAF with a PATCH command. After a TAF is locked, only the user who is set as editor can make changes to the TAF (with POST). The editor can be overwritten with a PATCH command, changing the name of the editor. A TAF can be "unlocked" (editor removed) with another PATCH command. This is done automatically after publishing a TAF;
- In the database, each TAF has a UUID. You can only work with TAFs which have a valid UUID (which thus is linked to an existing TAF in the database). If you want to test the backend, you should first retrieve the TAFs in the database (with a /taflist call, see [API Endpoints](#api-endpoints)) to retrieve valid data to test with.



# Formatting and licensing
## Formatting

This project follows the [Google Python Style Guide](https://github.com/google/styleguide/blob/gh-pages/pyguide.md). The [YAPF](https://github.com/google/yapf) tool can be used to enforce this. If you installed all dependencies using `pip install -r requirements.txt -r requirements-dev.txt`, you should be able to run YAPF by executing `yapf -ir app/`. This will format all Python files in [app](app), using the configuration specified in [.style.yapf](.style.yapf).

## Organizing imports

This project uses [isort](https://pycqa.github.io/isort/) for organizing import statements. This is a tool to sort imports alphabetically, and automatically separates them into sections and by type. To sort imports for the project run `isort app/`.

## IDE support

If you are using Visual Studio Code, consider updating [.vscode/settings.json](.vscode/settings.json) with the contents below. This will format code and organize imports on save.

``` bash
{
    "editor.codeActionsOnSave": {
        "source.organizeImports": true
    },
    "python.formatting.provider": "yapf",
    "[python]": {
        "editor.formatOnSave": true
    }
}
```

## Licence

This project is licensed under the terms of the Apache 2.0 licence.
Every file in this project has a header that specifies the licence and copyright. It is possible to check/add/remove the licence header using the following commands:

`npm run licence:check` => checks all the files for having the header text as specified in the LICENCE file. The format and which files to include is specified in `licence.config.json`.

`npm run licence:add` => adds licence to files without it. This action can require a manual check when the file contains a wrong header (i.g. a simple word is missed), because it just adds another header without removing the wrong one.

`npm run licence:remove` => removes licence from all files. This action can require a manual check when the file contains a wrong header (i.g. a simple word is missed), because it only removes a correct header and not a wrong one.


# Local installation for testing & development

The aviation-taf-backend can be run locally in three ways for different purposes:

1. For development - [Development: Local install with Uvicorn](#development-local-install-with-uvicorn)
2. For testing - [Testing: Docker compose](#testing-docker-compose)
3. For testing in a local FE branch - [Testing BE in local FE branch with GitLab or Cognito](#testing-be-in-local-fe-branch-with-gitlab-or-cognito)

## Development: Local install with Uvicorn

The application can be started after a local installation with Uvicorn, using the `--reload` flag.
This is useful for development as the aviation-taf-backend service is automatically refreshed when code changes are detected.
In order to make it work a Postgres database is needed on the local installation.
Below are the instructions to start the code with Uvicorn and a local Postgres database.

_All commands should be run from the root folder._

## Platform specific instructions on how to run the BE locally

<details>
<summary>Windows</summary>
To be filled..
</details>

<details>
<summary>Linux</summary>

### 1. Install Pyenv with Pyenv installer

Check the [prerequisites](https://github.com/pyenv/pyenv/wiki#suggested-build-environment) from the pyenv wiki. for Debian Linux they are:

```bash
sudo apt update; sudo apt install build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
```

Run the Pyenv installer script:

```bash
curl https://pyenv.run | bash
```

Setup your shell environment with these [instructions](https://github.com/pyenv/pyenv#set-up-your-shell-environment-for-pyenv)

### 2. Build and install Python

With Pyenv installed, check the available Python versions with

```bash
pyenv install -l
```

Install the latest Python 3.11 version from the list and set it as global Python version for your system.

```bash
pyenv install 3.11.8
pyenv global 3.11.8
```

### 3. Install Pipx

Pipx is a helper utility for installing system wide Python packages, adding them to your path automatically.

Install pipx with these [instructions](https://pipx.pypa.io/latest/installation/). Linux users can install it with pip:

```bash
python3 -m pip install --user pipx
python3 -m pipx ensurepath

# Optional
sudo pipx ensurepath --global
```

### 4. Install Poetry

Poetry is used for managing project dependencies and metadata. Install Poetry with

```bash
pipx install poetry
```

and add poetry export plugin to it

```bash
pipx inject poetry poetry-plugin-export
```

### 5. Activate virtualenv and install dependencies

Activate the virtualenv with poetry

```bash
poetry shell
```

And install project dependencies

```bash
poetry install
```

### 2. Setup Postgres database

``` bash
### Setup postgres database for user geoweb and password geoweb, databasename aviationtafbackenddb ###
sudo -u postgres createuser --superuser geoweb
sudo -u postgres psql postgres -c "ALTER USER geoweb PASSWORD 'geoweb';"
sudo -u postgres psql postgres -c "CREATE DATABASE aviationtafbackenddb;"
echo "\q" | psql "dbname=aviationtafbackenddb user=geoweb password=geoweb host=localhost"
if [ ${?} -eq 0 ];then
    echo "Postgres database setup correctly"
    echo "Use the following setting for the aviation TAF backennd:"
    echo "export AVIATION_TAF_BACKEND_DB=\"dbname=aviationtafbackenddb user=geoweb password=geoweb host=localhost\""
else
    echo "Postgres database does not work, please check"
fi
```

Or, alternatively, run a Dockerized Postgres database (while running Uvicorn locally):

``` bash
docker run --rm \
    --name aviation-taf-backend-db \
    -e POSTGRES_USER=geoweb \
    -e POSTGRES_PASSWORD=geoweb \
    -e POSTGRES_DB=aviationtafbackenddb \
    -p 5432:5432 \
    postgres:13.4
```

### 3. Setup geoweb-knmi-avi-messageservices container:

``` bash
export AVI_VERSION="v0.11.1"
docker run -d -p 8081:8080 --name geoweb-knmi-avi-messageservices-local registry.gitlab.com/opengeoweb/avi-msgconverter/geoweb-knmi-avi-messageservices:${AVI_VERSION}
export GEOWEB_KNMI_AVI_MESSAGESERVICES_HOST="localhost:8081"

# Should return greetings:
curl -l http://${GEOWEB_KNMI_AVI_MESSAGESERVICES_HOST}
```

### 4. Setup publish service

``` bash
docker build -t publishtofs ./publishtofs
mkdir -p tafpublishdir
chmod 777 tafpublishdir
docker rm -f publishtofs
docker run -d -it -p 8090:8080  -v ./tafpublishdir:/publishdir --name publishtofs publishtofs
export AVIATION_TAF_PUBLISH_HOST="localhost:8090"
sleep 5
curl -X POST --location http://${AVIATION_TAF_PUBLISH_HOST}/publish/hello.txt -d '{"data":"Hello-World"}' -H 'Content-Type: application/json'
cat ./tafpublishdir/hello.txt
echo
```

### 5. Setup tafplaceholder container - the database should be set up **before** executing this step:

``` bash
docker build -t aviation-taf-placeholder -f tafplaceholder.Dockerfile .
export AVIATION_TAF_BACKEND_DB="postgresql://geoweb:geoweb@localhost:5432/aviationtafbackenddb"
docker run --rm  -e "AVIATION_TAF_BACKEND_DB=${AVIATION_TAF_BACKEND_DB}" --network="host" --name aviation-taf-backend-placeholder-local aviation-taf-placeholder
```

If you have **not** yet set up the database and you get an error trying to execute the code above, try first running:
``` bash
python cli.py enable-alembic
alembic upgrade head
```

You can check that the taftable is set up correctly with:
``` bash
psql "${AVIATION_TAF_BACKEND_DB}"
# make sure you are connected to the aviationtafbackenddb
# you can now check that the taftable exists
select * from taftable ;
```

You can check a correct setup in the logs with:
``` bash
docker logs -f aviation-taf-backend-placeholder-local
```

Normally the container will stop by itself. If you need to keep it running, you can set the enironment variable `TAFPLACEHOLDER_KEEPRUNNING` to "true". In that case it will continuousely update by itself.

### 6. Start the application using:

``` bash
source env/bin/activate
export AVIATION_TAF_BACKEND_DB="postgresql://geoweb:geoweb@localhost:5432/aviationtafbackenddb"
export GEOWEB_KNMI_AVI_MESSAGESERVICES_HOST="localhost:8081"
export GEOWEB_USERNAME="maarten.plieger" # Setting the user header t your id.
export AVIATION_TAF_PUBLISH_HOST="localhost:8090"
export VERSION=1.0.0
export AVI_VERSION=v0.11.1
python cli.py enable-alembic
alembic upgrade head
python cli.py seed
bin/start-dev.sh
```

### 7. Access the running application

Visit http://127.0.0.1:8080/ or http://127.0.0.1:8080/taf

You can use curl commands to communicate with the FastAPI BE. Below, some examples are mentioned. In the section [API Endpoints](#api-endpoints) you can find more information about the available endpoints.
</details>

<details>
<summary>Mac</summary>



### 1. For installing dependencies, you can use [Homebrew](https://brew.sh) package manager

##### 1.1 Install Python 3.11
``` bash
brew install python@3.11
```

##### 1.2 Install PostgreSQL
``` bash
brew install postgresql
```

##### 1.3 To have launchd start postgresql at login:
``` bash
brew services start postgresql
```
##### 1.4 Create env folder where dependencies will be installed into, virtual env and activate it

``` bash
python3.11 -m venv env
source env/bin/activate
```

##### 1.5 Use pip to install the python dependencies
``` bash
pip3 install -r requirements-dev.txt
```

### 2. For setting up the database, you can use the following script.

##### 2.1 Check if the geoweb user exists, and if not, create it
``` bash
if ! psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='geoweb'" | grep -q 1; then
  createuser --superuser geoweb
  echo "User 'geoweb' created."
else
  echo "User 'geoweb' already exists."
fi
```

##### 2.2 Set the password for the geoweb user
``` bash
psql postgres -c "ALTER USER geoweb PASSWORD 'geoweb';"
```

##### 2.3 Check if the aviationtafbackenddb database exists, and if not, create it
``` bash
if ! psql -lqt | cut -d \| -f 1 | grep -qw aviationtafbackenddb; then
  createdb -O geoweb aviationtafbackenddb
  echo "Database 'aviationtafbackenddb' created."
else
  echo "Database 'aviationtafbackenddb' already exists."
fi
```

##### 2.4 Test connection to the database

``` bash
echo "\q" | PGPASSWORD=geoweb psql -h localhost -U geoweb -d aviationtafbackenddb
if [ $? -eq 0 ]; then
    echo "Postgres database setup correctly"
    echo "Use the following setting for the aviation TAF backend:"
    echo "export AVIATION_TAF_BACKEND_DB=\"dbname=aviationtafbackenddb user=geoweb password=geoweb host=localhost\""
else
    echo "Postgres database does not work, please check"
fi
```

### 3. Setup geoweb-knmi-avi-messageservices container:
``` bash
export AVI_VERSION="v0.11.1"
docker run -d -p 8081:8080 --name geoweb-knmi-avi-messageservices-local registry.gitlab.com/opengeoweb/avi-msgconverter/geoweb-knmi-avi-messageservices:${AVI_VERSION}
export GEOWEB_KNMI_AVI_MESSAGESERVICES_HOST="localhost:8081"
```

#####  3.1 Test that the container gives a response

``` bash
curl -l http://${GEOWEB_KNMI_AVI_MESSAGESERVICES_HOST}
```

### 4. Setup publish service

``` bash
docker build -t publishtofs ./publishtofs
mkdir -p tafpublishdir
chmod 777 tafpublishdir
docker rm -f publishtofs
docker run -d -it -p 8090:8080  -v ./tafpublishdir:/publishdir --name publishtofs publishtofs
export AVIATION_TAF_PUBLISH_HOST="localhost:8090"
sleep 5
curl -X POST --location http://${AVIATION_TAF_PUBLISH_HOST}/publish/hello.txt -d '{"data":"Hello-World"}' -H 'Content-Type: application/json'
cat ./tafpublishdir/hello.txt
echo
```

### 5. Setup taf placeholder container

##### 5.1 Build the docker image

``` bash
docker build -t aviation-taf-placeholder -f tafplaceholder.Dockerfile .
```

##### 5.2 Configure Database Connection for Direct Host Access

``` bash
export AVIATION_TAF_BACKEND_DB="postgresql://geoweb:geoweb@localhost:5432/aviationtafbackenddb"

```

##### 5.3 Initialize and Upgrade Database Schema with Alembic

``` bash
python3.11 cli.py enable-alembic
alembic upgrade head
```

##### 5.4 Verify Database Setup

``` bash
psql "${AVIATION_TAF_BACKEND_DB}"
select * from taftable;
```

##### 5.5 Exit the database

``` bash
\q
```

##### 5.6 Run the TAF placeholder container using host.docker.internal

``` bash
docker run --rm -e "AVIATION_TAF_BACKEND_DB=postgresql://geoweb:geoweb@host.docker.internal:5432/aviationtafbackenddb" --name aviation-taf-backend-placeholder-local aviation-taf-placeholder
```


### 6. Start the application using:

``` bash
source env/bin/activate
export AVIATION_TAF_BACKEND_DB="postgresql://geoweb:geoweb@localhost:5432/aviationtafbackenddb"
export GEOWEB_KNMI_AVI_MESSAGESERVICES_HOST="localhost:8081"
export GEOWEB_USERNAME="firstName.lastname@gmail.com"
export AVIATION_TAF_PUBLISH_HOST="localhost:8090"
export VERSION=1.0.0
export AVI_VERSION=0.1.3
python3.11 cli.py enable-alembic
alembic upgrade head
python3.11 cli.py seed
bin/start-dev.sh
```
</details>



### Get the taflist
``` bash
curl -kL http://127.0.0.1:8080/taflist -H "geoweb-username: user"
```
### Publish a TAF

Make sure to (temporarily) set the `"editor"` in the file `app/tests/testdata/draft.json` equal to the username set in the header "geoweb-username". Also update the "uuid" to a valid UUID (see the taflist results above).
``` bash
curl -X PATCH --location 'http://127.0.0.1:8080/taf' -H "geoweb-username: user" -H 'Content-Type: application/json'  -d @app/tests/testdata/draft.json
curl -X POST --location 'http://127.0.0.1:8080/taf' -H "geoweb-username: user" -H 'Content-Type: application/json'  -d @app/tests/testdata/draft.json
curl -X POST --location 'http://127.0.0.1:8080/taf' -H "geoweb-username: user" -H 'Content-Type: application/json' -d @app/tests/testdata/tobepublished.json

```

Published tafs should show in the `tafpublishdir` folder.

### Convert tafjson to ASCII TAC
``` bash
curl -k -X POST --location 'http://127.0.0.1:8080/taf2tac' -H 'Content-Type: application/json' -d @app/tests/testdata/draft_taf_fortaf2tac.json
```
### Convert tafjson to JSON TAC with adverse weather properties
``` bash
curl -k -X POST --location 'http://127.0.0.1:8080/taf2tac' -H 'Content-Type: application/json' -H 'Accept: application/json' -d @app/tests/testdata/draft_taf_fortaf2tac.json | python -m json.tool
```

### Access the database
You can browse the database using

``` bash
psql "${AVIATION_TAF_BACKEND_DB}"
select * from taftable ;
```

### Clean up
If you want to reset/clean the database, you can drop the `taftable` table using:

``` bash
psql "${AVIATION_TAF_BACKEND_DB}" -c 'DROP TABLE IF EXISTS taftable;'
```

And cleanup the Alembic table as well:

``` bash
psql "${AVIATION_TAF_BACKEND_DB}" -c 'DROP TABLE IF EXISTS alembic_version;'
```

Clean up the tafplaceholder algorithm and avimessageservice with:
``` bash
docker container stop aviation-taf-backend-placeholder-local geoweb-knmi-avi-messageservices-local
```

When the application is restarted, it will be initialized with fresh data.

### Managing Python dependencies

With Poetry installed from previous step, you can add or update Python dependencies with poetry add command.

```bash
# adding dependency
poetry add fastapi

# updating dependency
poetry update fastapi
```

You can add development dependencies with --group keyword.

```bash
# adding development dependency
poetry add --group dev isort
```

All poetry operations respect version constraints set in pyproject.toml file.

##### Fully updating dependencies

You can see a list of outdated dependencies by running

```bash
poetry show --outdated
```

To update all dependencies, still respecting the version constraints on pyproject.toml, run

```bash
poetry update
```

### Dependabot integration

This repository has a Dependabot integration with the following behavior specified in the `.gitlab/dependabot.yml` file:

| definition               | behavior                                                                                                                                                                                                                                                                   |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| schedule block           | Scan the repository daily at random time between 6-11. Add any pending package update as MR and update any MR with merge conflicts by doing a rebase. If there is a newer release of the dependency that already has an open MR, make a new MR and close the previous one. |
| open-pull-requests-limit | Don't open more than 3 MRs per directory, even if there are more package updates available than 3.                                                                                                                                                                         |
| rebase-strategy          | Always rebase open MRs that have dependabot as assignee, if there are merge conflicts present.                                                                                                                                                                             |
| assignee                 | add dependabot as a assignee for every MR it opens.                                                                                                                                                                                                                        |

More detailed guide on how to work with dependabot can be found in the [opengeoweb wiki](https://gitlab.com/groups/opengeoweb/-/wikis/How-to-work-with-Dependabot-MRs)

### Using pre-commit

You can also use the pre-commit configuration. This is used to inspect the code that's about to be committed, to see if you've forgotten something, to make sure tests run, or to examine whatever you need to inspect in the code. To use pre-commit make sure the `pip install` command has succesfully ran with the `requirements-dev.txt` argument.
Then execute the following command in the terminal in the root folder:
 ```
 pre-commit install
 ```
 This will install the pre-commit functions and now pre-commit will run automatically on each `git commit`.
 Only changed files are taken into consideration.

 Optionally you can check all the files with the pre-commit checks (NOTE: this command can make changes to files):

```
pre-commit run --all-files
```

If you wish to skip the pre-commit test but do have pre-commit setup, you can use the `--no-verify` flag in git to skip the pre-commit checks. For example:
```
git commit -m "Message" --no-verify
```

## Testing: Docker compose

The application can be started in a Docker container.
This will create a static version of the aviation-taf-backend which is not automatically refreshed after saving changes in the code.
After starting the application with Docker and GitLab or Cognito, it can be integrated in the [FE for testing](#testing-be-in-local-fe-branch-with-gitlab-or-cognito).

The [docker-compose.yml](docker-compose.yml) file starts Nginx, Postgres and the aviation-taf-backend. The [docker-compose.yml](docker-compose.yml) file wires everthing together using a network. The nginx acts like a proxy or load balancer, it is the gatekeeper in front of the application. The postgres database stores the TAFs. The aviation-taf-backend is the python application which makes the aviation TAF backend. Some settings need to be defined, like on which port and what url the application should be run. By default, a self signed certificate is generated, which is not by default trusted by the browser.

### Starting up

Before starting to build the container, make sure that docker is installed and running on your machine.

To start the application with Docker Compose, set up the environment, build the Docker image and start the infrastructure.

_All commands should be run from the root folder._

Remember to set AVIATION_TAF_ENABLE_SSL to FALSE if you are deploying behind a load balancer. Otherwise, SSL is required for a local deployment.

1. First, the `.env` file should be created. If GitLab.com is used as an identity provider:

``` bash
cat <<EOF > .env
ENABLE_SSL=TRUE
ENABLE_DEBUG_LOG=TRUE
BACKEND_PORT=4443
BACKEND_PORT_HTTP=80
OAUTH2_USERINFO=https://gitlab.com/oauth/userinfo
GEOWEB_USERNAME_CLAIM="email"
# GEOWEB_REQUIRE_READ_PERMISSION="groups=opengeoweb/internal"
# GEOWEB_REQUIRE_WRITE_PERMISSION="groups=opengeoweb/internal"
VERSION=1.0.0
AVI_VERSION=v0.11.1
EOF
```

In case KNMI's Cognito is used as an identity provider:

``` bash
cat <<EOF > .env
ENABLE_SSL=TRUE
ENABLE_DEBUG_LOG=TRUE
BACKEND_PORT=4443
BACKEND_PORT_HTTP=80
OAUTH2_USERINFO=https://knmi-geoweb-tst.auth.eu-west-1.amazoncognito.com/oauth2/userInfo
GEOWEB_USERNAME_CLAIM='username'
# GEOWEB_REQUIRE_READ_PERMISSION="cognito:groups=TafRead"
# GEOWEB_REQUIRE_WRITE_PERMISSION='username=some_user'
# JWKS_URI=<add_jwks_uri_here>
# AUD_CLAIM="client_id"
# AUD_CLAIM_VALUE=<add_audience_here>
# ISS_CLAIM="iss"
# ISS_CLAIM_VALUE=<add_issuer_here>
VERSION=1.0.0
AVI_VERSION=v0.11.1
EOF
```

2. Setup output directory

Here the published products will show up. They are written via the publishtofs container, which listens to POST requests from the taf backend.
``` bash
mkdir -p tafpublishdir
chmod 777 tafpublishdir
```

3.  After correctly setting `.env`, the container can be built with Docker Compose.

If you work with a `Docker Compose` version 1.x (check `docker-compose -v` or `docker-compose version`), use:

``` bash
docker-compose up -d --build
```

If you have `Compose V2` installed (check `docker compose -v` or `docker compose version`), you should be able to run:

``` bash
docker compose up -d --build
```

according to the [docs](https://docs.docker.com/compose/).

4. After succesfully composing the containers, logging can be viewed via:

`docker logs -f aviation-taf-backend`

The following endpoints should now be available (for more information about the endpoints see [API endpoints](#api-endpoints)):

- `curl -kL https://0.0.0.0:4443/`
- `curl -kL https://0.0.0.0:4443/healthcheck` (for the BE itself)
- `curl -kL https://0.0.0.0:4443/health_check` (for NGINX)
- `curl -kL https://0.0.0.0:4443/version`
- `curl -kL https://0.0.0.0:4443/aviversion`

For Mac users, replace `https://0.0.0.0:4443/` with `https://localhost:4443`:

- `curl -kL https://localhost:4443/`
- `curl -kL https://localhost:4443/healthcheck` (for the BE itself)
- `curl -kL https://localhost:4443/health_check` (for NGINX)
- `curl -kL https://localhost:4443/version`
- `curl -kL https://localhost:4443/aviversion`


### Publish a TAF
To post a taf to the taf backend:

- Temporarily change the `"editor"` in the draft.json to match the one on your token. Also update the UUID to a UUID present in the database.
- Obtain your token from the network explorer in GeoWeb
- Set your token: `export token=<Bearer Token>`
- `curl -k -X PATCH --location 'https://0.0.0.0:4443/taf' --header "Authorization: ${token}" -H 'Content-Type: application/json'  -d @app/tests/testdata/draft.json`
- `curl -k -X POST --location 'https://0.0.0.0:4443/taf' --header "Authorization: ${token}" -H 'Content-Type: application/json'  -d @app/tests/testdata/draft.json`


### Clean-up
To reset everything, including the database do:

``` bash
docker-compose down --volumes
```

or

``` bash
docker compose down --volumes
```

depending on your `Docker Compose` version.

## Testing BE in local FE branch with GitLab or Cognito

To test backend changes in a local [opengeoweb frontend](https://gitlab.com/opengeoweb/opengeoweb) branch, the aviation-taf-backend can be setup in a Docker and connected to a local frontend branch with the following steps:

- Make sure that you are _not_ connected to a VPN
- Checkout the branch you want to test locally
- Check that docker is installed and running
- Set your `.env` file up with the appropriate [Authentication mechanism](#authentication-mechanism) (depending on what is used in your front-end). Also see [this section](#testing-docker-compose).
   - If you use Cognito, in the `.env` file you should set: `OAUTH2_USERINFO=https://knmi-geoweb-tst.auth.eu-west-1.amazoncognito.com/oauth2/userInfo`
   - If you use Gitlab, you should set `OAUTH2_USERINFO=https://gitlab.com/oauth/userinfo`
- Start the application with Docker, also see [this section](#testing-docker-compose)
- In the web browser that you will use to test the app:
  - Go to address: https://localhost:4443/ or https://0.0.0.0:4443/
  - Accept the certificate (this is not possible on connect). Keep this browser open.
- Copy `"GW_TAF_BASE_URL": "https://0.0.0.0:4443/"` or `"GW_TAF_BASE_URL": "https://localhost:4443/"` into your local config.json in the FE code.
- Connect to your VPN
- Start up your local FE and open the app in the same browser as where you accepted the certificate
- Open the TAF module and get started!

# Authentication mechanism

The authentication is handled in nginx. Nginx checks if an authorization header is set with an OAuth2 bearer token. If it is set, nginx will communicate with the identity provider to get the user information using the OAuth2 token. If an user can be identified, nginx will proceed with forwarding to the aviation-taf-backend container, and it will set a header named `GEOWEB_USERNAME` with the identity. The aviation TAF backend will read this header, it uses this to know who signed in.

## Gitlab

You can authenticate for the aviation-taf-backend by using the following command:

``` bash
curl -k --location 'https://0.0.0.0:4443/taflist' --header 'Authorization: Bearer  <gitlab opaque access token>'
```

The following userinfo endpoint is used:

``` bash
curl --location --request POST 'https://gitlab.com/oauth/userinfo' \
--header 'Authorization: Bearer <gitlab opaque access token>'
```

In this case the OAUTH2_USERINFO=https://gitlab.com/oauth/userinfo must be set for the nginx proxy. This can be set in the .env file in the Docker folder.

## Cognito

Set the OAUTH2_USERINFO to the userinfo endpoint of the cognito userpool you are using. This has the following form: `https://<userpool>.amazoncognito.com/oauth2/userInfo`

You can authenticate by the following command:

``` bash
curl -k --location 'https://0.0.0.0:4443/taf' --header 'Authorization: Bearer  <cognito bearer access token>''
```

To obtain user information:
``` bash
curl --location --request POST 'https://<cognito userpool>.amazoncognito.com/oauth2/userInfo' \
--header 'Authorization: Bearer <cognito bearer access token>'
```

# Running with Docker and without Docker Compose and SSL

Start the Postgres container at port 5432:

`docker run -it -e POSTGRES_USER=geoweb -e POSTGRES_PASSWORD=geoweb -e POSTGRES_DB=presetsbackenddb postgres:13.4`

Start avi-messageconverter:
```
docker run -it -p 8090:8080 \
    registry.gitlab.com/opengeoweb/avi-msgconverter/geoweb-knmi-avi-messageservices:0.1.3
```

Build the publisher:

`docker build -t publisher publishtofs/`

Start the publisher:
```
docker run -it -p 8091:8080 publisher
```

Build the aviation-taf-backend container:

`docker build -t aviation-taf-backend .`

Start the aviation-taf-backend container at port 8085:

```
export AVIATION_TAF_BACKEND_DB="postgresql://geoweb:geoweb@localhost:5432/aviationtafbackenddb"
export GEOWEB_KNMI_AVI_MESSAGESERVICES_HOST="0.0.0.0:8090"
export AVIATION_TAF_PUBLISH_HOST="0.0.0.0:8091"

docker run -it \
    -e "AVIATION_TAF_PORT_HTTP=8085" \
    -e "AVIATION_TAF_BACKEND_DB=${AVIATION_TAF_BACKEND_DB}" \
    -e "GEOWEB_KNMI_AVI_MESSAGESERVICES_HOST=${GEOWEB_KNMI_AVI_MESSAGESERVICES_HOST}" \
    -e "AVIATION_TAF_PUBLISH_HOST=${AVIATION_TAF_PUBLISH_HOST}" \
    --network host \
    aviation-taf-backend
```

Start the Nginx container at port 8081:

```
docker run -it \
    -e "ENABLE_SSL=FALSE" \
    -e "ENABLE_DEBUG_LOG=TRUE" \
    -e "NGINX_PORT_HTTP=8081" \
    -e "BACKEND_HOST=0.0.0.0:8085" \
    -e OAUTH2_USERINFO="https://gitlab.com/oauth/userinfo" \
    -v nginxcertvol:/cert \
    -v "$(pwd)"/nginx/nginx.conf:/nginx.conf \
    --network host \
    registry.gitlab.com/opengeoweb/backend-services/auth-backend/auth-backend:v0.3.0
```

Test it:

`curl -kL http://0.0.0.0:8081/health_check (for NGINX)`
`curl -kL http://0.0.0.0:8081/healthcheck (for the aviation-taf-backend itself)`

# API Endpoints
A general description of the endpoints described below can also be retrieved with the endpoint `/api`.

For some endpoints authentication is necessary. If you use Uvicorn, you can authenticate by including a `Geoweb-Username` header in each request:
- Since there is no authentication linked to Gitlab or Cognito, you can choose any (string) value as username
- With this username, you can lock and unlock TAFs, and post TAFs
- If you do not include the header, you may get an "Unauthorized" response for some endpoints
- You can include the header like this:
``` bash
curl -X GET 'http://0.0.0.0:8080/taflist'  -H 'accept: application/json' -H 'Geoweb-Username: test.username'
```

If you use Docker, you can authenticate by including an `Authorization` header in each request:
- Obtain a token by browsing GeoWeb and look at the network explorer
- Retrieve the token from the Authorization header in the network explorer
- Copy the token from the explorer and enter: `export token=<Bearer Token>`
- Include the authorization header in your requests, for example:
``` bash
`curl -k -X GET --location 'https://0.0.0.0:4443/taflist' --header "Authorization: ${token}" -H 'Content-Type: application/json'`
```
- Please note that the token validity will time out - you can retrieve a fresh token from the GeoWeb application and export the token again


## GET /
Root endpoint

### Response
- `GeoWeb Aviation TAF API` (200)

## GET /healthcheck
Checks the health of the application.

### Response
- `{'status': 'OK', 'service': 'TAF'}` (200)
- `{"message":"Health check is failing"}` (500)

## GET /version
Returns the version of the backend

### Response
- `{"version":"X.X.X"}` (200)

## GET /aviversion
Returns the version of the avi messageconverter

### Response
- `{"version":"X.X.X"}` (200)
## GET /taflist
Returns TAFs for the upcoming, current and previous (expired) timeslots. With Docker, authentication is required for this endpoint.

### Example request
With uvicorn:
``` bash
curl -X GET 'http://0.0.0.0:8080/taflist'  -H 'accept: application/json' -H 'Geoweb-Username: test.username'
```
With docker:
``` bash
curl -X GET 'https://0.0.0.0:4443/taflist'  -H 'accept: application/json' -H 'Authorization: ${token}'
```

### Response
- list of TAFs (200)
``` bash
json
[
  {
    "taf": {
      "baseTime": "2023-07-07T18:00:00Z",
      "location": "EHAM",
      "messageType": "ORG",
      "uuid": "7aa1ee18-1cd0-11ee-bd0a-9cb6d03fb37c",
      "validDateEnd": "2023-07-09T00:00:00Z",
      "validDateStart": "2023-07-07T18:00:00Z",
      "status": "NEW",
      "baseForecast": {
        "valid": {
          "end": "2023-07-09T00:00:00Z",
          "start": "2023-07-07T18:00:00Z"
        }
      }
    },
    "creationDate": "2023-07-07T20:00:00Z",
    "editor": "test.user",
    "canbe": [
      "DRAFTED",
      "DISCARDED",
      "PUBLISHED"
    ]
  },
  {
    "taf": {
      "baseTime": "2023-07-07T18:00:00Z",
      "location": "EHBK",
      "messageType": "ORG",
      "uuid": "7aa4f8c4-1cd0-11ee-bd0a-9cb6d03fb37c",
      "validDateEnd": "2023-07-09T00:00:00Z",
      "validDateStart": "2023-07-07T18:00:00Z",
      "status": "NEW",
      "baseForecast": {
        "valid": {
          "end": "2023-07-09T00:00:00Z",
          "start": "2023-07-07T18:00:00Z"
        }
      }
    },
    "creationDate": "2023-07-07T20:00:00Z",
    "editor": null,
    "canbe": [
      "DRAFTED",
      "DISCARDED",
      "PUBLISHED"
    ]
  }
...
]
```
- HTTPException (400)

## GET /taf/{taf_id}
Returns contents of a TAF based on UUID

### Example request
With uvicorn:
``` bash
curl -X 'GET' \
  'http://0.0.0.0:8080/taf/{taf_id}' \
  -H 'accept: application/json' \
  -H 'Geoweb-Username: example.username'
```
With Docker:
``` bash
curl -X 'GET' \
  'https://0.0.0.0:4443/taf/{taf_id}' \
  -H 'accept: application/json' \
  -H 'Authorization: ${token}'
```

### Response
- TAF body (200)
``` bash
json
{
  "taf": {
    "baseTime": "2023-07-07T18:00:00Z",
    "location": "EHAM",
    "messageType": "ORG",
    "uuid": "7aa1ee18-1cd0-11ee-bd0a-9cb6d03fb37c",
    "validDateEnd": "2023-07-09T00:00:00Z",
    "validDateStart": "2023-07-07T18:00:00Z",
    "status": "NEW",
    "baseForecast": {
      "valid": {
        "end": "2023-07-09T00:00:00Z",
        "start": "2023-07-07T18:00:00Z"
      }
    }
  },
  "creationDate": "2023-07-07T20:00:00Z",
  "editor": null,
  "canbe": [
    "DRAFTED",
    "DISCARDED",
    "PUBLISHED"
  ]
}
```
- HTTPException (400)

## PATCH /taf
Lock or unlock a TAF. By posting a body with the key `"editor": "username"`, a TAF is locked. A TAF can be unlocked by posting a taf without an `editor` key.

If you want to lock a TAF, it is important that the username in `Geoweb-Username`-header is **equal** to the `"editor"` in the posted body. If you authenticate with a token, the `"editor"` should have the same value as the _username_  that is associated with the token.

### Parameters

The request accepts a `force` paramter which can be set to `true` or `false` (default) and can be used to force a take-over of the editor role in case a user wants to take over a TAF that already has another editor.

If the TAF in the database has _no_ editor, a user can be set as editor without the need to set `?force=true` in the request. However, if the TAF in the database _does_ have an editor assigned and another user wants to take over, force should be set to true. If not, the request will return a 409 error code stating that the editor could not be updated.

| Editor in database | Editor in request & header | `?force=` | Response |
| ------------------ | ----------------- | --------- | -------- |
| None               | "user-1"          | `false`   | 200      |
| "user-1"           | "user-2"          | `false`   | 409      |
| "user-1"           | "user-2"          | `true`    | 200      |


### Example request
With Uvicorn:
``` bash
curl -X 'PATCH' 'http://0.0.0.0:8080/taf?force=true' -H 'accept: application/json' \
  -H 'Geoweb-Username: example.user' -H 'Content-Type: application/json' \
  -d '{
  "taf": {
    "baseTime": "2023-07-07T18:00:00Z",
    "location": "EHAM",
    "messageType": "ORG",
    "uuid": "7aa1ee18-1cd0-11ee-bd0a-9cb6d03fb37c",
    "validDateEnd": "2023-07-09T00:00:00Z",
    "validDateStart": "2023-07-07T18:00:00Z",
    "status": "NEW",
    "baseForecast": {
      "valid": {
        "end": "2023-07-09T00:00:00Z",
        "start": "2023-07-07T18:00:00Z"
      }
    }
  },
  "creationDate": "2023-07-07T20:00:00Z",
  "editor": "example.user",
  "canbe": [
    "DRAFTED",
    "DISCARDED",
    "PUBLISHED"
  ]
}'
```

With Docker:
``` bash
curl -X 'PATCH' 'https://0.0.0.0:4443/taf?force=true' -H 'accept: application/json' \
  -H 'Authorization: ${token}' -H 'Content-Type: application/json' \
  -d '{
  "taf": {
    "baseTime": "2023-07-07T18:00:00Z",
    "location": "EHAM",
    "messageType": "ORG",
    "uuid": "7aa1ee18-1cd0-11ee-bd0a-9cb6d03fb37c",
    "validDateEnd": "2023-07-09T00:00:00Z",
    "validDateStart": "2023-07-07T18:00:00Z",
    "status": "NEW",
    "baseForecast": {
      "valid": {
        "end": "2023-07-09T00:00:00Z",
        "start": "2023-07-07T18:00:00Z"
      }
    }
  },
  "creationDate": "2023-07-07T20:00:00Z",
  "editor": "example.user",
  "canbe": [
    "DRAFTED",
    "DISCARDED",
    "PUBLISHED"
  ]
}'
```

### Response
- null (200) is successfull
- HTTPException (409) TAF already has another editor
- HTTPException (400)

## POST /taf
Update the contents of a TAF. The `changeStatusTo` determines the action. Possible values:
- `DRAFT`: save a TAF
- `PUBLISHED`: publish a TAF
- `CANCELLED`: cancels a published TAF (only published TAFs can be cancelled)
- `DRAFT_CORRECTED`: save a TAF that will correct a published TAF (only published TAFs can be corrected)
- `CORRECTED`: correct a published TAF (only publshed TAFs can be corrected)
- `DRAFT_AMENDED`: save a TAF that will amend a published TAF (only published TAFs can be amended)
- `AMENDED`: amend a published TAF (only published TAFs can be corrected)

**Note:** you have to lock or [PATCH](#patch-taf) a TAF **first** before you can edit the status of a TAF.

### Example request
With Uvicorn:
``` bash
curl -X 'POST' 'http://0.0.0.0:8080/taf' -H 'accept: application/json' \
  -H 'Geoweb-Username: test.user' -H 'Content-Type: application/json' \
  -d '{
  "taf": {
    "baseTime": "2023-07-07T18:00:00Z",
    "location": "EHAM",
    "messageType": "ORG",
    "uuid": "7aa1ee18-1cd0-11ee-bd0a-9cb6d03fb37c",
    "validDateEnd": "2023-07-09T00:00:00Z",
    "validDateStart": "2023-07-07T18:00:00Z",
    "status": "NEW",
    "baseForecast": {
      "wind": {
        "direction": 120,
        "speed": 10,
        "unit": "KT"
      },
      "valid": {
        "end": "2023-07-09T00:00:00Z",
        "start": "2023-07-07T18:00:00Z"
      },
      "cavOK": true
    }
  },
  "creationDate": "2023-07-07T20:00:00Z",
  "editor": "test.user",
  "changeStatusTo": "PUBLISHED"
}'
```

With Docker:
``` bash
curl -X 'POST' 'https://0.0.0.0:4443/taf' -H 'accept: application/json' \
  -H 'Authorization: ${token}' -H 'Content-Type: application/json' \
  -d '{
  "taf": {
    "baseTime": "2023-07-07T18:00:00Z",
    "location": "EHAM",
    "messageType": "ORG",
    "uuid": "7aa1ee18-1cd0-11ee-bd0a-9cb6d03fb37c",
    "validDateEnd": "2023-07-09T00:00:00Z",
    "validDateStart": "2023-07-07T18:00:00Z",
    "status": "NEW",
    "baseForecast": {
      "wind": {
        "direction": 120,
        "speed": 10,
        "unit": "KT"
      },
      "valid": {
        "end": "2023-07-09T00:00:00Z",
        "start": "2023-07-07T18:00:00Z"
      },
      "cavOK": true
    }
  },
  "creationDate": "2023-07-07T20:00:00Z",
  "editor": "test.user",
  "changeStatusTo": "PUBLISHED"
}'
```

### Response
- null (200) is successfull
- HTTPException (400)
- `{"WarningMessage": "You are no longer the editor for this TAF"}` (400)

## POST /taf2tac
Generate a TAC from a TAF. Return format can be specified to text or json:
- If you set `-H 'accept: application/json'`, you'll recieve a JSON response
- If you set -H 'accept: application/json'

### Example request
With Uvicorn, receive a JSON format:
``` bash
curl -X 'POST' \
  'http://0.0.0.0:8080/taf2tac' -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "issueDate": "2023-07-07T16:54:46Z",
  "baseTime": "2023-07-07T18:00:00Z",
  "location": "EHAM",
  "messageType": "ORG",
  "uuid": "7aa1ee18-1cd0-11ee-bd0a-9cb6d03fb37c",
  "validDateEnd": "2023-07-09T00:00:00Z",
  "validDateStart": "2023-07-07T18:00:00Z",
  "status": "PUBLISHED",
  "baseForecast": {
    "cavOK": true,
    "valid": {
      "end": "2023-07-09T00:00:00Z",
      "start": "2023-07-07T18:00:00Z"
    },
    "wind": {
      "direction": 120,
      "speed": 10,
      "unit": "KT"
    }
  }
}'
```

With Uvicorn, to receive a string, omit the accept header `-H 'accept: application/json'` :
``` bash
curl -X 'POST' \
  'http://0.0.0.0:8080/taf2tac' \
  -H 'Content-Type: application/json' \
  -d '{
  "issueDate": "2023-07-07T16:54:46Z",
  "baseTime": "2023-07-07T18:00:00Z",
  "location": "EHAM",
  "messageType": "ORG",
  "uuid": "7aa1ee18-1cd0-11ee-bd0a-9cb6d03fb37c",
  "validDateEnd": "2023-07-09T00:00:00Z",
  "validDateStart": "2023-07-07T18:00:00Z",
  "status": "PUBLISHED",
  "baseForecast": {
    "cavOK": true,
    "valid": {
      "end": "2023-07-09T00:00:00Z",
      "start": "2023-07-07T18:00:00Z"
    },
    "wind": {
      "direction": 120,
      "speed": 10,
      "unit": "KT"
    }
  }
}'
```

For Docker, use in a similar way (without authentication):
``` bash
curl -X 'POST' \
  'https://0.0.0.0:4443/taf2tac' -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "issueDate": "2023-07-07T16:54:46Z",
  "baseTime": "2023-07-07T18:00:00Z",
  "location": "EHAM",
  "messageType": "ORG",
  "uuid": "7aa1ee18-1cd0-11ee-bd0a-9cb6d03fb37c",
  "validDateEnd": "2023-07-09T00:00:00Z",
  "validDateStart": "2023-07-07T18:00:00Z",
  "status": "PUBLISHED",
  "baseForecast": {
    "cavOK": true,
    "valid": {
      "end": "2023-07-09T00:00:00Z",
      "start": "2023-07-07T18:00:00Z"
    },
    "wind": {
      "direction": 120,
      "speed": 10,
      "unit": "KT"
    }
  }
}'
```

### Response
- If text/plain: returns a string
``` bash
TAF EHAM 071654Z 0718/0824 12010KT CAVOK=
```
- If application/json: returns a JSON
``` bash
{
  "tac": [
    [
      {
        "text": "TAF"
      },
      {
        "text": "EHAM"
      },
      {
        "text": "071654Z"
      },
      {
        "text": "0718/0824"
      },
      {
        "text": "12010KT"
      },
      {
        "text": "CAVOK"
      },
      {
        "text": "="
      }
    ]
  ],
  "errors": []
}
```
- Error code (400, 422 Unprocessable entity, ...)
