"""CLI commands for project"""
#!/usr/bin/env python
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)

import json
import logging
import logging.config
from datetime import datetime, timezone

import typer
from alembic.command import stamp
from alembic.config import Config
from sqlalchemy import inspect
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.engine import Connection
from sqlalchemy.exc import SQLAlchemyError

from app.db import engine
from app.models import TafDbModel
from app.utils.utils import DATE_FORMAT

logging.config.dictConfig({
    'version': 1,
    'formatters': {
        'json': {
            'class': 'app.logger.JsonFormatter'
        }
    },
    'handlers': {
        'stream': {
            'class': 'logging.StreamHandler',
            'formatter': 'json',
            'stream': 'ext://sys.stderr',
        }
    },
    'root': {
        'handlers': ['stream'],
        'level': logging.INFO,
    },
    'loggers': {
        'sqlalchemy': {
            'level': logging.NOTSET,
        },
        'alembic': {
            'level': logging.NOTSET,
        },
        'alembic.runtime.migration': {
            'level': logging.NOTSET,
        },
    },
})

app = typer.Typer()


@app.command()
def enable_alembic() -> None:
    """Inspect the state of the database and make sure Alembic is enabled"""
    alembic_cfg = Config('alembic.ini')
    inspector = inspect(engine)
    if not inspector.has_table('alembic_version'):
        if inspector.has_table('taftable'):
            # pre-alembic schema seems to exist, so stamp current database with initial version
            stamp(alembic_cfg, revision='ca7c433921cf')


@app.command()
def seed() -> None:
    'Seed the database with default TAFS'

    # load taf from file
    logging.info('loading taflist')
    try:
        with engine.connect() as conn:
            tx = conn.begin()

            logging.info('inserting tafs')
            with open('migrationdata/taflist.json', mode='rb') as taf_list:
                taflist: dict = json.load(taf_list)

                for tafjson in taflist:

                    try:
                        _upsert_taf(
                            conn=conn,
                            taf_id=tafjson['taf']['uuid'],
                            creation_date=datetime.strptime(
                                tafjson['creationDate'],
                                DATE_FORMAT).replace(tzinfo=timezone.utc),
                            base_time=datetime.strptime(
                                tafjson['taf']['baseTime'],
                                DATE_FORMAT).replace(tzinfo=timezone.utc),
                            editor=tafjson['editor'],
                            taf=tafjson['taf'])
                    except SQLAlchemyError as err:
                        logging.error('error while inserting taf into database',
                                      extra={
                                          'taf_id': tafjson['uuid'],
                                          'err': err
                                      })
                        tx.rollback()
                        return
                tx.commit()

    except FileNotFoundError:
        logging.info('file "migrationdata/taflist.json" not found - continue')


def _upsert_taf(conn: Connection, taf_id: str, creation_date: datetime,
                base_time: datetime, taf: dict, editor):
    '''Function to populate TAF database during startup'''
    logging.info('insert taf into database', extra={'taf_id': taf_id})

    stmt = insert(TafDbModel).values(taf_id=taf_id,
                                     creationDate=creation_date,
                                     baseTime=base_time,
                                     editor=editor,
                                     taf=taf).on_conflict_do_update(
                                         index_elements=['taf_id'],
                                         set_=dict(creationDate=creation_date,
                                                   baseTime=base_time,
                                                   editor=editor,
                                                   taf=taf))
    conn.execute(stmt)


if __name__ == '__main__':
    app()
